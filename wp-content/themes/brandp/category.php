<!DOCTYPE html>
<head>
	<?php get_header(); ?>
</head>

<body>
	<?PHP include 'nav.php'; ?>
	
	<?php
	$pageid = get_option( 'page_for_posts' );	
	$imgfeat = hasFeaturedImage($pageid);
	$the_title = get_the_title( $pageid );
	if (strlen($the_title)>13) $the_title = str_replace(' ','<br>',$the_title);
	?>
	<div id='feature'>
		<div class='wrap' style="height:100%;"><div style="display:table; height:100%;"><span><?php echo $the_title; ?></span></div></div>
	</div>
	<script>jQuery("#feature").backstretch("<?php echo $imgfeat[0]; ?>")</script>
	
	<div id="page">
		
		<div id="wrap">
			<div id="single_content" class="blog">
				<?php $i=0; ?>
				<?php while ( have_posts() ) : the_post() ?>
					<div class="post">
							<a href="<?php the_permalink(); ?>">
								<div class="fimg"><?php the_post_thumbnail('thumb_new'); ?></div>
							</a>
							<div class="post_text">
								<div class="inner">
									<a href="<?php the_permalink(); ?>">
										<div class="title"><?php the_title(); ?></div>
									</a>
									<div class="date"><i class="fa fa-calendar-o"></i><?php the_time('j M Y'); ?></div>
									<div class="post_content"><?php the_content(); ?></div>
								</div>
							</div>
					</div>
					
					
				<?php $i++; ?>
				<?php endwhile; ?>
			</div>
			
		</div>
	</div>
	
	<div id="footer">
		<?php get_footer(); ?>
	</div>
</body>