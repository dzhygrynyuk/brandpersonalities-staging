var page = {};
var mobile = false;


jQuery(document).ready(function($){ //WordPress

	$('.social-section a.__pop_up').on('click',function(e){
    e.preventDefault();
    window.open(jQuery(this).attr('href'),'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');
  });

	$('.textCell a').attr('target', '_blank');

	$('body').on('click','.page-down',function(){
		var $target = $(this).closest('.row');
		$('html, body').stop().animate({
				'scrollTop': $target.height()
		}, 350);
	});

	$('body').on('click','.burger-cont',function(){
		var burger = $('.burger',this);
    if(burger.hasClass('open')){
      burger.removeClass('open');
			$('.mobile-nav').removeClass('open');
    } else {
      burger.addClass('open');
			$('.mobile-nav').addClass('open');
    }
	});

	var top = $('.mobile-nav li').has('ul').find('a:first').prepend('<div class="expand"></div>');
	$('body').on('click','.mobile-nav li .expand',function(){
	    event.preventDefault();
		var par = $(this).closest('li');
		if($('ul:first',par).is(':visible')){
			$('ul:first',par).slideToggle(250);
			$(this).removeClass('open');
		} else {
			$('ul:first',par).slideToggle(250);
			$(this).addClass('open');
		}
	});

  $('body').on('click','.__window .close, .__cancel, #blackout',function(e){
    $('.__window').fadeOut(250);
    $('#blackout').fadeOut(250);
    document.body.style.overflow = 'auto';
  });

  //--- Media Queries ---//
  function is_mobile(){
    if (window.matchMedia("(max-width: 1280px)").matches) {
      mobile = true;
    } else {
      mobile = false;
    }
  }
  is_mobile();

  var resizeTimer;
  $(window).on('resize',function(){
    clearTimeout(resizeTimer);
    resizeTimer = setTimeout(function() {
      is_mobile();
			hide_empty_cols();
    }, 250);
  });

 function hide_empty_cols(){
	  $('.row .col').each(function(i,v){
	    if($('.innerCol',this).length>0){
	      if(!$.trim( $('.innerCol',this).html() ).length){
	        if(mobile) $(this).hide();
	        else $(this).show().css('display','table-cell');
	      }
	    }
	  });
	}
	hide_empty_cols();

	//--- Scroll Event ---//
  var compact = false;

  var dh = $(document).height();
  var wh = $(window).height();

  var scroll = 80;

  function scroll_conds(){
		if ($(window).scrollTop() >= scroll){
			if(!compact){
				$('.nav').addClass('compact');
				$('.page').addClass('compact');
				$('.mobile-nav').addClass('compact');
				compact = true;
			}
		}
		else if ($(window).scrollTop() < scroll && !(dh <= wh)){
			if(compact){
        $('.nav').removeClass('compact');
				$('.page').removeClass('compact');
        $('.mobile-nav').removeClass('compact');
				compact = false;
			}
		}
  }
  scroll_conds();

	$(window).scroll(function(){
		dh = $(document).height();
		wh = $(window).height();
		scroll_conds();
	});









}); //WordPress
