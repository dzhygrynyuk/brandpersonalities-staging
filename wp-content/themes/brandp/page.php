<!DOCTYPE html>
<head>
	<?php get_header(); ?>
</head>

<body>
	<div id="blackout"></div>
	<div class="page">
		<?php include 'nav.php'; ?>

			<div id="single_content">
				<?php while ( have_posts() ) : the_post() ?>
					<?php echo load_page($post->ID); ?>
				<?php endwhile; ?>
			</div>
	</div>

	<?php get_footer(); ?>

	<?php
	// $s = array('settings'=>array('link_id'=>4));
	// $s = array('settings'=>array('link_id'=>1));
	// echo display_form($s);
	?>

</body>
