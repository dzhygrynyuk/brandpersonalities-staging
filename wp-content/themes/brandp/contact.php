<?php
/*
Template Name: Contact
*/
?>

<!DOCTYPE html>
<head>
	<?php get_header(); ?>
</head>

<body>
	
	<?PHP include 'nav.php'; ?>
	<div id="page" class="footer_fix">
		
		<div id="single_content">
			<?php while ( have_posts() ) : the_post() ?>
			
				<?php echo the_content(); ?>
			
			<?php endwhile;?>
		</div>
		
	</div>
	
	<script>
	jQuery(document).ready(function($){
		$(window).load(function(){
			var message = "<?php echo $_GET['msg']; ?>";
			$('#rsvpBox textarea').val(message);
		});
	});
	</script>
		
	<div id="footer">
		<?php //echo $validation; ?>
		<?php get_footer(); ?>
	</div>
	
</body>