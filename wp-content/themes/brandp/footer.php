<?php

$pages = wp_nav_menu( array( 'theme_location' => 'footer-pages', 'container_class' => 'f-nav', 'echo' => false ) );
$services = wp_nav_menu( array( 'theme_location' => 'footer-services', 'container_class' => 'f-nav', 'echo' => false ) );

?>

<footer class="footer">
	<div class="footer-main">
		<div class="wrap">
			<div class="footer-left">
				<a class="inline" href="<?php bloginfo('url'); ?>">
					<div class="footer-logo"></div>
				</a>
			</div>
			<div class="footer-right">
				<?php echo dsp_nav('footer-socials'); ?>

				<div class="footer-copy">
					<!--a style="display:block; height:100%;" target="_blank" href="http://www.debbieobrands.com/">
						Designed by Debbie.O<br>
					</a-->
					<a style="display:block;" target="_blank" href="https://www.brandpersonalities.com.au/terms-and-conditions/">
						Terms and Conditions </a> © Brand Personalities 2020
						
				</div>
			</div>
		</div>
	</div>
</footer>
<?php wp_footer(); ?>

<script src="https://www.google.com/recaptcha/api.js?onload=CaptchaCallback&render=explicit" async defer></script>
<script type="text/javascript">
  var CaptchaCallback = function() {
    jQuery('.g-recaptcha').each(function(index, el) {
      var id = grecaptcha.render(el, {
				'sitekey' : '6LcSEGkUAAAAAPDWbf8UnDHrzjBLxrWR5zyfgPic',
				'callback' : formSubmit
			});
			jQuery(el).attr('widget-id',id);
    });
  };
</script>
