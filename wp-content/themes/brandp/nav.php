<?php

function dsp_nav($class=null){ ?>
	<div class="social-section <?php echo $class; ?>">

		<a target="_blank" href="https://www.facebook.com/DebbieOBrands/">
			<div class="sharebtn icc __facebook"></div>
		</a>
		<a target="_blank" href="https://www.instagram.com/debbieobrands/">
			<div class="sharebtn icc __instagram"></div>
		</a>
		<a target="_blank" href="https://www.linkedin.com/in/debbieobrands/">
			<div class="sharebtn icc __linkedin"></div>
		</a>
		<a target="_blank" href="https://www.youtube.com/channel/UCdfJ41SNKZecS7zYMYsuvdQ">
			<div class="sharebtn icc __youtube"></div>
		</a>

	</div>
<?php }

?>

<div class="offset"></div>

<div class="nav">

	<div class="mini-nav text"></div>

	<div class="wrap">
		<div class="main-nav">

			<nav>
				<a class="inline" href="<?php bloginfo('url'); ?>">
					<div class="logo"></div>
				</a>
				<?php
					$menu = clean_custom_menus('header-menu','nav-menu');
					echo $menu;
					// $menu = wp_nav_menu( array( 'theme_location' => 'header-menu', 'container_class' => 'nav-menu', 'echo' => false ) );
					// echo $menu;
				?>

				<div class="burger-cont">
					<div class="burger">
						<span></span>
						<span></span>
						<span></span>
						<span></span>
					</div>
				</div>

			</nav>

		</div>
	</div>
</div>

<div class="mobile-nav">
	<div class="mobile-nav-inner">
		<nav>
			<?php
				$menu = clean_custom_menus('header-menu','topnav');
				echo $menu;
				// $menu = wp_nav_menu( array( 'theme_location' => 'header-menu', 'container_class' => 'topnav', 'echo' => false ) );
				// echo $menu;
			?>
		</nav>
		<?php //dsp_nav('mobile'); ?>
	</div>

</div>

<?php
function social_share($page_url,$page_title){
  ob_start(); ?>

  <div class="social-section share-section">

		<a class="__pop_up" href="http://www.facebook.com/share.php?u=<?php echo $page_url; ?>&title=<?php echo $page_title; ?>">
      <div class="sharebtn icc __facebook"></div>
    </a>
		<a class="__pop_up" href="http://twitter.com/intent/tweet?status=<?php echo $page_title; ?>+<?php echo $page_url; ?>">
			<div class="sharebtn icc __twitter"></div>
		</a>
    <a class="__pop_up" href="http://www.linkedin.com/shareArticle?mini=true&url=<?php echo $page_url; ?>">
      <div class="sharebtn icc __linkedin"></div>
    </a>

  </div>

  <?php return ob_get_clean();
} ?>
