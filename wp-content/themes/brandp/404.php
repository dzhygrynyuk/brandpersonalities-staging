<!DOCTYPE html>
<head>
	<?php get_header(); ?>
</head>

<body>
	<div class="page">
		<?php include 'nav.php'; ?>

		<?php $a = get_page_by_title('404')->ID; ?>
		<?php echo load_page($a); ?>
		<?php $a = get_page_by_title('Personalities')->ID; ?>
		<?php echo load_page($a); ?>

	</div>

	<?php get_footer(); ?>
</body>
