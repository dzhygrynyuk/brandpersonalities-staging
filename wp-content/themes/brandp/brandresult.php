<?php /* Template Name: Brand Result */ ?>

<?php 
include 'brandgame/config.php'; 
include 'brandgame/db.php'; 

function brandp_replace_tags($string,$data){
  $string = str_replace("{name}", $data['name'], $string);

  $data['personality'] = implode(' and ', $data['personality']);
  $string = str_replace("{personality}", $data['personality'], $string);

  return stripslashes($string);
}

$personality = get_query_var( 'personality' );
$personality = explode('-',$personality);
$token = $_GET['token'];
$play_result = $_GET['result'];	
$game_link = get_bloginfo('url').'/brand-quiz/?token='.$token;

$icons = array(
  'confused'=>array('name'=>'Confused', 'color'=>'#510856'),
  'caregiver'=>array('name'=>'The Caregiver', 'color'=>'#EF5289'),
  'creator'=>array('name'=>'The Creator', 'color'=>'#F89C4D'),
  'entertainer'=>array('name'=>'The Entertainer', 'color'=>'#862665'),
  'everyman'=>array('name'=>'The Neighbour', 'color'=>'#7DCEF1'),
  'explorer'=>array('name'=>'The Explorer', 'color'=>'#F58021'),
  'hero'=>array('name'=>'The Hero', 'color'=>'#EE3124'),
  'innocent'=>array('name'=>'The Innocent', 'color'=>'#00B5EF'),
  'magician'=>array('name'=>'The Magician', 'color'=>'#510856'),
  'rebel'=>array('name'=>'The Rebel', 'color'=>'#003E6A'),
  'ruler'=>array('name'=>'The Ruler', 'color'=>'#005A96'),
  'sage'=>array('name'=>'The Sage', 'color'=>'#7CCCCC'),
  'seducer'=>array('name'=>'The Seducer', 'color'=>'#ED1944')
);

$colors = array(
  1=>'#EF5289',
  2=>'#F89C4D',
  3=>'#862665',
  4=>'#7DCEF1',
  5=>'#F58021',
  6=>'#EE3124',
  7=>'#00B5EF',
  8=>'#510856',
  9=>'#003E6A',
  10=>'#005A96',
  11=>'#7CCCCC',
  12=>'#ED1944'
);

foreach($icons as $k => $v){
  $url = strtolower($v['name']);
  $url = str_replace(' ','-',$url);
  $url = 'https://brandpersonalities.com.au/personalities/'.$url;
  $icons[$k]['url'] = $url;
  $icons[$k]['img'] = get_bloginfo('template_url').'/img/icons/'.$k.'.png';
}

$arr = array();
foreach($personality as $p){
  $arr[] = $icons[$p]['name'];
}
$personality_string = implode(' and ', $arr);

if(empty($personality_string)) $personality_string = 'Confused';

$tagData = array(
  'personality'=>$arr,
);

$success = brandp_replace_tags(get_option('message_success'),$tagData);
$fail = brandp_replace_tags(get_option('message_fail'),$tagData);

$result_string = $personality[0] == 'confused' ? $fail : $success;

// ========================================================

if(get_query_var('personality') == 'confused' && !empty($token) && ($play_result === 'first' || $play_result === 'second')){

  $sql_user = "SELECT * FROM bg_users WHERE token = ?";
  $user = $db->select_row($sql_user,array($token));
  $plays_avail = $user['plays_avail'];

  //$sql_results = "SELECT * FROM bg_results WHERE user_id = ?";
  //$result = $db->select_row($sql_results,array($user['ID']));
  $sql_results = 'SELECT * FROM bg_results WHERE user_id IN ('.$user['ID'].')';
  $result = $db->select($sql_results,array());

  if($play_result === 'first'){
  	$personalities_ids = $result[0]['personalities'];
  	$_result_person = $result[0]['result'];
  }elseif($play_result === 'second'){
  	$personalities_ids = $result[1]['personalities'];
  	$_result_person = $result[1]['result'];
  }

  // if($plays_avail < 1){
  //   $personalities_ids = $result[1]['personalities'];
  // }else{
  //   $personalities_ids = $result[0]['personalities'];
  // }


  $_personality_ids = explode(",",$personalities_ids);
  $count_personalities = array_count_values($_personality_ids);

  $sql_personalities = 'SELECT id, value FROM bg_personalities WHERE ID IN (' . $personalities_ids . ')';
  $personalities = $db->select($sql_personalities,array());

}

?>

<!DOCTYPE html>
<head>
  <title><?php echo 'I Am '.$personality_string.'. What are you?'; ?></title>
  <meta property="og:url"                content="<?php echo get_bloginfo('url').'/brand-result/'.get_query_var( 'personality' ); ?>" />
  <meta property="og:title"              content="<?php echo 'I Am '.$personality_string.'. What are you?'; ?>" />
  <meta property="og:description"        content="Discover your Brand Personality today!" />
  <meta property="og:image"              content="https://brandpersonalities.com.au/wp-content/uploads/2018/08/Branding-with-Personality-Brand-Personalities-System-Game-developed-by-White-River-Design-Branding-Studio-2.jpg" />
	<?php get_header(); ?>
</head>

<body>
	<div id="blackout"></div>
	<div class="page result_page">
		<?php include 'nav.php'; ?>

			<div id="single_content">

        
        <?php if(get_query_var('personality') == 'confused' && !empty($token) && ($play_result === 'first' || $play_result === 'second') && $_result_person === '13'):?>
          
          <div class="result_banner confused_banner">
            <div class="wrap">
              <?php if($plays_avail < 1):?>
                <h2>It looks as though you are still unclear about your Brand Personality.</h2>
              <?php else: ?>
                <h2>Oops! It looks like you’re trying to be all things to all people…</h2>
              <?php endif; ?>  
              <div class="confuser-results">
                <?php foreach ($personalities as $value): ?>
                  <div class="item <?php if( $count_personalities[$value['id']] > 1) echo 'first-item'; ?>">
                    <div class="circle" style="background-color: <?php echo $colors[$value['id']];?>"><?php if( $count_personalities[$value['id']] > 1) echo '40%'; else echo '20%'; ?></div>
                    <h3><?php echo $value['value'];?></h3>
                  </div>
                <?php endforeach; ?>
              </div>
            </div>
          </div>

        <?php else: ?>
          <div class="result_banner">
            <ul>
              <?php foreach($personality as $p){
                $p = $icons[$p];
              ?>
              <li>
                <div class="icon-inner">
                  <div  class="brandp-icon"
                        style="background-image:url('<?php echo $p['img']; ?>?v=1.01');">
                  </div>
                  <div class="brand-name"><?php echo $p['name']; ?></div>
                </div>
              </li>
              <?php } ?>
            </ul>
          </div>
        <?php endif; ?>  

        <?php if(get_query_var('personality') == 'confused' && !empty($token) && ($play_result === 'first' || $play_result === 'second') && $_result_person === '13'):?>
          <div class="row result_row confused_result_row">
            <div class="wrap">
              <div class="col">
                <?php if($play_result === 'second'):?>
                  <h2>Remember you can only be 1 or 2 personalities.</h2>
                  <p>Finding your Brand Personality, as you can see, is not easy, but with some extra guidance we know you will get there!<br>
                  Either give the quiz another go, book in a one-on-one consulation or join our <b><a href="https://www.brandmagicmasterclass.com/">Brand Magic Masterclass</a></b> where you will learn all you need to know<br> about branding and have unlimited access to use Brand Personalities until you find a result.</p>
                  <br>
                  <div class="button-wrap">
                    <a href="<?php echo get_home_url(); ?>/brand-quiz/">
                      <div class="button" style="background-color: #E40572;">
                        <div class="button-over"></div>
                        <div class="__c">Try the quiz again</div>
                      </div>
                    </a>
                    <a href="https://www.brandmagicmasterclass.com/1-on-1-brand-masterclass-consultation/">
                      <div class="button __restart">
                        <div class="button-over"></div>
                        <div class="__c">Book a one-on-one consultation</div>
                      </div>
                    </a>
                    <a href="https://www.brandmagicmasterclass.com/">
                      <div class="button" style="background-color: #510856;">
                        <div class="button-over"></div>
                        <div class="__c">Learn about Branding</div>
                      </div>
                    </a>
                  </div>
                <?php elseif($play_result === 'first'): ?>
                  <h2>Hold it tiger! You can only be 1 or 2 personalities</h2>
                  <p><b>More than that is confusing for your customer and well, everyone that has to deal with you!</b></p>
                  <p>Even though you are currently unclear, don’t stress! You have another opportunity to do the quiz again.</p>
                  <p><b>Next time around, take a bit more time to think about your answers. </b><br>
                    Focus on how you do what you do, how you want your customers to feel and try and find words that fit together like a puzzle.<br>
                    This will help you get a better result.</p>
                  <p>Good luck!</p>
                  <br>
                  <div class="button-wrap">
                    <a href="<?php echo $game_link; ?>">
                      <div class="button __restart">
                        <div class="button-over"></div>
                        <div class="__c">Try again</div>
                      </div>
                    </a>
                  </div>
                <?php endif; ?>
              </div>
            </div>
          </div> 
        <?php else: ?>
          <div class="row result_row">
            <div class="wrap">
              <div class="col">
                <div class="textCell text" data-style="normal" data-inherit="true">
                  <?php echo $result_string; ?>
                </div>

                <div class="button-wrap">
                  <?php if(!empty($token)){ ?>
                  <a href="<?php echo $game_link; ?>">
                    <div class="button __restart">
                      <div class="button-over"></div>
                      <div class="__c">Restart Quiz</div>
                    </div>
                  </a>
                  <?php } ?>
                </div>

                <?php
                  $page_url = get_bloginfo('url').'/brand-result/'.get_query_var( 'personality' );
                  $page_title = 'I Am '.$personality_string.'. What are you?';
                ?>
                <div class="social-share-contain">
                  <div class="social-label">Share on Socials</div>
                  <?php echo social_share($page_url,$page_title); ?>
                </div>

              </div>
            </div>
          </div>
        <?php endif; ?>

				<?php /* while ( have_posts() ) : the_post() ?>
          <?php echo load_page($post->ID); ?>
        <?php endwhile; */ ?>

        <!-- ===================================== Archetypes START ===================================== -->
        <?php while ( have_posts() ) : the_post(); 
          $fields = get_fields(); ?>

          <?php if(!empty($token)):?>
            <div class="row _1  m_1   is_wrap" style="; background-color:#E2E2E2;" data-animate=";" data-inherit="false;">
              <div class="wrap" data-pad="true">
                <div class="wrapInner top">
                  <div class="col " style="vertical-align:middle;">
                    <div class="innerCol top">
                      <div class="textCell text" data-style="normal" data-listyle="" data-inherit="true">
                        <?php if($fields['archetypes_title']): ?>
                          <h1 style="text-align: center;"><?php echo $fields['archetypes_title'];?></h1>
                        <?php endif; ?>  
                        <?php if($fields['archetypes_desc']): ?>
                          <p style="text-align: center;"><?php echo $fields['archetypes_desc'];?></p>
                        <?php endif; ?>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <?php if($fields['archetypes_list']): ?>
              <div class="row _1  m_1   is_wrap" style="; background-color:#E2E2E2;" data-animate=";" data-inherit="false;">
                <div class="wrap" data-pad="true">
                  <div class="wrapInner topMini">
                    <div class="col " style="vertical-align:middle;">
                      <div class="innerCol topMini">
                        <?php foreach ($fields['archetypes_list'] as $archetype): ?>
                          <a class="inline" href="<?php echo $archetype['url'];?>">
                            <div class="fullGrid-contain">
                              <div class="image fullImg fullGrid">
                                <img src="<?php echo $archetype['image'];?>">
                              </div>
                            </div>
                          </a>  
                        <?php endforeach; ?>                                                                  
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            <?php endif; ?>

            <?php if(!empty($fields['archetypes_btn']) && ($fields['archetypes_btn']['title'] && $fields['archetypes_btn']['url'])) : ?>
              <div class="row _1  m_1   is_wrap" style="; background-color:#E2E2E2;" data-animate=";" data-inherit="false;">
                <div class="wrap" data-pad="true">
                  <div class="wrapInner bottom">
                    <div class="col " style="vertical-align:middle;">
                      <div class="innerCol bottom">
                        <div class="button-outer" align="center">
                          <a href="<?php echo $fields['archetypes_btn']['url'];?>">
                            <div class="button coloured" data-style="coloured" style="">
                              <div class="button-over"></div>
                              <div class="__c"><?php echo $fields['archetypes_btn']['title'];?></div>
                            </div>
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>  
            <?php endif;?>
          <?php else: ?>  
            <?php echo load_page($post->ID); ?>
          <?php endif;?>  

        <?php endwhile; ?>
        <!-- ====================================== Archetypes END ====================================== -->

			</div>
	</div>


  <?php $p = $icons[$personality[0]]; ?>
  <style>
    .row[data-inherit="true"],
    .button[data-style="coloured"]{
      background-color:<?php echo $p['color']; ?> !important;
    }
    .col .textCell[data-inherit="true"] h1 {
      color:<?php echo $p['color']; ?> !important;
    }
    .col .card-contain[data-inherit="true"] {
      border-color:<?php echo $p['color']; ?>;
    }
    .col .card-contain[data-inherit="true"] .separate {
      background-color:<?php echo $p['color']; ?>;
    }
  </style>

	<?php get_footer(); ?>

	<?php
	// $s = array('settings'=>array('link_id'=>4));
	// $s = array('settings'=>array('link_id'=>1));
	// echo display_form($s);
	?>

  	<?php if(get_query_var('personality') != 'confused'):?>
	  <script>
	    var url = window.location.href;
	    url = url.split('?')[0];
	    history.replaceState(null,null,url);
	  </script>
	<?php endif; ?> 

</body>
