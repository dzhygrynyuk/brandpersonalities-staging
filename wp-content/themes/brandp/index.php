<?php
/*
Template Name: Blog Page
*/
?>

<!DOCTYPE html>
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<?php get_header(); ?>
</head>

<body>
	<?PHP include 'nav.php'; ?>

	<?php
	$pageid = get_option( 'page_for_posts' );
	$content_post = get_post($pageid);
	$content = $content_post->post_content;
	$content = apply_filters('the_content', $content);
	$content = str_replace(']]>', ']]&gt;', $content);
	?>

	<div id="page">
		<div id="single_content" class="blog">

			<?php echo $content; ?>

			<div class="ly_row padding">
				<div class="ly_wrap">
					<div class="ly_col">
					<div class="inner">

						<?php
						$url = 'https://www.youtube.com/watch?v=1g0QB5TW2i8';
						$id = '1g0QB5TW2i8';
						$img = 'http://img.youtube.com/vi/'.$id.'/sddefault.jpg';
						?>
						<div class="ly_video_cell" data-video="<?php echo $id; ?>" data-url="<?php echo $url; ?>" data-type="yt_video">
							<div class="ly_image_cell lazyload resize"
									 style="width:100%;"
									 data-height="300" >
								<div class="ly_overlay">
									<div class="ly_overlay_text"><i class="fa fa-play" aria-hidden="true"></i></div>
								</div>
							</div>
						</div>
					</div>

				<div class="inner">

				<?php
				$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
				$posts = $paged==1 ? 3 : 4;
				$class = $paged==1 ? 'featr' : 'grid';

				$count = new WP_Query( array('posts_per_page'=>'-1', 'category__not_in'=>array(get_cat_ID( 'event' ))) );
				$count = $count->post_count;
				$maxpp = $posts;

				$offset = $paged==1 ? 0 : (($paged-1) * $posts)-1;

				$args = array(
					'posts_per_page' => $posts,
					'category__not_in' => array(get_cat_ID( 'event' )),
					'offset' => $offset
				);

				$wp_query = new WP_Query( $args );

				?><ul class="events blog <?php echo $class; ?>"><?php

				$i=0;
				if ( $wp_query->have_posts() ) : while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>

				<?php
					$img_id = get_post_thumbnail_id( $post_id );
					$featured = $i==0 ? true : false;
					$class = $featured ? 'featured' : 'normal';
					$size = $featured && $paged==1 ? 'resize' : '_square';
					$imgSrc = wp_get_attachment_image_src( $img_id, get_size($featured) );
					$imgSrc = $imgSrc[0];
					$excerpt = text_excerpt(get_the_content(),150);
				?>

					<li class="<?php echo $class; ?>">
						<a href="<?php the_permalink(); ?>">
							<div class="ly_image_cell lazyload <?php echo $size; ?>"
									 style="width:100%;"
									 data-background="<?php echo $imgSrc; ?>" <?php //data-background is used for lazy loading ?>
									 data-height="335">
							</div>
						</a>

							<div class="title"><a href="<?php the_permalink(); ?>"><h1><?php the_title(); ?></h1></a></div>
							<div class="date"><?php the_time('F j, Y'); ?></div>
							<div class="post">
								<?php echo $excerpt; ?> ... <a class="readmore" href="<?php the_permalink(); ?>">Read More</a>
							</div>
					</li>

					<?php $i++; ?>

				<?php endwhile; endif; wp_reset_query();?>

				</ul>


				<?php
				if($maxpp*$paged < $count) {
					next_posts_link( '<div class="nav-previous alignleft">Older Stories</div>' );
				}
					previous_posts_link( '<div class="nav-next alignright">Newer Stories</div>' );
				?>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>

	<div id="footer">
		<?php get_footer(); ?>
	</div>
</body>
