<?php do_action('wp_head'); ?>
<?php
	global $post;
	$meta = get_post_meta( $post->ID, 'custom_metadata', true );
	$meta = json_decode($meta,true);
?>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<?php if(!empty($meta['meta'])){ ?>
<meta name="description" content="<?php echo $meta['meta'] ?>">
<?php } ?>

<script type="text/javascript">
  var templateurl = '<?php get_bloginfo("template_url"); ?>';
  var ajaxurl = "<?php echo bloginfo('wpurl'); ?>/wp-admin/admin-ajax.php";
  var frontpage = false;
  <?php if(is_front_page()){?> frontpage=true; <?php } ?>
</script>



<?php //FAVICONS HERE ?>
<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favicons/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favicons/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favicons/favicon-16x16.png">
<link rel="manifest" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favicons/site.webmanifest">
<link rel="mask-icon" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favicons/safari-pinned-tab.svg" color="#3d3d3d">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="theme-color" content="#ffffff">
