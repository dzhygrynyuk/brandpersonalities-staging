<?php

// Function to check if this is an elementor page
function is_elementor(){
  global $post;
  return \Elementor\Plugin::$instance->db->is_built_with_elementor($post->ID);
}


function setup() {
  add_theme_support('title-tag');
}
add_action('after_setup_theme', 'setup');


function custom_rewrite_tag_brandp() {
  add_rewrite_tag('%personality%', '([^&]+)');
}
add_action('init', 'custom_rewrite_tag_brandp', 10, 0);

function custom_rewrite_rule_brandp() {
  add_rewrite_rule('^brand-result/([^/]*)/?','index.php?pagename=brand-result&personality=$matches[1]','top');
}
add_action('init', 'custom_rewrite_rule_brandp', 10, 0);


function clean_custom_menus($menu_name,$class) {
  global $post;
  $thePostID = $post->ID;
  if (($locations = get_nav_menu_locations()) && isset($locations[$menu_name])) {
    $menu = wp_get_nav_menu_object($locations[$menu_name]);
    $menuitems = wp_get_nav_menu_items($menu->term_id);

    ob_start();
    echo '<div class="'.$class.'">' ."\n";
    echo "\t\t\t\t". '<ul id="menu-main">' ."\n";

    $count = 0;
    $submenu = false;
    foreach( $menuitems as $item ):
      // get page id from using menu item object id
      $id = get_post_meta( $item->ID, '_menu_item_object_id', true );
      // set up a page object to retrieve page data
      $page = get_page( $id );
      $link = get_page_link( $id );
    
      $title = $item->title;
    
      // item does not have a parent so menu_item_parent equals 0 (false)
      if ( !$item->menu_item_parent ):
        // save this id for later comparison with sub-menu items
        $parent_id = $item->ID;
        $class = '';
        if($thePostID == $item->object_id) $class .= 'current-menu-item';
        if ( $menuitems[$count]->title == 'Personalities' ) $class .= ' menu-item-has-children';?>

        <li class="item <?php echo $class; ?>"><a href="<?php echo $link; ?>" class="title"><?php echo $title; ?></a>
      <?php endif; ?>

      <?php if ( $parent_id == $item->menu_item_parent ): ?>

        <?php if ( !$submenu ): $submenu = true; ?>
          <ul class="sub-menu">
        <?php endif; ?>

        <li class="item"><a href="<?php echo $link; ?>" class="title"><?php echo $title; ?></a></li>

        <?php if ( $menuitems[ $count + 1 ]->menu_item_parent != $parent_id && $submenu ): ?>
          </ul>
        <?php $submenu = false; endif; ?>

      <?php endif; ?>

      <?php if($title == 'Personalities') : ?>
        <ul class="sub-menu">
        <?php
          $pg = get_page_by_title( $title );
          $children = new WP_Query( array(
            'post_type'     => 'page',
            'post_parent'   => $pg->ID,
            'order'         => 'ASC',
            'orderby'       => 'post_title',
            'post_status'   => 'publish',
            'posts_per_page'=> -1
          ) );
          while ( $children->have_posts() ) {
            $children->the_post();
            $name = get_the_title();
            $color = get_post_meta(get_the_ID(),'custom_color')[0];
            ?>
            <li>
              <a href="<?php echo get_permalink($children->ID); ?>" style="color:#<?php echo $color; ?>">
                <?php echo $name; ?>
              </a>
            </li>
          <?php } ?>
        </ul>
      <?php endif; ?>

      <?php // if ( $menuitems[ $count + 1 ]->menu_item_parent != $parent_id ): ?>
      <?php if ( $parent_id == $item->menu_item_parent ): ?>
    </li>
    <?php $submenu = false; endif; ?>

    <?php $count++; endforeach;

    // var_dump($menu_items);
    echo "\t\t\t\t". '</ul>' ."\n";
    echo "\t\t\t". '</div>' ."\n";
    $return = ob_get_clean();

    // var_dump($menuitems);

    return $return;
  } else {
    // $menu_list = '<!-- no list defined -->';
  }
}

/* Admin init */
add_action( 'admin_init', 'my_settings_init' );

/* Settings Init */
function my_settings_init(){

    /* Create settings section */
    add_settings_section(
      'wrd-settings',
      'Brand Personalities Game',
      'wrd_settings_desc',
      'general'
    );

    register_setting(
      'general',
      'wrd-notification-email'
    );
    add_settings_field(
      'wrd-notification-email',
      'Notification E-Mail',
      'wrd_settings_fn',
      'general',
      'wrd-settings',
      array('id'=>'wrd-notification-email','type'=>'text')
    );

    register_setting(
      'general',
      'wrd-personal-price'
    );
    add_settings_field(
      'wrd-personal-price',
      'Personal Price',
      'wrd_settings_fn',
      'general',
      'wrd-settings',
      array('id'=>'wrd-personal-price','type'=>'number')
    );

    register_setting(
      'general',
      'wrd-business-price'
    );
    add_settings_field(
      'wrd-business-price',
      'Business Price',
      'wrd_settings_fn',
      'general',
      'wrd-settings',
      array('id'=>'wrd-business-price','type'=>'number')
    );
}
function wrd_settings_desc(){
    echo wpautop( "Set options for brand personalities game." );
}
function wrd_settings_fn($args){
  $opt = get_option( $args['id'] );
  if($args['type']=='textarea'){ ?>
    <textarea cols="50" rows="10" name="<?php echo $args['id']; ?>"><?php echo $opt; ?></textarea>
  <?php } else if($args['type']=='textarea_small'){ ?>
    <textarea cols="30" rows="5" name="<?php echo $args['id']; ?>"><?php echo $opt; ?></textarea>
  <?php } else if($args['type']=='number'){ ?>
    <input type="number" name="<?php echo $args['id']; ?>" value="<?php echo $opt; ?>">
  <?php } else { ?>
    <input type="text" name="<?php echo $args['id']; ?>" value="<?php echo $opt; ?>">
  <?php }
}

function get_size($feature=false){
  if ($feature) {
    $size = wp_is_mobile() ? 'large' : 'full';      //mobile : desktop
  } else {
    $size = wp_is_mobile() ? 'large' : 'large';       //mobile : desktop
  }
  return $size;
}

function wrd_include_page($atts, $content){
  if (! is_elementor()) {
    extract(shortcode_atts(array('p'=>null), $atts));
    $page = get_page_by_title($p);
    ob_start(); ?>
    <?php echo load_page($page->ID); ?>
    <?php return ob_get_clean();
  }
}
add_shortcode('includePage', 'wrd_include_page');

//---------------- [Redirect]

function ly_redirect($atts, $content){
  extract(shortcode_atts(array('form'=>null), $atts));
  $home = get_bloginfo('url');
  ob_start(); ?>
  <script>
     window.location="<?php echo $home.'/services/?form='.$form; ?>";
  </script>
  <?php return ob_get_clean();
}
add_shortcode('redirect', 'ly_redirect');


//----------------

//Get rid of random <P></P> tags
remove_filter ('the_content', 'wpautop');
//remove_filter ('comment_text', 'wpautop');

if( !function_exists('wpex_fix_shortcodes') ) {
  function wpex_fix_shortcodes($content){
    $content = do_shortcode($content);

    //remove useless empty <p></p> tags
    $content = preg_replace( '%<p>&nbsp;\s*</p>%', '', $content );
    $content = preg_replace( '/<p[^>]*><\\/p[^>]*>/', '', $content );

    return $content;
  }
  add_filter('the_content', 'wpex_fix_shortcodes');
}

//fix chrome bug with admin menu (slimming-paint)
function admin_menu_fix() {
  echo '<style>
    #adminmenu { transform: translateZ(0); }
  </style>';
}
add_action('admin_head', 'admin_menu_fix');

function register_my_menu() { //navigation menus
  register_nav_menu('header-menu',__( 'Header Menu' ));
  register_nav_menu('footer-pages',__( 'Footer Pages' ));
  register_nav_menu('footer-services',__( 'Footer Services' ));
}
add_action( 'init', 'register_my_menu' );

add_theme_support( 'post-thumbnails' );

add_filter('show_admin_bar', '__return_false');


/*-----IMAGE SIZES-----*/
add_image_size( 'quarter', 700, 500, array( 'center', 'center' ) );
add_image_size( 'Mquarter', 350, 250, array( 'center', 'center' ) );
add_image_size( 'square', 500, 500, array( 'center', 'center' ) );

update_option( 'medium_size_w', 600 );
update_option( 'medium_size_h', 600 );
update_option( 'large_size_w', 1400 );
update_option( 'large_size_h', 1024 );

add_filter( 'pre_update_option_medium_size_w', 'image_filter_medium_size_w' );
function image_filter_medium_size_w( $newvalue ) {
  return 600;
}
add_filter( 'pre_update_option_medium_size_h', 'image_filter_medium_size_h' );
function image_filter_medium_size_h( $newvalue ) {
  return 600;
}
add_filter( 'pre_update_option_large_size_w', 'image_filter_large_size_w' );
function image_filter_large_size_w( $newvalue ) {
  return 1400;
}
add_filter( 'pre_update_option_large_size_h', 'image_filter_large_size_h' );
function image_filter_large_size_h( $newvalue ) {
  return 1024;
}

if ( ! defined( '_S_VERSION' ) ) {
  // Replace the version number of the theme on each release.
  define( '_S_VERSION', '1.1.0' );
}


//========SCRIPTS AND STYLES==========//
function scripts_front_end() {
  //load styles
  wp_enqueue_style( 'fa', get_stylesheet_directory_uri().   '/fonts/fa/css/font-awesome.min.css' );
  wp_enqueue_style( 'main', get_stylesheet_directory_uri(). '/style.css', array(), time() );
  wp_enqueue_style( 'mq', get_stylesheet_directory_uri().   '/mq.css?v=1.10' );



  //load jquery first!
  //wp_enqueue_script('jquery');

  //load these only on the homepage
  if( is_front_page() ){
    //wp_enqueue_script( 'unslider', get_stylesheet_directory_uri().  '/js/unslider.js', array('jquery') );
    //wp_enqueue_script( 'mytwitter', get_stylesheet_directory_uri(). '/js/twitterFetcher_min.js', array('jquery') );
  }

  //load theses on all pages
  wp_enqueue_script( 'imagesloaded', get_stylesheet_directory_uri().  '/js/imagesloaded.min.js', array('jquery') );
  wp_enqueue_script( 'placeholder', get_stylesheet_directory_uri(). '/js/jquery.placeholder.min.js', array('jquery') );
  wp_enqueue_script( 'matchmedia', get_stylesheet_directory_uri().  '/js/matchmedia.js', array('jquery') );
  wp_enqueue_script( 'validate', get_stylesheet_directory_uri().  '/js/jquery.validate.min.js', array('jquery') );
  wp_enqueue_script( 'main', get_stylesheet_directory_uri().  '/js/scripts.js?v=1.06', array('jquery') );
}
add_action( 'wp_enqueue_scripts', 'scripts_front_end' );
function dequeue_jquery_migrate( &$scripts){
  if(!is_admin()){
    $scripts->remove( 'jquery');
    $scripts->add( 'jquery', false, array( 'jquery-core' ), '1.10.2' );
  }
}
add_filter( 'wp_default_scripts', 'dequeue_jquery_migrate' );

//=====GOOGLE ANALYTICS=====//
add_action('wp_footer', 'add_google_analytics');
function add_google_analytics() { ?>
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-17928506-37"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-17928506-37');
  </script>
<?php }

//=====GOOGLE ANALYTICS=====//
add_action('wp_footer', 'add_facebook_pixel');
function add_facebook_pixel() { ?>
  <!-- Facebook Pixel Code -->
  <script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window,document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');

    fbq('init', '322428671450889');
    fbq('track', 'PageView');
  </script>
  <noscript>
  <img height="1" width="1"
  src="https://www.facebook.com/tr?id=322428671450889&ev=PageView
  &noscript=1"/>
  </noscript>
  <!-- End Facebook Pixel Code -->
<?php }

?>
