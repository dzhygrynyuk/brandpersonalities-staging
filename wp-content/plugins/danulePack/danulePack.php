<?php
/*
Plugin Name: Danule Package
Version:     1.0
*/

//remove update notification for plugin
function danulePack_update_remove( $value ) {
    unset( $value->response['danulePack/danulePack.php'] );
    return $value;
}
add_filter( 'site_transient_update_plugins', 'danulePack_update_remove' );

$core = plugin_dir_path( __FILE__ );

function danulePack_core_files(){
  wp_enqueue_style( 'fa', plugins_url( '/', __FILE__ ) . 'modules/fa/css/font-awesome.min.css' );
}
add_action( 'admin_enqueue_scripts', 'danulePack_core_files' );

//--AJAX
include 'modules/selection/core.php';

//--FILES
include 'modules/fns.php';

//--Plugins
require 'pageBuilder/pageBuilder.php';
require 'slider/slider.php';
require 'form/form.php';
require 'brandp/brandp.php';
// require 'coupons/coupons.php';


// -- SLIDER DB
global $daSlider_db_version;
$daSlider_db_version = '1.0';

//activation
function daSlider_install() {
	global $wpdb;
	global $daSlider_db_version;

  $table_name = 'slider';

  $charset_collate = $wpdb->get_charset_collate();

  $sql = "CREATE TABLE $table_name (
    ID mediumint(9) NOT NULL AUTO_INCREMENT,
    slug varchar(255) NOT NULL,
    name text NOT NULL,
    content text NOT NULL,
    PRIMARY KEY  (ID)
  ) $charset_collate;";

  require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
  $res = dbDelta( $sql );

  add_option( 'daSlider_db_version', $daSlider_db_version );

  //admin message
  set_transient( 'daSlider-admin-notice', true, 5 );
}
register_activation_hook( __FILE__, 'daSlider_install' );

//admin message
function daSlider_admin_notice_fn(){
  global $wpdb;
  global $daSlider_db_version;
  if( get_transient( 'daSlider-admin-notice' ) ){
      ?>
      <div class="updated notice is-dismissible">
        Thanks!
      </div>
      <?php
      /* Delete transient, only display this notice once. */
      delete_transient( 'daSlider-admin-notice' );
  }
}
add_action( 'admin_notices', 'daSlider_admin_notice_fn' );

// -- FORMS DB
global $daForms_db_version;
$daForms_db_version = '1.1';

//activation
function daForms_install() {
	global $wpdb;
	global $daForms_db_version;
  require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

  $charset_collate = $wpdb->get_charset_collate();

  $table_name = 'form';
  $sql = "CREATE TABLE $table_name (
    ID mediumint(9) NOT NULL AUTO_INCREMENT,
    slug varchar(255) NOT NULL,
    name text NOT NULL,
    content text NOT NULL,
    notification text NOT NULL,
    PRIMARY KEY  (ID)
  ) $charset_collate;";

  $res = dbDelta( $sql );

  $table_name = 'form_submission';
  $sql = "CREATE TABLE $table_name (
    ID mediumint(9) NOT NULL AUTO_INCREMENT,
    full_name text NOT NULL,
    email text NOT NULL,
    form_id text NOT NULL,
    content text NOT NULL,
    date datetime NOT NULL,
    PRIMARY KEY  (ID)
  ) $charset_collate;";

  $res = dbDelta( $sql );

  add_option( 'daForms_db_version', $daForms_db_version );

  //admin message
  set_transient( 'daForms-admin-notice', true, 5 );
}
register_activation_hook( __FILE__, 'daForms_install' );

//admin message
function daForms_admin_notice_fn(){
  global $wpdb;
  global $daForms_db_version;
  if( get_transient( 'daForms-admin-notice' ) ){
      ?>
      <div class="updated notice is-dismissible">
        Thanks!
      </div>
      <?php
      /* Delete transient, only display this notice once. */
      delete_transient( 'daForms-admin-notice' );
  }
}
add_action( 'admin_notices', 'daForms_admin_notice_fn' );

?>
