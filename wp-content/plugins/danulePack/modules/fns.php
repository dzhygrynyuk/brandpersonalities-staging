<?php

function slugify($text){
  $text = preg_replace('~[^\pL\d]+~u', '-', $text);
  $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
  $text = preg_replace('~[^-\w]+~', '', $text);
  $text = trim($text, '-');
  $text = preg_replace('~-+~', '-', $text);
  $text = strtolower($text);
  if (empty($text)) {
    return 'n-a';
  }
  return $text;
}

function slugify_dupe($table,$slug,$id=null){
  global $wpdb;

  if(isset($id)){$id = intval($id);}
  $_slug = $slug;

  $i=1;
  while(!empty($_slug)){
    $vars = array();

    $sql = "SELECT slug FROM ".$table." WHERE slug=%s";
    $test_slug = $slug.'-'.$i;
    $vars[] = $test_slug;

    if($id) {
      $sql.=" AND ID !=%d";
      $vars[] = $id;
    }
    $sql = $wpdb->prepare($sql,$vars);
    $_slug = $wpdb->get_var($sql);
    $i++;
  }
  return $test_slug;
}

?>
