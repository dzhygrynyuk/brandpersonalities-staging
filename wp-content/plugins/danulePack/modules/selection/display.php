<link rel="stylesheet" type="text/css" href="<?php site(); ?>core/selection/style.css?v=<?php echo time(); ?>">

<div id="sel">
  <div class="box">
    <div class="searchField">
      <div class="clearField icc"></div>
      <input type="text" placeholder="search">
      <div class="button-search icc"></div>
    </div>
    <div class="results">
      <ul>

      </ul>
    </div>
    <div class="actionBox">
      <div class="button button-save"><div>Choose</div></div>
      <div class="button button-cancel"><div>Cancel</div></div>
    </div>
  </div>
</div>
