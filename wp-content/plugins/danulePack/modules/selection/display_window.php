<div id="sel" class="__sel __window">
  <div class="box">
    <div class="searchField">
      <div class="clearField icc"></div>
      <input type="text" placeholder="search">
      <div class="button-search icc"></div>
    </div>
    <div class="results">
      <ul>

      </ul>
    </div>
    <div class="name-row">
      <input type="text" id="name" placeholder="name">
    </div>
    <div class="actionBox">
      <div class="button button-save"><div>Choose</div></div>
      <div class="button button-cancel"><div>Cancel</div></div>
      <div class="button button-remove icc"></div>
    </div>
  </div>
</div>
