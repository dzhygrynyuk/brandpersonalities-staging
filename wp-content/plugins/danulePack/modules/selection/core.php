<?php
add_action( 'wp_ajax_fetchRecent', 'fetch_recent' );
add_action( 'wp_ajax_fetchOne', 'fetch_one' );
add_action( 'wp_ajax_getLinks', 'fetch_links' );
add_action( 'wp_ajax_makeUrl', 'make_url' );

function dsp($r,$x=false){
  global $siteurl;
  if(isset($r['image'])) $image = $siteurl.'/uploads/thumb/'.$r['image'];
  if($x) $class = 'active';
  if($r['type']=='post' || $r['type']=='page') $r['url'] = get_permalink($r['ID']);
  ob_start();
  ?>
  <li class="__item"
      data-id="<?php echo $r['ID']; ?>"
      data-name="<?php echo $r['name']; ?>"
      data-type="<?php echo $r['type']; ?>"
      <?php if(isset($r['url'])){ ?>data-url="<?php echo $r['url']; ?>"<?php } ?>
      >
    <a>
      <div class="jcheck <?php echo $class; ?> icc"></div>
      <span><?php echo $r['name']?></span>
      <?php if(isset($image)){ ?>
      <div class="thumb" style="background-image:url('<?php echo $image; ?>');"></div>
      <?php } ?>
    </a>
  </li>
  <?php
  return ob_get_clean();
}

function fetch_one(){
  global $wpdb;
  $text = $_POST['text'];
  if(!$text) $id = intval($_POST['id']);
  else $id = $_POST['id'];
  $source = $_POST['source'];

  if($text){
    $results = array('ID'=>'0','name'=>$id,'type'=>'url','url'=>$id);
  }
  else if($source == 'page' || $source == 'post'){
    $sql = "SELECT ID, post_title as name, post_type as type
    FROM wp_posts
    WHERE ID = %d";
    $sql = $wpdb->prepare($sql,array($id));
    $results = $wpdb->get_row($sql,ARRAY_A);
  }
  else if($source == 'slider'){
    $sql = "SELECT ID, name, 'slider' as type
    FROM slider
    WHERE ID = %d";
    $sql = $wpdb->prepare($sql,array($id));
    $results = $wpdb->get_row($sql,ARRAY_A);
  }
  else if($source == 'form'){
    $sql = "SELECT ID, name, 'form' as type
    FROM form
    WHERE ID = %d";
    $sql = $wpdb->prepare($sql,array($id));
    $results = $wpdb->get_row($sql,ARRAY_A);
  }
  $r = $results;
  echo dsp($r,true);
  wp_die();
}

function fetch_links(){
  global $wpdb;
  $search = $_POST['search'];
  $id = $_POST['id'];
  $source = $_POST['source'];

  $results = array();
  if($source == 'url'){
    if(filter_var($search, FILTER_VALIDATE_URL) !== FALSE ){
      $results[] = array('ID'=>'0','name'=>$search,'type'=>'url','url'=>$search);
    } else if(mb_substr($search,0,1)=='#') {
      $results[] = array('ID'=>'0','name'=>$search,'type'=>'url','url'=>$search);
    } else if (strpos($search, 'mailto:') !== false) {
      $results[] = array('ID'=>'0','name'=>$search,'type'=>'url','url'=>$search);
    } else {
      $sql = "SELECT ID, post_title as name, post_type as type
      FROM wp_posts
      WHERE (post_status = 'publish' OR post_status = 'draft')
      AND (post_type = 'page' OR post_type = 'post')
      AND post_title LIKE %s";
      $sql = $wpdb->prepare($sql,array('%'.$search.'%'));
      $results = $wpdb->get_results($sql,ARRAY_A);
    }
  }

  if($source == 'any'){
    if(filter_var($search, FILTER_VALIDATE_URL) !== FALSE ){
      $results[] = array('ID'=>'0','name'=>$search,'type'=>'url','url'=>$search);
    } else if(mb_substr($search,0,1)=='#') {
      $results[] = array('ID'=>'0','name'=>$search,'type'=>'url','url'=>$search);
    } else if (strpos($search, 'mailto:') !== false) {
      $results[] = array('ID'=>'0','name'=>$search,'type'=>'url','url'=>$search);
    } else {

      $sql = "SELECT ID, name, 'form' as type
          FROM form
          WHERE name LIKE %s";
      $_search = '%'.$search.'%';
      $sql = $wpdb->prepare($sql,array($_search));
      $results = $wpdb->get_results($sql,ARRAY_A);
    }
  }

  if($source == 'slider'){
    $sql = "SELECT ID, name, 'slider' as type
    FROM slider
    WHERE name LIKE %s";
    $sql = $wpdb->prepare($sql,array('%'.$search.'%'));
    $results = $wpdb->get_results($sql,ARRAY_A);
  }

  if($source == 'form'){
    $sql = "SELECT ID, name, 'form' as type
    FROM form
    WHERE name LIKE %s";
    $sql = $wpdb->prepare($sql,array('%'.$search.'%'));
    $results = $wpdb->get_results($sql,ARRAY_A);
  }

  $active = false;
  if(count($results)==1){$active = true;}
  foreach($results as $r){
    echo dsp($r,$active);
  }
  wp_die();
}

function make_url(){
  global $db, $siteurl;

  $id = $_POST['id'];
  $type = $_POST['type'];

  if($type=='url') {
    echo $id;
  } else {
    if($type == 'category'){
      $sql = 'SELECT slug FROM category WHERE ID = ?';
      $slug = $db->select_var($sql,array($id));
      $url = $siteurl.'/category/'.$slug;
    }
    if($type == 'page'){
      $sql = 'SELECT slug FROM page WHERE ID = ?';
      $slug = $db->select_var($sql,array($id));
      $url = $siteurl.'/'.$slug;
    }
    echo $url;
  }
}
?>
