//main code
function isEmpty(string){
  if(!string) return true;
  return /^\s+$/.test(string);
}

jQuery(document).ready(function( $ ) {
  var _, page, move, s;
  var _ = $('#sel');
  var settings, vars, current;

  //functions

  //load results while typing
  var timer;
  $('body').on('keyup','.searchField',function(e){
    clearTimeout(timer);
    timer = setTimeout(function() {
      var search = $('input',_).val();
      loadLinks(search);
    }, 200);
  });

  /*Search*/
  $('body').on('click','.button-search',function(e){
    var par = $(this).closest('.searchField');
    var search = $('input',par).val();
    loadLinks(search);
  });

  $('body').on('keydown','.searchField',function(e){
    var key = e.keyCode || e.which;
    var search = $('input',this).val();
    if(key==13){
      loadLinks(search);
    }
    if(key==27 && search){
      clearField($(this),true);
    }
  });

  $('body').on('click','.searchField .clearField',function(e){
    clearField($(this),true);
  });

  $('body').on('keypress','.searchField input',function(e){
    var par = $(this).closest('.searchField');
    var search = $('input',par).val();
    if(search) $('.clearField',par).show();
    else $('.clearField',par).hide();
  });

  function clearField(x,reload){
    var par = $('.searchField',_);
    var search = $('input',par);
    search.val('');
    search.focus();
    $('.clearField',par).hide();
    if(reload) loadLinks();
  }
  /*--*/

  /*jCheck*/
  $('body').on('click','.results li',function(){
    if($('.jcheck',this).hasClass('active')){
      var par = $(this);
      vars.checked = null;
      $('.jcheck',this).removeClass('active');
    } else {
      $('.results .jcheck',_).removeClass('active');
      var par = $(this);
      var id = par.attr('data-id');
      var type = par.attr('data-type');
      var name = par.attr('data-name');
      var url = par.attr('data-url');
      vars.checked = {id:id,name:name,type:type,url:url};
      $('.jcheck',this).addClass('active');
      $('#name',_).val(vars.checked.name);
    }
  });
  /*--*/

  function update_checked(ele, name){
    var id = ele.attr('data-id');
    var type = ele.attr('data-type');
    var name = ele.attr('data-name');
    var url = ele.attr('data-url');
    vars.checked = {id:id,name:name,type:type,url:url};
  }

  function loadLinks(search,load){
    var cont = $('.results',_);
    vars.checked = null;

    var data = {
      action: 'getLinks',
      source: settings.type
    };
    if(current && load){
      if(!isNaN(current.link_id)){
        data['id'] = current.link_id;
        data['type'] = current.link_type;
      }
    }
    if(search) data['search'] = search;
    if(isEmpty(search)) {
      fetchRecent();
      return;
    }
    //re-place in data
    $.post(ajaxurl, data, function(r){
      console.log('loaded links!');
      var par = $('.results ul',_);
      par.html(r);
      var ele = $('.jcheck.active:first',par).closest('li');
      if(ele.length>0){
        update_checked(ele);
        $('#name',_).val(vars.checked.name);
      }

      if(current){
        var id = current.link_id;
        var type = current.link_type;
        var name = current.link_name;
        var ele = $('li[data-id="'+id+'"][data-type="'+type+'"]',par);
        $('.jcheck',ele).addClass('active');
      }

      if (!r) {
        par.html('<div class="message">No results found</div>');
      }
    });
  }

  function fetchRecent(){
    var data = {
      action: 'fetchRecent',
      source: settings.type
    };
    $.post(ajaxurl, data, function(r){
      var par = $('.results ul',_);
      par.html(r);
    });
  }

  function fetchOne(id,type){
    var data = {
      action: 'fetchOne',
      id:id,
      source: settings.type
    };
    if(type) data['source'] = type;
    if(!type) data['text'] = true;
    $.post(ajaxurl, data, function(r){
      var par = $('.results ul',_);
      par.html(r);
      var ele = $('.jcheck.active:first',par).closest('li');
      if(ele.length>0){
        update_checked(ele);
      }
    });
  }

  $('body').on('click','.button-save',function(){
    vars.action = 'save';
    vars.checked.name = $('#name',_).val();
    $('body').trigger({
       type: "selection:updated",
       vars: vars
    });
  });

  $('body').on('click','.button-cancel',function(){
    vars.action = 'cancel';
    $('body').trigger({
       type: "selection:updated",
       vars: vars
    });
  });

  $('body').on('click','.button-remove',function(e){
    vars.action = 'save';
    vars.checked = {};
    vars.checked.id = null;
    vars.checked.name = null;
    vars.checked.type = null;
    vars.checked.url = null;
    $('body').trigger({
       type: "selection:updated",
       vars: vars
    });
  });

//---load
  settings = $global['settings'];
  vars = $global['vars'];
  s = {};
  settings.type='url';
  loadLinks(); //RE-ENABLE

  // function updateWin(e){
  //   if(e){
  //     var w = picker.width();
  //     picker.removeClass('reverse');
  //     $('#cal-win').css({
  //       left: e.pageX - w/2,
  //       top: e.pageY + 10
  //     }).show();
  //     var pos = $(window).height() - (e.pageY - $(window).scrollTop());
  //     var h = $('#cal-win .picker').height();
  //     if(pos <= h+20){
  //       picker.addClass('reverse');
  //       $('#cal-win').css({
  //         left: e.pageX - w/2,
  //         top: e.pageY - (h+20)
  //       }).show();
  //     }
  //   }
  // }



  $('#sel').on('win:open',function(e){
    console.log();
    $('.clearField',_).hide();
    $('.searchField input',_).val('');
    $('.results ul',_).html('');
    settings.type = e.ele.attr('source');

    var found = false;
    console.log($global.settings);
    if($global.settings.mode=='editor'){
      console.log('Mode: editor!');
      nodes = $global.nodes;
      var id, url, type;
      nodes.forEach(function(v,i) {
        if(v.tagName == 'A'){
          console.log('anchor link! Index: '+i);
          id = nodes[i].getAttribute('data-id');
          type = nodes[i].getAttribute('data-type');
          url = nodes[i].getAttribute('href');
          found = true;
          return;
        }
      });
    } else if($global.settings.mode=='block'){
      var block = $global.settings.block;
      var id = block.settings.link_id;
      var type = block.settings.link_type;
      var url = block.settings.link_url;
      var name = block.settings.link_name;
      $('#name',_).val(name);
      if(!isEmpty(id)) found = true;
    } else {
      var extra = $global.settings.editRow.attr('extra');
      if(extra) extra = "_"+extra;
      var id = $('input[data-label="id'+extra+'"]',$global.settings.editRow).val();
      var type = $('input[data-label="type'+extra+'"]',$global.settings.editRow).val();
      var url = $('input[data-label="url'+extra+'"]',$global.settings.editRow).val();
      var name = $('input[data-label="name'+extra+'"]',$global.settings.editRow).val();
      $('#name',_).val(name);
      if(!isEmpty(id)) found = true;
    }
    if(found) {
      if(id != '0') fetchOne(id,type);
      else {
        fetchOne(url);
      }
    } else {
      //nothing
    }

    var shft = $('.__sel').width() / 2;
    var offset = e.ele.offset();
    var w = $(e.ele).outerWidth() / 2;
    var h = $(e.ele).height();
    var h_this = $(this).height();

    if((offset.top + h_this + 20) > $(window).height()){
      var top = ($(window).height()/2) - (h_this/2) + 'px';
      $(this).addClass('reverse');
    } else {
      var top = (offset.top - $(window).scrollTop())+h+'px';
      $(this).removeClass('reverse');
    }

    $(this).css({
      left:offset.left-shft+w+'px',
      top:top
    });

    $(this).show();
    $('.searchField input',_).focus();
  });

  $('#sel.__window').appendTo($('body'));
  console.log($('#sel.__window'));

  //Trigger Code
  // $('.__sel').trigger({
  //    type: "win:open",
  //    x:e.pageX,
  //    y:e.pageY
  // });

});
