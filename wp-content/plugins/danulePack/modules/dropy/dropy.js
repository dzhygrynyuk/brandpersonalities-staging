/**
 * Dropy
 */

(function($) {

 	var settings = {};
 	//default options
 	var defaults = {
    openClass:'open',
    selectClass:'selected'
 	};

 	var methods = {
    init : function(options) {
	    return this.each(function(n) {
        var settings = $.extend(true, {}, defaults, options);
        $(this).data("settings", settings);

        $(this).append('<dt class="dropy__title"><span></span></dt>');
        $(this).append('<input type="hidden">');

        $(this).find('li').wrapAll('<ul></ul>');
        $(this).find('ul').wrap('<dd class="dropy__content"></dd>');

        var sel = $(this).attr('data-value');
        if(sel){
          var opt = $('li[data-value*='+sel+']:first',this);
          opt.addClass(settings.selectClass);
          $('.dropy__title span',this).html(opt.text());
          $('input',this).val(sel);
        } else {
          $('.dropy__title span',this).html('Select...');
        }
	    });
	  }
 	};

 	$.fn.dropy = function(method) {
 		var _s; //settings object;
 		//boilerplate
 	  if (methods[method]) {
 	  	return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
 	  } else if (typeof method === 'object' || !method) {
 			//if initialize
 	  	var init = methods.init.apply(this, arguments);
 			var settings = $(this).data("settings");
 			var _this = $(this);
 			_s = settings;

 		//--EVENTS--//

    _this.on('click','.dropy__title',function(){
      var par = $(this).closest('.dropy');
      if(!par.hasClass(_s.openClass)){
        $('.dropy').removeClass(_s.openClass);
        par.addClass(_s.openClass);
      } else {
        par.removeClass(_s.openClass);
      }
    });

    // Click on a dropy list
    _this.on('click','.dropy__content ul li',function(){
      var $that = $(this);
      var $dropy = $that.parents('.dropy');
      var $input = $dropy.find('input');
      var $title = $(this).parents('.dropy').find('.dropy__title span');

      // Remove selected class
      $('.dropy__content li',_this).removeClass(_s.selectClass);

      // Update selected value
      $title.html($that.html());
      $input.val($that.attr('data-value')).trigger('change');
      console.log($input.val());

      // If back to default, remove selected class else addclass on right element
      if($that.hasClass('dropy__header')){
        $title.removeClass(_s.selectClass);
        $title.html($title.attr('data-title'));
        $input.val('');
      }
      else{
        $title.addClass(_s.selectClass);
        $that.addClass(_s.selectClass);
      }

      // Close dropdown
      $dropy.removeClass(_s.openClass);
    });

    // Close all dropdown onclick on another element

    $(document).on('click', function(e){
      if(!_s) return;
      if (! $(e.target).parents().hasClass('dropy')){ _this.removeClass(_s.openClass); }
    });

 		//---//

 			return init;
 	  } else {
 	  	$.error('Method ' + method + ' does not exist');
 	  }

 	//--FUNCTIONS--//


 	//--//
 	};

})(jQuery);




/*$(function(){
  dropy.init();
});*/
