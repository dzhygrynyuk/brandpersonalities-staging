<div class="__settings __window">
  <!-- <div class="window_close icc winclose"></div> -->
  <div class="scroll-box">
    <span class="window_title">Settings</span>
    <div class="__content"></div>
  </div>
  <div class="action-bar">
    <div class="button icc __save button-primary"></div>
    <div class="button icc __delete"></div>
    <div class="button icc __cancel"></div>
  </div>
</div>

<style>
.__settings.active {display:block;}
.__settings {
  display:none;
  box-sizing: border-box;
	position: fixed;
	width:100%;
  max-width: 300px;
	height:auto;
	background-color:white;
	z-index:9999;
  box-shadow:0px 0px 15px 0px rgba(0,0,0,0.75);

  overflow-y: auto;
  max-height: calc(100% - 0%);
  top: 50%;
  left: 50%;
  transform: translate(-50%,-50%);
  -ms-transform: translate(-50%,-50%);
  -webkit-transform: translate(-50%,-50%);
}
.__settings .window_title {
  margin:0;padding:0;
  font-weight:700;
  font-size: 20px;
  background-color:#e0e0e0;
  padding:10px;
  float:left;
  margin-bottom:20px;
}
.__settings .scroll-box {
  height: 100%;
  width: 100%;
  box-sizing: border-box;
  float: left;
  padding:20px 20px 80px 20px;
  overflow-y: auto;
  overflow-x: hidden;
}
.__settings .action-bar {
  position: relative;
  width: 100%;
  left: 0;
  height: 40px;
  box-sizing: border-box;
  padding: 5px;
  float: left;
  border-top:solid 1px #e3e3e3;
}
.__settings .action-bar .button {
  width:30px;
  height:30px;
}
.__settings .action-bar .button.icc:before {
  margin-top:1px;
}
.__settings .action-bar .__save {
  float:right;
}
.__settings .button {
  position:relative;
}
.__settings .__save:before {content:'\f00c';}
.__settings .__delete:before {content:'\f1f8';}
.__settings .__cancel:before {content:'\f00d';}
.button.__delete.selected {
  background-color:#ea432a;
  color:white;
  border-color:#d33a26;
}
.window_close {
	cursor:pointer;
	width:30px;
	height:30px;
	background-color:black;
	color:white;
	top:0;
	right:0;
	position:absolute;
	transition:all .25s ease-in-out;
	-moz-transition:all .25s ease-in-out;
	-webkit-transition:all .25s ease-in-out;
}
.window_close:hover {
	background-color: #4f4f4f;
}
.window_close:before {
	content:'\f00d';
}
.__settings input,
.__settings textarea {
  position:relative;
  box-sizing: border-box;
  width:100%;
  margin:0;
  padding:5px;
  left:0;
  box-shadow: none;
  height:100%;
}
.__settings textarea {
  height:80px;
}
.__settings .table {
  margin-bottom: 5px;
}






</style>
