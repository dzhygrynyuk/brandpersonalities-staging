<div class="text_editor __window">
  <div class="scroll-box">
    <span class="window_title">Enter Text</span>
    <div class="window_close icc winclose"></div>
    <div class="__editor">
      <textarea id="mytextarea"></textarea>
    </div>
    <div class="button button-primary __save winclose"><div>Save</div></div>
    <!-- <div class="button __log"><div>Log contents</div></div> -->
    <!-- <div class="button __add"><div>Add contents</div></div> -->
    <!-- <div class="button __selection"><div>Get Selection</div></div> -->
  </div>
</div>

<style>
/*.modal-backdrop {display:none!important;}*/
.text_editor.__window {
  display: none;
  box-sizing: border-box;
  position: fixed;
  width: 100%;
  max-width: 70%;
  height: auto;
  background-color: white;
  z-index: 9999;
  box-shadow: 0px 0px 15px 0px rgba(0,0,0,0.75);
  overflow-y: auto;
  max-height: calc(100% - 0%);
  top: 50%;
  left: 50%;
  transform: translate(-50%,-50%);
  -ms-transform: translate(-50%,-50%);
  -webkit-transform: translate(-50%,-50%);
}
.text_editor .scroll-box {
  height: 100%;
  width: 100%;
  box-sizing: border-box;
  float: left;
  padding:20px 20px 20px 20px;
  overflow-y: auto;
  overflow-x: hidden;
}
.text_editor .window_title {
  margin:0;padding:0;
  font-weight:700;
  font-size: 20px;
  background-color:#e0e0e0;
  padding:10px;
  float:left;
  margin-bottom:20px;
}
.text_editor .__editor {
  width:100%;
  float:left;
}
.window_close {
	cursor:pointer;
	width:30px;
	height:30px;
	background-color:black;
	color:white;
	top:0;
	right:0;
	position:absolute;
	transition:all .25s ease-in-out;
	-moz-transition:all .25s ease-in-out;
	-webkit-transition:all .25s ease-in-out;
}
.window_close:hover {
	background-color: #4f4f4f;
}
.window_close:before {
	content:'\f00d';
}

.text_editor .button {
  cursor:pointer;
  position:relative;
  box-sizing: border-box;
  height:100%;
  text-align:center;
  float:left;
  padding:5px 15px;
	height:40px;
  font-weight: 600;
  background-color:#e0e0e0;
  transition: all .25s ease-in-out;
  -moz-transition: all .25s ease-in-out;
  -webkit-transition: all .25s ease-in-out;
}
.text_editor .button:after {
  content:'';
  display:block;
  position:absolute;
  bottom:0;
  left:0;
  width:100%;
  height:3px;
  background-color:rgba(0,0,0,0.2);
}
.text_editor .button div {
  position:relative;
  top:50%;
  transform: translateY(-50%);
  -ms-transform: translateY(-50%);
  -webkit-transform: translateY(-50%);
}
.text_editor .button-primary {
	float:right;
  background-color:#11aadd;
  color:white;
}
</style>
