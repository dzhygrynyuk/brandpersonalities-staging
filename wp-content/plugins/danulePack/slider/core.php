<?php

add_action( 'wp_ajax_sliderSave', 'save_slider' );
add_action( 'wp_ajax_sliderList', 'get_list_slider' );
add_action( 'wp_ajax_sliderLoad', 'load_slider' );
add_action( 'wp_ajax_sliderDelete', 'delete_slider' );

function delete_slider(){
  global $wpdb;

  $id = intval($_POST['id']);
  $wpdb->delete( 'slider', array( 'ID' => $id ) );

  wp_die();
}

function load_slider(){
  global $wpdb;

  $id = intval($_POST['id']);

  $sql = "SELECT *
  FROM slider WHERE ID = %d";
  $sql = $wpdb->prepare($sql,array($id));
  $slider = $wpdb->get_row($sql,ARRAY_A);
  echo json_encode($slider);

  wp_die();
}

function get_list_slider(){
  global $wpdb;

  $sql = "SELECT ID, slug, name
  FROM slider
  ORDER BY name ASC";
  $sql = $wpdb->prepare($sql,array());
  $list = $wpdb->get_results($sql,ARRAY_A);

  echo json_encode($list);
  wp_die();
}

function save_slider(){
  global $wpdb;

  $content = json_encode($_POST['content'],JSON_UNESCAPED_SLASHES);
  $name = $_POST['name'];
  $slug = slugify($name);
  $pass=false;

  $id = intval($_POST['content']['settings']['id']);
  if($id>0){
    //update
    while($pass===false){
      $data = array(
        'name'=>$name,
        'slug'=>$slug,
        'content'=>$content
      );
      $pass = $wpdb->update('slider', $data, array('ID'=>$id));
      if($pass===false){
        $slug = slugify_dupe('slider',$slug,$id);
      }
    }
    echo json_encode(array('ID'=>$id,'slug'=>$slug));
  } else {
    //insert
    while($pass==false){
      $data = array(
        'name'=>$name,
        'slug'=>$slug,
        'content'=>$content
      );
      $pass = $wpdb->insert('slider', $data);
      if($pass==false){
        $slug = slugify_dupe('slider',$slug);
      }
    }
    echo json_encode(array('ID'=>$wpdb->insert_id,'slug'=>$slug));
  }

  wp_die();
}
?>
