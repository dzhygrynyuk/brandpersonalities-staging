<div class="wrap">

  <div class="side">

    <div class="fixed">
      <div class="button icl __new_slider"><div class="_c">New Slider</div></div>
      <div class="loader spin icc"></div>
    </div>

    <div class="scroll">
      <ul id="sideItems"></ul>
    </div>
    <div class="scrollOverlay"></div>
  </div>

  <div class="main">

    <div class="save-row">
      <input type="text" id="slider_name" placeholder="Slider Name">
      <div class="button button-primary icr __save"><div class="_c">Save</div></div>
    </div>

    <div class="action-row">
      <div class="button icl __media"><div class="_c">Add Media</div></div>
      <div class="button button-delete icl __delete"><div class="_c">Delete Slider</div></div>

      <div class="label-row">
        <div class="label"><div class="_c">Items per:</div></div>
        <select id="slider-items-count">
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
          <option value="6">6</option>
        </select>
      </div>

      <div class="type-select label-row">
        <div class="label"><div class="_c">Type:</div></div>
        <select id="slider-type">
          <option value="image_slider">Image Slider</option>
          <option value="item_slider">Item Slider</option>
          <option value="testimonial">Testimonial</option>
        </select>
      </div>
    </div>

    <div class="dsp_slug"></div>

    <div class="loader spin icc"></div>

    <div id="items">
      <ul id="sortable"></ul>
    </div>

  </div>


</div>
