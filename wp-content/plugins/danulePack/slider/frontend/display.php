<?php

function slider_shortcodes(){
  add_shortcode('slider', 'slider_dsp');
}
add_action( 'init', 'slider_shortcodes');

function slider_dsp($atts, $content = null) {
  extract(shortcode_atts(array(
    'slug' => null
  ), $atts));

  if(!isset($slug)) return;

  global $wpdb;

  $sql = "SELECT content FROM slider WHERE slug=%s";
  $sql = $wpdb->prepare($sql,array($slug));
  $content = $wpdb->get_var($sql);

  display_slider($settings,$atts,$content);
}

function display_slider($settings, $atts=null, $content=null){
  global $wpdb;

  if(!isset($content)){
    $id = intval($settings['link_id']);
    $sql = "SELECT content FROM slider WHERE ID=%d";
    $sql = $wpdb->prepare($sql,array($id));
    $content = $wpdb->get_var($sql);
  }

  $slider = json_decode($content,true);

  $set = $slider['settings'];
  $type = $set['type'];
  $scale = 'false';
  if($set['type']=="image_slider_scale"){
    $set['type']="image_slider";
    $scale = 'true';
  }
  $image = 'false';
  if($set['type']=="item_slider_image"){
    $set['type']="item_slider";
    $image = 'true';
  }

  ob_start(); ?>
  <div class="slider-wrap"
    data-type="<?php echo $set['type']; ?>"
    >
    <div class="dot-nav"></div>
    <div class="overlay">
      <div class="arrow left"></div>
      <div class="arrow right"></div>
    </div>

    <div class="owl-carousel owl-theme"
      data-items="<?php echo $set['items']; ?>"
      data-type="<?php echo $set['type']; ?>"
      data-scale="<?php echo $scale; ?>"
      data-image="<?php echo $image; ?>"
      >
    <?php
    $i = 0;
    foreach($slider['content'] as $s){
      $_s = $s['settings'];
      $img_desktop = wp_get_attachment_image_src($_s['id'],'full')[0];
      $img_mobile = array_key_exists('mobile_id', $_s) ? wp_get_attachment_image_src($_s['mobile_id'],'full')[0] : '';

      ?>
      <?php if($type=="image_slider"){ //[START] image_slider ?>

        <div
        class="item slide-item"
        data-bgrnd="<?php echo $img_desktop; ?>"
        data-bgrnd-mobile="<?php echo $img_mobile; ?>"
        style="/*background-image:url('<?php echo $img_desktop; ?>')*/">
          <img src="<?php echo $img_desktop; ?>">
          <?php if(!empty($_s['text'])) { ?>
            <div class="slide-float">
              <div class="sliderInner">

                <?php if (!empty($_s['text'])){ ?>
                  <div class="text sliderText" style="color:#<?php echo $_s['text_color']; ?>"><?php echo stripslashes($_s['text']); ?></div>
                <?php } ?>
                <?php if (!empty($_s['link_url_main'])){ ?>
                  <a class="button-link" href="<?php echo $_s['link_url_main']; ?>">
                    <div class="button sub-button"><div><?php echo stripslashes($_s['link_name_main']); ?></div></div>
                  </a>
                <?php } ?>
                <?php if ($_s['link_type_main'] == 'form'){ ?>
                  <?php
                    $sql = "SELECT slug FROM form WHERE ID = %s";
                    $sql = $wpdb->prepare($sql,array($_s['link_id_main']));
                    $slug = $wpdb->get_var($sql);
                    //embed form
                    echo display_form_core($slug,array('style'=>'window'),false);
                  ?>
                  <div class="button form-button" data-form="<?php echo $slug ?>"><div><?php echo stripslashes($_s['link_name_main']); ?></div></div>
                <?php } ?>

                <?php if (!empty($_s['link_url_sub'])){ ?>
                  <a class="button-link" href="<?php echo $_s['link_url_sub']; ?>">
                    <div class="button"><div><?php echo stripslashes($_s['link_name_sub']); ?></div></div>
                  </a>
                <?php } ?>
                <?php if ($_s['link_type_sub'] == 'form'){ ?>
                  <?php
                    $sql = "SELECT slug FROM form WHERE ID = %s";
                    $sql = $wpdb->prepare($sql,array($_s['link_id_sub']));
                    $slug = $wpdb->get_var($sql);
                    //embed form
                    echo display_form_core($slug,array('style'=>'window'),false);
                  ?>
                  <div class="button form-button" data-form="<?php echo $slug ?>"><div><?php echo stripslashes($_s['link_name_sub']); ?></div></div>
                <?php } ?>

              </div>
            </div>
          <?php } ?>
        </div>

      <?php } //[END] image_slider ?>

      <?php if($type=="testimonial"){ ?>
        <div
        class="item item-slider"
        data-bgrnd="<?php echo $_s['bgrnd_color']; ?>"
        data-index="<?php echo $i; ?>"
        style="background-color:#<?php echo $_s['bgrnd_color']; ?>;
        background-image:url('<?php //echo wp_get_attachment_image_src($_s['id'],'large')[0]; ?>')">
          <?php if(!empty($_s['text'])) { ?>
            <div class="wrap">
              <div class="inner-content">
                <?php $img = wp_get_attachment_image_src($_s['id'],'medium')[0] ?>
                <div class="testimonial-image" style="background-image:url('<?php echo $img; ?>')"></div>

                <div class="testimonial-text-cont textCell text">
                  <?php if (!empty($_s['text'])){ ?>
                    <?php echo stripslashes($_s['text']); ?>
                  <?php } ?>
                </div>
              </div>
            </div>
            <div class="wash"></div>
          <?php } ?>
          </div>
      <?php } ?>

      <?php if($type=="item_slider"){ ?>
        <div
        class="item"
        <?php if (isset($_s['bgrnd_color'])) { echo 'data-bgrnd="' . $_s['bgrnd_color'] . '"'; } ?>
        data-index="<?php echo $i; ?>"
        style="">
        <img src="<?php echo wp_get_attachment_image_src($_s['id'],'medium')[0]; ?>">
        </div>

      <?php } ?>

    <?php $i++; } ?>
    </div>
  </div>
  <?php return ob_get_clean();

}


?>
