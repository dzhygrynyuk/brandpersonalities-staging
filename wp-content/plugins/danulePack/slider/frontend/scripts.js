var mobile;

jQuery(document).ready(function($){ //WordPress

  function is_mobile(){
    if (window.matchMedia("(max-width: 800px)").matches) {
      mobile = true;
    } else {
      mobile = false;
    }
  }
  is_mobile();

  var resizeTimer;
  $(window).on('resize',function(){
    clearTimeout(resizeTimer);
    resizeTimer = setTimeout(function() {
      is_mobile();
    }, 250);
  });

  $('.owl-carousel').each(function(i,v){

    var $this = $(this);
    var par = $(this).closest('.slider-wrap');
    var items = parseInt($this.attr('data-items'));
    if(!items) items = 1;
    var slides = parseInt($('.item',$this).length);
    console.log(slides);
    // var autoplay = false;
    var autoplay = (slides>1) ? true : false;
    var dots = (slides/items>1) ? true : false;
    var dots_mob = (slides>1) ? true : false;
    var drag = (slides>1) ? true : false;
    var type = $this.attr('data-type');
    var extra = $this.attr('data-extra');
    if(extra=='coupons') autoplay = false;
    var autoHeight = false;
    if(type=='testimonial'){
      autoHeight = true;
      // $('.slide-item',$this).css('height','auto');
    }
    if(type=='item_slider') dots = false;
    if(extra=='coupons' && !mobile){
      $('.arrow',par).hide();
      drag = false;
    }

    var animSpeed = 500;

    par.on('click','.arrow.right',function() {
      if($(this).hasClass('disabled')) return;
      $this.trigger('next.owl.carousel',animSpeed);
    });
    par.on('click','.arrow.left',function() {
      if($(this).hasClass('disabled')) return;
      $this.trigger('prev.owl.carousel',animSpeed);
    });

    $this.on('initialized.owl.carousel',function(event){
      $this.trigger('refresh.owl.carousel')
    });

    $this.owlCarousel({
      loop:true,
      // lazyLoad:true,
      autoHeight: autoHeight,
      margin:0,
      items:items,
      dots:dots,
      dotsContainer:$('.dot-nav',par),
      autoplay:autoplay,
      autoplayTimeout:5000,
      autoplaySpeed:animSpeed,
      autoplayHoverPause:true,
      mouseDrag:drag,
      touchDrag:drag,
      responsive : {
        0 : { //mobile
          items:1,
          dots:false
        },
        800 : { //default
          items:items,
          dots:dots
        },
      }
    });

    $this.imagesLoaded().progress( function( instance, image ) {
      var item = $(image.img).closest('.item');
      var index = $('.item',$this).index(item);
      $this.trigger('refresh.owl.carousel');
    });

  });




}); //WordPress
