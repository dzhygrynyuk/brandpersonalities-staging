var $global = {};
$global.settings = {};
$global.vars = {};

String.prototype.stripSlashes = function(){
    return this.replace(/\\(.)/mg, "$1");
}

Array.prototype.move = function (old_index, new_index) {
    if (new_index >= this.length) {
        var k = new_index - this.length;
        while ((k--) + 1) {
            this.push(undefined);
        }
    }
    this.splice(new_index, 0, this.splice(old_index, 1)[0]);
    return this; // for testing purposes
};

jQuery(document).ready(function( $ ) {

  var s = {};

  function load_template(x,data){
    var html = ejs.render(templates[x],data);
    return html;
  }

  function findBlock_1D(s,index,remove){
    index = parseInt(index);
    var block = s.pagearr.content[index];
    if(remove) s.pagearr.content.splice(index,1);
    return block;
  }

  function updateIndexes_1D(s){
    var block = s.pagearr;
    for(var i=0; i<block.content.length; i++){
      block.content[i].settings.index = i;
    }
  }

  function displayAll(s){
    s.page.html('');
    for(var k=0; s.pagearr.content.length>k; k++){
      $.each(s.pagearr.content[k].settings,function(i,v){
        if(v){
          if(typeof v == 'string'){
            s.pagearr.content[k].settings[i] = v.stripSlashes(); //STRIP SLASHES FOR ALL STORED VALUES
          }
        }
      });
      var template = load_template('item',s.pagearr.content[k].settings);
      s.page.append(template);
    }
  }

  function loadSideItems(){
    $('#sideItems').html('');
    $('.side .loader').addClass('active');
    var data = {
      action: 'sliderList'
    };
    $.post(ajaxurl, data, function(r){
      r = JSON.parse(r);
      console.log(r);
      s.sliderList = r;
      $.each(s.sliderList,function(i,v){
        var html = load_template('sideItem',v);
        $('#sideItems').append(html);
      });
      $('.side .loader').removeClass('active');
      if("id" in s.pagearr.settings){
        var parentDiv = $('.side .scroll');
        var innerListItem = $('#sideItems li[data-id="'+s.pagearr.settings.id+'"]');
        innerListItem.addClass('active');
        parentDiv.scrollTop(parentDiv.scrollTop() + (innerListItem.position().top - parentDiv.position().top) - (parentDiv.height()/2) + (innerListItem.height()/2)  );
      }
      // $('#sideItems li:first').click(); //remove this
    });
  }

  $('body').on('click','.action-row .__delete',function(){
    if(! s.pagearr.settings.id) return;
    var data = {
      action: 'sliderDelete',
      id: s.pagearr.settings.id
    };
    $.post(ajaxurl, data, function(r){
      s.pagearr = {
        content:[],
        settings:{}
      }
      s.page.html('');
      $('#slider_name').val('');
      loadSideItems();
    });
  });

  $('body').on('click','.__new_slider',function(){
    $('#slider_name').val('');
    $('#slider_name').focus();
    s.pagearr = {
      content:[],
      settings:{}
    }
    s.page.html('');
    $('#sideItems li').removeClass('active');
  });

  $('body').on('click','.save-row .__save',function(){
    if($(this).hasClass('disabled')) return;
    $(this).addClass('disabled');
    $(this).addClass('active');

    s.pagearr.settings.type = $('.action-row #slider-type').val();
    s.pagearr.settings.items = $('.action-row #slider-items-count').val();
    console.log(s.pagearr.settings);

    var name = $('#slider_name').val();
    console.log(s.pagearr);
    var data = {
      action: 'sliderSave',
      content: s.pagearr,
      name: name
    };
    var $this = $(this);
    $.post(ajaxurl, data, function(r){
      r = JSON.parse(r);
      s.pagearr.settings.id = r.ID;
      $('.dsp_slug').html('[slider slug="'+r.slug+'"][/slider]');
      console.log(r);
      console.log(s.pagearr);
      $this.removeClass('disabled');
      setTimeout(function(){
        $this.removeClass('active');
      },600);
      loadSideItems();
    });
  });

  $('body').on('click','.control.__del',function(){
    var index = $(this).closest('.list-item').attr('data-index');
    s.editing = findBlock_1D(s,index,true);
    updateIndexes_1D(s);
    displayAll(s);
  });

  $('body').on('click','#sideItems li',function(i,v){
    if($(this).hasClass('active')) return;

    $('#sideItems li').removeClass('active');
    $(this).addClass('active');
    s.page.html('');
    $('.main .loader').addClass('active');

    var id = $(this).attr('data-id');
    var data = {
      action: 'sliderLoad',
      id: id
    };
    var $this = $(this);
    $.post(ajaxurl, data, function(r){
      r = JSON.parse(r);
      $('.dsp_slug').html('[slider slug="'+r.slug+'"][/slider]');
      s.pagearr = JSON.parse(r.content);
      console.log(s.pagearr);
      $('#slider_name').val(r.name);
      if(!s.pagearr) s.pagearr = {};
      if(!("settings" in s.pagearr)) {
        s.pagearr.settings = {};
      }
      if(!("content" in s.pagearr)) {
        s.pagearr.content = [];
      }
      s.pagearr.settings.id = r.ID;
      if(!("type" in s.pagearr.settings)) s.pagearr.settings.type = 'image_slider';
      if(!("items" in s.pagearr.settings)) s.pagearr.settings.items = 1;
      $('.action-row #slider-type').val(s.pagearr.settings.type);
      $('.action-row #slider-items-count').val(s.pagearr.settings.items);
      updateIndexes_1D(s);
      displayAll(s);
      $('.main .loader').removeClass('active');
    });
  });

//--text
  $('body').on('click','.control.__text',function(e){
    if($(e.target).hasClass('rmv')) return;

    var index = $(this).closest('.list-item').attr('data-index');
    s.editing = findBlock_1D(s,index);
    $('.text_editor').fadeIn(250);
    if(s.editing.settings.text){
      $global.editor.setContent(s.editing.settings.text);
    } else {
      $global.editor.setContent('');
    }
  });

  $('body').on('click','.text_editor .__save',function(){
    var text = $global.editor.getContent();
    s.editing.settings.text = text;
    displayAll(s);
  });

  $('body').on('click','.winclose',function(){
    if($(this).is('.button-disabled, .disabled')) return;
    var open = $('.__window:visible').length;
    var win = $(this).closest('.__window');
    if(open == 1){
      $('#blackout').fadeOut(250);
      win.fadeOut(250);
    } else {
      win.hide();
    }
  });

//--linking
$('body').on('click','.__link',function(e){
  if($(e.target).hasClass('rmv')) return;

  var index = $(this).closest('.list-item').attr('data-index');
  s.editing = findBlock_1D(s,index);
  $global.settings.mode = 'block';
  $global.settings.block = s.editing;
  $('#sel').trigger({
     type: "win:open",
     ele: $(this)
  });
});

$('body').on('click','.__selector',function(e){
  $global.settings.mode = 'default';
  s.editRow = $(this).closest('.tCell');
  console.log('EDIT ROW:');
  console.log(s.editRow.html());
  $global.settings.editRow = s.editRow;
  $('#sel').trigger({
     type: "win:open",
     ele: $(this)
  });
});

$('body').on('click','.control .rmv',function(e){
  var index = $(this).closest('.list-item').attr('data-index');
  s.editing = findBlock_1D(s,index);

  var par = $(this).closest('.control');
  if(par.hasClass('__link')) {
    delete s.editing.settings.link_id;
    delete s.editing.settings.link_url;
    delete s.editing.settings.link_name;
    delete s.editing.settings.link_type;
  }
  if(par.hasClass('__text')) delete s.editing.settings.text;
  if(par.hasClass('__image')) {
    delete s.editing.settings.id;
    delete s.editing.settings.url;
  }
  displayAll(s);
});

//--images
  $('body').on('click','.__image',function(e){
    if($(e.target).hasClass('rmv')) return;

    s.gallery_context = $(this).attr('gallery-context');

    if(s.gallery_context == 'normal'){
      var index = $(this).closest('.list-item').attr('data-index');
      s.editing = findBlock_1D(s,index);
    } else if(s.gallery_context == 'input') {
      s.editingRow = $(this).closest('.tRow');
    }
    show_gallery();
  });

  //---WP Gallery
  var frame;
  function show_gallery(){
    // If the media frame already exists, reopen it.
    if (frame) {
        frame.open();
        return;
    }
    // Create a new media frame
    frame = wp.media({
        title: 'Select Image',
        button: {
            text: 'Choose'
        },
        multiple: false // Set to true to allow multiple files to be selected
    });
    // When an image is selected in the media frame...
    frame.on('select', function() {
      // Get media attachment details from the frame state
      var attachment = frame.state().get('selection').first().toJSON();

      if(s.gallery_context == 'normal'){
        s.editing.settings.id = attachment.id;

        if('thumbnail' in attachment.sizes) {
          s.editing.settings.url = attachment.sizes.thumbnail.url;
        } else {
          s.editing.settings.url = attachment.sizes.full.url;
        }

        displayAll(s);
      }
      if(s.gallery_context == 'input'){
        $('input[data-label="url"]',s.editingRow).val(attachment.url);
        $('input[data-label="id"]',s.editingRow).val(attachment.id);
        console.log(s.editingRow);
      }
    });
    // Finally, open the modal on click
    frame.open();
  }

  $('body').on('click','.__media',function(){
    show_gallery_m();
  });

  //---WP Gallery (Multiple)
  var frameM;
  function show_gallery_m(){
    // If the media frame already exists, reopen it.
    if (frameM) {
        frameM.open();
        return;
    }
    // Create a new media frame
    frameM = wp.media({
        title: 'Select Image(s)',
        button: {
            text: 'Choose'
        },
        multiple: true
    });
    // When an image is selected in the media frame...
    frameM.on('select', function() {
        // Get media attachment details from the frame state
        var attachment = frameM.state().get('selection').toJSON();
        console.log(attachment);

        $.each(attachment,function(i,v){
          var id = v.id;
          if('thumbnail' in v.sizes) {
            var url = v.sizes.thumbnail.url;
          } else {
            var url = v.sizes.full.url;
          };
          console.log(id+':'+url);
          var item = $.extend(true, {}, s.object.item);
          item.settings = {id:id,url:url};
          s.pagearr.content.push(item);
        });

        updateIndexes_1D(s);
        console.log(s.pagearr);
        displayAll(s);

    });
    // Finally, open the modal on click
    frameM.open();
  }

  //---TinyMCE
  tinymce.init({
    selector: '#mytextarea',
    theme: 'modern',
    convert_urls: false,
    relative_urls: false,
    plugins: 'link lists',
    menubar: false,
    height: 400,
    toolbar: 'formatselect bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | removeformat | bullist numlist | blockquote | undo redo | mybutton unlink',
    setup: function (editor) {
      editor.addButton('mybutton', {
        text: false,
        icon: 'link',
        onclick: function () {
          $global.settings.mode = 'editor';
          $global.nodes = editor.dom.getParents(editor.selection.getNode());
          $global.editor = editor;
          $('#sel').trigger({
             type: "win:open",
             ele: this.$el
          });
        },
        onpostrender: function() {
          var btn = this;
          editor.on('init', function() {
            $global.editor = editor;
            btn.$el.attr('source','url');
          });
        }
      });
    }
  });

  //--selection
  $('body').on('selection:updated',function(e){
    if(e.vars.action=='cancel'){
      $('#sel').hide();
      return;
    }

    console.log(e.vars);
    var checked = e.vars.checked;

    var replaced = false;
    if($global.settings.mode=='editor'){
      nodes = $global.nodes;
      nodes.forEach(function(v,i) {
        console.log(v.tagName);
        if(v.tagName == 'A'){
          console.log('anchor link! Index: '+i);
          nodes[i].setAttribute('href', checked.url);
          nodes[i].setAttribute('data-mce-href', checked.url);
          nodes[i].setAttribute('data-id', checked.id);
          nodes[i].setAttribute('data-type', checked.type);
          replaced = true;
          return;
        }
      });
      if(!replaced){
        var text = $global.editor.selection.getContent();
        $global.editor.selection.setContent("<a href='"+checked.url+"' data-id='"+checked.id+"' data-type='"+checked.type+"'>"+text+"</a>");
      }
    } else {
      console.log('CHECKED ITEMS:');
      console.log(checked);
      var extra = s.editRow.attr('extra');
      if(extra) extra = "_"+extra;
      $('input[data-label="id'+extra+'"]',s.editRow).val(checked.id);
      $('input[data-label="name'+extra+'"]',s.editRow).val(checked.name);
      $('input[data-label="type'+extra+'"]',s.editRow).val(checked.type);
      $('input[data-label="url'+extra+'"]',s.editRow).val(checked.url);
      // s.editing.settings.link_id = checked.id;
      // s.editing.settings.link_name = checked.name;
      // s.editing.settings.link_type = checked.type;
      // s.editing.settings.link_url = checked.url;
      displayAll(s);
    }

    $('#sel').hide();
  });

  function menuToggle(x,show){
    $('.__delete',x).removeClass('selected');
    if(x.is(':visible') && show!=true){
      x.removeClass('active');
      x.hide();
    }
    else if(!x.is(':visible') && show!=false){
      x.addClass('active');
      x.show();
    }
  }

  function menuLoad(x,y,data){

    // $('.loader',y).addClass('visible');
    var extra = x+'_extra';
    x = x+'_settings';
    if(!data) var data = {};
    var part = ejs.render(templates['settings'],data);
    $('.__content',y).html(part);
    $('.loader',y).removeClass('visible');
    jscolor.installByClassName("jscolor");
  }

  $('body').on('click','.__set',function(){
    console.log('things');

    var index = $(this).closest('.list-item').attr('data-index');
    s.editing = findBlock_1D(s,index);
    block = s.editing;

    var menu = $('.__settings');
    menuToggle(menu,true);
    if(menu.hasClass('active')){
      menuLoad('',menu,block.settings);
    }
  });

  $('body').on('click','.__settings .__save',function(){
    var menu = $('.__settings.__window');

    var block = s.editing;

    $(':input',menu).each(function(index,data) {
      if($(this).closest('.tRow').hasClass('double')){
        var name = $(this).closest('.tCell').attr('value');
      } else {
        var name = $(this).closest('.table').find('.tCell:first').attr('value');
      }
      var value = $(this).val();
      var label = $(this).attr('data-label');
      if(label) name = name+'_'+label;
      block.settings[name] = value;
      console.log(block);
    });

    menuToggle(menu,false);
  });

  $('.__delete','.__settings').hide();

  $('body').on('click','.__settings .__cancel',function(){
    var menu = $('.__settings.__window');
    menuToggle(menu,false);
  });

  //---load
  $('body').on('slider_ready', function() {

    s = {};
    s.object = {};
    s.page = $('#sortable');

    s.pagearr = {
      content:[],
      settings:{
        type:'image_slider'
      }
    }

    s.object.item = {
      type:'slide',
      settings:{
        index:null,
        id:null,
        url:null
      }
    }

    loadSideItems();

    console.log(templates);
    //--sortable
    var el = document.getElementById("sortable");
    Sortable.create(el, {
      animation: 150,
      handle: ".handle",
      onStart: function (evt) {
        var itm = evt.item;
        s.index = evt.oldIndex;
      },
      onEnd: function (evt) {
        var itm = evt.item;
        s.pagearr.content.move(s.index,evt.newIndex);
        updateIndexes_1D(s);
        displayAll(s);
      },
    });
  });

});
