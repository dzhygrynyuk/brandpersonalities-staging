<?php
//--AJAX / Files
if(is_admin()){
	include 'editor/core.php';
	include 'view/core.php';
}

include 'frontend/display.php';

function form_load_frontend() {
	wp_enqueue_style( 'form_style', plugins_url( '/', __FILE__ ) .	'frontend/style.css?v=1.10' );
	wp_enqueue_script( 'validate', plugins_url( '/', __FILE__ ) .	'frontend/jquery.validate.min.js', array('jquery') );
	wp_enqueue_script( 'form_script', plugins_url( '/', __FILE__ ) .	'frontend/scripts.js?v=1.08', array('jquery','validate') );
	wp_enqueue_script( 'form_stripe', 'https://js.stripe.com/v3/', array('jquery') );
	$ajaxurl = get_bloginfo('wpurl').'/wp-admin/admin-ajax.php';
	wp_localize_script( 'form_script', 'ajaxurl', $ajaxurl);
}
add_action( 'wp_enqueue_scripts', 'form_load_frontend' );

//---------SETTINGS PAGE

function form_styles() {

  if (isset($_GET['page'])) :
  	if($_GET['page'] == 'form' || $_GET['page'] == 'form_submissions' ){
  		wp_enqueue_style( 'dropy', plugins_url( '/../', __FILE__ ) . 'modules/dropy/dropy.css' );
  		wp_enqueue_script( 'dropy', plugins_url( '/../', __FILE__ ) . 'modules/dropy/dropy.js', array('jquery') );
  		wp_enqueue_script( 'form_ejs', plugins_url( '/', __FILE__ ) . 'js/ejs.min.js', array('jquery') );
  		wp_enqueue_script( 'form_sortable', plugins_url( '/', __FILE__ ) . 'js/sortable.min.js', array('jquery') );
  	}
  	if($_GET['page']=='form'){
  		wp_enqueue_style( 'form_style', plugins_url( '/', __FILE__ ) . 'editor/style.css');

  		wp_enqueue_script( 'form_tmp', plugins_url( '/', __FILE__ ) . 'editor/templates/_main.js', array('jquery') );
  		wp_enqueue_script( 'form_script', plugins_url( '/', __FILE__ ) . 'editor/scripts.js', array('jquery') );
  		wp_localize_script( 'form_tmp', 'plugins_url', plugins_url( '/', __FILE__ ) );
  	}
  	if($_GET['page']=='form_submissions'){
  		wp_enqueue_style( 'form_style', plugins_url( '/', __FILE__ ) . 'view/style.css');

  		wp_enqueue_script( 'form_tmp', plugins_url( '/', __FILE__ ) . 'view/templates/_main.js', array('jquery') );
  		wp_enqueue_script( 'form_script', plugins_url( '/', __FILE__ ) . 'view/scripts.js', array('jquery') );
  		wp_localize_script( 'form_tmp', 'plugins_url', plugins_url( '/', __FILE__ ) );
  	}
  endif;
}
add_action( 'admin_enqueue_scripts', 'form_styles' );


// add the admin options page
function form_admin_add_pages() {
	//Main menu item
	add_menu_page('Forms', 'Forms', 'manage_options', 'form', 'form_options_page','dashicons-feedback');
	add_submenu_page( 'form', 'Form Submissions', 'Submissions', 'manage_options', 'form_submissions', 'form_submissions_page' );
}
add_action('admin_menu', 'form_admin_add_pages');

//MAIN
function form_options_page() {
  global $core;
  wp_enqueue_media();

  // include $core.'modules/selection/display_window.php';
  // include $core.'modules/window/text.php';
  include 'editor/display.php';
}

//SUBMISSIONS
function form_submissions_page(){
	global $core;
	include 'view/display.php';
}

?>
