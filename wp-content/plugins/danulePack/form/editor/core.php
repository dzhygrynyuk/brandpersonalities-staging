<?php

add_action( 'wp_ajax_formSave', 'save_form' );
add_action( 'wp_ajax_formList', 'get_list_form' );
add_action( 'wp_ajax_formLoad', 'load_form' );
add_action( 'wp_ajax_formDelete', 'delete_form' );

function delete_form(){
  global $wpdb;

  $id = intval($_POST['id']);
  $wpdb->delete( 'form', array( 'ID' => $id ) );

  wp_die();
}

function load_form(){
  global $wpdb;

  $id = intval($_POST['id']);

  $sql = "SELECT *
  FROM form WHERE ID = %d";
  $sql = $wpdb->prepare($sql,array($id));
  $form = $wpdb->get_row($sql,ARRAY_A);
  echo json_encode($form);

  wp_die();
}

function get_list_form(){
  global $wpdb;

  $sql = "SELECT ID, slug, name
  FROM form
  ORDER BY name ASC";
  $sql = $wpdb->prepare($sql,array());
  $list = $wpdb->get_results($sql,ARRAY_A);

  echo json_encode($list);
  wp_die();
}

function save_form(){
  global $wpdb;

  $content = json_encode($_POST['content'],JSON_UNESCAPED_SLASHES);
  $name = $_POST['name'];
  $slug = slugify($name);
  $pass=false;

  $id = intval($_POST['content']['settings']['id']);
  $notify = $_POST['content']['settings']['notifications'];
  $stripe_enable = $_POST['content']['settings']['stripe_enable'];
  $stripe_enable = $stripe_enable == 'true' ? 1 : 0;
  $stripe_price = $_POST['content']['settings']['stripe_price'];

  // remove everything except a digit "0-9", a comma ",", and a dot "."
  $stripe_price = preg_replace('/[^\d,\.]/', '', $stripe_price);
  // replace the comma with a dot, in the number format ",12" or ",43"
  $stripe_price = preg_replace('/,(\d{2})$/', '.$1', $stripe_price);


  $emails = explode(',',$notify);
  $emails = array_filter(array_map('trim', $emails));
  $emails = json_encode($emails,JSON_UNESCAPED_SLASHES);

  if($id>0){
    //update
    while($pass===false){
      $data = array(
        'name'=>$name,
        'slug'=>$slug,
        'content'=>$content,
        'stripe_enable' => $stripe_enable,
        'stripe_price' => $stripe_price,
        'notification'=>$emails
      );
      $pass = $wpdb->update('form', $data, array('ID'=>$id));
      if($pass===false){
        $slug = slugify_dupe('form',$slug,$id);
      }
    }
    echo json_encode(array('ID'=>$id,'slug'=>$slug));
  } else {
    //insert
    while($pass==false){
      $data = array(
        'name'=>$name,
        'slug'=>$slug,
        'content'=>$content,
        'stripe_enable' => $stripe_enable,
        'stripe_price' => $stripe_price,
        'notification'=>$emails
      );
      $pass = $wpdb->insert('form', $data);
      if($pass==false){
        $slug = slugify_dupe('form',$slug);
      }
    }
    echo json_encode(array('ID'=>$wpdb->insert_id,'slug'=>$slug));
  }

  wp_die();
}
?>
