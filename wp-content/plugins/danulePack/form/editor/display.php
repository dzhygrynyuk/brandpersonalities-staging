<div class="wrap">

  <div class="side">

    <div class="fixed">
      <div class="button icl __new_form"><div class="_c">New Form</div></div>
      <div class="loader spin icc"></div>
    </div>

    <div class="scroll">
      <ul id="sideItems"></ul>
    </div>
    <div class="scrollOverlay"></div>
  </div>

  <div class="main" id="form">

    <div class="save-row">
      <input type="text" id="form_name" placeholder="Form Name">
      <div class="button button-primary icr __save"><div class="_c">Save</div></div>
    </div>

    <div class="action-row">
      <div class="button button-delete icl __delete"><div class="_c">Delete Form</div></div>

      <div class="dsp_slug"></div>

    </div>

    <div class="loader-overlay">
      <div class="loader-contain">
        <div class="loader spin icc"></div>
      </div>
    </div>

    <div class="zone">
      <div class="boxpanel modules">
        <div class="inner">
          <ul id="pageItems">
            <li class="fa capsule drag-item" data-type="input" data-tmp="input">Short Text</li>
            <li class="fa capsule drag-item" data-type="textarea" data-tmp="input">Long Text</li>
            <!--<li class="fa capsule drag-item" data-type="radio" data-tmp="multiple">Multiple Choice</li>-->
            <li class="fa capsule drag-item" data-type="checkbox" data-tmp="multiple">Checkboxes</li>
            <li class="fa capsule drag-item" data-type="select" data-tmp="multiple">Drop-down</li>
          </ul>
        </div>

        <div class="frame_box stripe">
          <div class="stripe-active">ACTIVE</div>
          <h1>Stripe Payment</h1>
          <p></p>
          <input type="checkbox" id="stripe_enable" data-setting="stripe_enable">
          <label for="stripe_enable">Enable</label>
          <div class="form-label">Price</div>
          <input type="text" id="stripe_price" data-setting="stripe_price">
        </div>

        <div class="frame_box mailchimp">
          <h1>Mail Chimp</h1>
          <p></p>
          <input type="checkbox" id="mailchimp_enable" data-setting="mailchimp_enabled">
          <label for="mailchimp_enable">Enable</label>
          <div class="form-label">API Key</div>
          <input type="text" id="mailchimp_api" data-setting="mailchimp_api">
          <div class="form-label">List ID</div>
          <input type="text" id="mailchimp_list" data-setting="mailchimp_list">
        </div>
        <!-- <div class="frame_box notes">
          <h1>Sub-Note</h1>
          <input type="text" id="note" data-setting="note">
        </div> -->
        <div class="frame_box notify">
          <h1>Send Notifications to:</h1>
          <span>List of e-mail addresses, separated by commas</span>
          <input type="text" id="notify" data-setting="notifications">
        </div>
      </div>

      <div class="boxpanel editor">
        <div class="inner">
          <div id="drop">

          </div>
        </div>
      </div>
    </div>

  </div>


</div>
