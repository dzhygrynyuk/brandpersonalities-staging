var $global = {};
$global.settings = {};
$global.vars = {};

String.prototype.stripSlashes = function(){
    return this.replace(/\\(.)/mg, "$1");
}

Array.prototype.move = function (old_index, new_index) {
    if (new_index >= this.length) {
        var k = new_index - this.length;
        while ((k--) + 1) {
            this.push(undefined);
        }
    }
    this.splice(new_index, 0, this.splice(old_index, 1)[0]);
    return this; // for testing purposes
};

jQuery(document).ready(function( $ ) {

  var s = {};

  function load_template(x,data){
    $.each(data,function(i,v){
      data[i] = v=='false' ? false : v=='true' ? true : v;
    });
    var html = ejs.render(templates[x],data);
    return html;
  }

  function findBlock_1D(s,index,remove){
    var block = s.pagearr;
    var blockparent = s.pagearr;
    if (index.indexOf(',') > -1){
      var indexes = index.split(',');
    } else {
      indexes = [index];
    }
    for(var i=0; i<indexes.length; i++){
      blockparent=block.content;
      block=block.content[indexes[i]];
      if(remove && i==indexes.length-1){
        blockparent.splice(indexes[i],1);
      }
    }
    return block;
  }

  function updateIndexes_1D(s){
    var block = s.pagearr;
    var frame = block.content;
    for(var i=0; i<frame.length; i++){
      frame[i].settings.index = i;
      if(frame[i].tmp != 'input'){
        for(var k=0; k<frame[i].content.length; k++){
          frame[i].content[k].settings.index = i+','+k;
        }
      }
    }
  }

  function displayAll(s){
    s.page.html('');
    var frame = s.pagearr.content;
    for(var k=0; frame.length>k; k++){
      var data = $.extend(true, {}, frame[k].settings);
      data.type = frame[k].type;
      var template = load_template(frame[k].tmp, data);
      $(template).appendTo(s.empty);
      if(frame[k].settings.validate){
        var html = load_template('_extra',frame[k].settings);
        var extra = $('.module-extra',s.empty);
        extra.html(html);
      }
      if(frame[k].tmp!='input'){
        for(var j=0; frame[k].content.length>j; j++){
          var arr = frame[k].content[j].settings;
          arr['type'] = frame[k].type;
          var option = load_template('_option', arr);
          $(option).appendTo($('.module-inputs',s.empty));
        }
      }
      s.page.append(s.empty.contents());
      s.empty.empty();
    }

    if($('.module-box',s.page).length>0){
      var divs = document.getElementsByClassName("module-inputs");
      for(var i=0; i<divs.length; i++){
        Sortable.create(divs.item(i), {
          animation: 150,
          handle: ".input-handle",
          onStart: function (evt) {
            var itm = evt.item;
            s.index = evt.oldIndex;
          },
          onEnd: function (evt) {
            var itm = evt.item;
            var boxIndex = $(itm).closest('.module-box').attr('data-index');
            s.pagearr.content[boxIndex].content.move(s.index,evt.newIndex);
            updateIndexes_1D(s);
            displayAll(s);
          },
        });
      }
      $('.dropy',s.page).dropy();
    }
  }

  function init_default(){
    var item = $.extend(true, {}, s.object.item);
    item.type = 'input';
    item.tmp = 'input';
    item.settings.name = 'First Name';
    item.settings.collapsed = true;
    item.settings.required = true;
    item.settings.perma = true;
    item.settings.identity = 'first_name';
    s.pagearr.content.push(item);

    var item = $.extend(true, {}, s.object.item);
    item.type = 'input';
    item.tmp = 'input';
    item.settings.name = 'Last Name';
    item.settings.collapsed = true;
    item.settings.required = true;
    item.settings.perma = true;
    item.settings.identity = 'last_name';
    s.pagearr.content.push(item);

    var item = $.extend(true, {}, s.object.item);
    item.type = 'input';
    item.tmp = 'input';
    item.settings.name = 'E-Mail';
    item.settings.validate = 'email';
    item.settings.identity = 'email';
    item.settings.collapsed = true;
    item.settings.required = true;
    item.settings.perma = true;
    item.settings.identity = 'email';
    s.pagearr.content.push(item);
  }

  //---Delete / Add / Save / Load

  function loadSideItems(){
    $('#sideItems').html('');
    $('.side .loader').addClass('active');
    var data = {
      action: 'formList'
    };
    $.post(ajaxurl, data, function(r){
      r = JSON.parse(r);
      console.log(r);
      s.formList = r;
      $.each(s.formList,function(i,v){
        var html = load_template('sideItem',v);
        $('#sideItems').append(html);
      });
      $('.side .loader').removeClass('active');
      if("id" in s.pagearr.settings){
        var parentDiv = $('.side .scroll');
        var innerListItem = $('#sideItems li[data-id="'+s.pagearr.settings.id+'"]');
        innerListItem.addClass('active');
        parentDiv.scrollTop(parentDiv.scrollTop() + (innerListItem.position().top - parentDiv.position().top) - (parentDiv.height()/2) + (innerListItem.height()/2)  );
      }
      // $('#sideItems li:first').click(); //remove this
    });
  }

  $('body').on('click','.action-row .__delete',function(){
    if(! s.pagearr.settings.id) return;
    var data = {
      action: 'formDelete',
      id: s.pagearr.settings.id
    };
    $.post(ajaxurl, data, function(r){
      s.pagearr = {
        content:[],
        settings:{}
      }
      s.page.html('');
      $('#form_name').val('');
      loadSideItems();
    });
  });

  $('body').on('click','.__new_form',function(){
    $('#form_name').val('');
    $('#form_name').focus();
    s.pagearr = {
      content:[],
      settings:{}
    }
    s.page.html('');

    $('.frame_box input').val('');
    $('.frame_box :input').prop('checked',false);
    $('.frame_box').removeClass('active');

    init_default();
    displayAll(s);

    $('#sideItems li').removeClass('active');
  });

  $('body').on('click','.save-row .__save',function(){
    if($(this).hasClass('disabled')) return;
    $(this).addClass('disabled');
    $(this).addClass('active');

    s.pagearr.settings.type = $('.action-row #slider-type').val();
    s.pagearr.settings.items = $('.action-row #slider-items-count').val();
    console.log(s.pagearr.settings);

    var name = $('#form_name').val();
    console.log(s.pagearr);
    var data = {
      action: 'formSave',
      content: s.pagearr,
      name: name
    };
    var $this = $(this);
    $.post(ajaxurl, data, function(r){
      r = JSON.parse(r);
      console.log('saved it.');
      console.log(r);
      s.pagearr.settings.id = r.ID;
      $('.dsp_slug').html('[form slug="'+r.slug+'"][/form]');
      $this.removeClass('disabled');
      setTimeout(function(){
        $this.removeClass('active');
      },600);
      loadSideItems();
    });
  });

  $('body').on('click','#sideItems li',function(i,v){
    if($(this).hasClass('active')) return;

    $('.frame_box input').val('');
    $('.frame_box :input').prop('checked',false);
    $('.frame_box').removeClass('active');

    $('#sideItems li').removeClass('active');
    $(this).addClass('active');
    s.page.html('');
    $('.main .loader-overlay').addClass('active');

    var id = $(this).attr('data-id');
    var data = {
      action: 'formLoad',
      id: id
    };
    var $this = $(this);
    $.post(ajaxurl, data, function(r){
      r = JSON.parse(r);
      $('.dsp_slug').html('[form slug="'+r.slug+'"][/form]');
      s.pagearr = JSON.parse(r.content);

      if(!s.pagearr) s.pagearr = {};
      if(!("settings" in s.pagearr)) {
        s.pagearr.settings = {};
      }
      if(!("content" in s.pagearr)) {
        s.pagearr.content = [];
      }

      if("note" in s.pagearr.settings) $('#note').val(s.pagearr.settings.note);
      if("notifications" in s.pagearr.settings) $('#notify').val(s.pagearr.settings.notifications);
      $('#form_name').val(r.name);

      if(s.pagearr.settings.mailchimp_enabled=='true'){
        $('.mailchimp #mailchimp_enable').prop('checked',true);
      }
      $('#mailchimp_list').val(s.pagearr.settings.mailchimp_list);
      $('#mailchimp_api').val(s.pagearr.settings.mailchimp_api);

      if(s.pagearr.settings.stripe_enable=='true'){
        $('.stripe #stripe_enable').prop('checked',true);
        $('.stripe').addClass('active');
      } else {
        $('.stripe').removeClass('active');
      }
      $('#stripe_price').val(s.pagearr.settings.stripe_price);

      s.pagearr.settings.id = r.ID;
      updateIndexes_1D(s);
      displayAll(s);
      $('.main .loader-overlay').removeClass('active');
    });
  });

  //---


  $('body').on('click','.module-box .collapsable',function(){
    var par = $(this).closest('.module-box');
    s.editing = par.attr('data-index');
    var block = findBlock_1D(s,s.editing);
    if($('.module-content',par).hasClass('active')){
      $('.module-content',par).slideToggle(250);
      $('.module-content',par).removeClass('active');
      $('.collapsable',par).removeClass('active');
      block.settings.collapsed = true;
    } else {
      $('.module-content',par).slideToggle(250);
      $('.module-content',par).addClass('active');
      $('.collapsable',par).addClass('active');
      block.settings.collapsed = false;
    }
  });

  $('body').on('focus','.module-box input',function(){
    var par = $(this).closest('.drop');
    $('.inp',par).removeClass('active');
    $(this).closest('.inp').addClass('active');
  });

  $('body').on('blur','.module-box input',function(){
    var par = $(this).closest('.module-box');
    $(this).closest('.inp').removeClass('active');
    var setting = $(this).closest('.inp').attr('data-setting');
    if(setting=='content'){
      s.editing = $(this).closest('.inp').attr('data-index');
      var block = findBlock_1D(s,s.editing);
      block.settings['name'] = $(this).val();
    }
    else if(setting){
      s.editing = par.attr('data-index');
      var block = findBlock_1D(s,s.editing);
      block.settings[setting] = $(this).val();
      if(setting=='name') $('.name .__label',par).html($(this).val());
    }
  });

  $('body').on('click','.miniButton',function(){
    if($('.module-menu',this).length>0){
      if($(this).hasClass('active')){
        $(this).removeClass('active');
        $('.module-menu',this).slideToggle(200);
      } else {
        $(this).addClass('active');
        $('.module-menu',this).slideToggle(200);
      }
    }
  });

  $('body').on('click','.__addOption',function(){
    var par = $(this).closest('.module-box');
    s.editing = par.attr('data-index');
    var block = findBlock_1D(s,s.editing);
    var option = $.extend(true, {}, s.object.option);
    block.content.push(option);
    updateIndexes_1D(s);
    displayAll(s);
  });

  $('body').on('click','.module-box .inp .remove',function(){
    var option = $(this).closest('.inp');
    s.editing = option.attr('data-index');
    var block = findBlock_1D(s,s.editing);
    var block = findBlock_1D(s,s.editing,true); //remove block
    updateIndexes_1D(s);
    displayAll(s);
  });

  $('body').on('click','.miniButton.__delete',function(){
    var par = $(this).closest('.module-box');
    s.editing = par.attr('data-index');
    var block = findBlock_1D(s,s.editing,true); //remove block
    updateIndexes_1D(s);
    displayAll(s);
  });

  $('body').on('click','.inp[data-type="checkbox"] .jcheck',function(){
    var index = $(this).closest('.inp').attr('data-index');
    var block = findBlock_1D(s,index);
    if($(this).hasClass('active')){
      block.settings.checked = false;
    } else {
      block.settings.checked = true;
    }
    console.log(block);
  });

  $('body').on('click','.miniButton.__required',function(){
    if($('.jcheck',this).hasClass('disabled')) return;
    var par = $(this).closest('.module-box');
    s.editing = par.attr('data-index');
    var block = findBlock_1D(s,s.editing);
    if($('.jcheck',this).hasClass('active')){
      block.settings.required = true;
    } else {
      block.settings.required = false;
    }
  });

  $('body').on('click','.miniButton.__half',function(){
    if($('.jcheck',this).hasClass('disabled')) return;
    var par = $(this).closest('.module-box');
    s.editing = par.attr('data-index');
    var block = findBlock_1D(s,s.editing);
    if($('.jcheck',this).hasClass('active')){
      block.settings.half = true;
    } else {
      block.settings.half = false;
    }
  });

  $('body').on('click','.module-menu li',function(){
    var validate = $(this).attr('data-value');
    var par = $(this).closest('.module-box');

    var index = par.attr('data-index');
    var block = findBlock_1D(s,index);
    block.settings.validate = validate;
    console.log(block);

    var html = load_template('_extra', block.settings);
    var extra = $('.module-extra',par);
    $('.module-extra',par).html(html);
  });

  $('body').on('click','.jcheck',function(){
    if($(this).hasClass('disabled')) return;
    if($(this).hasClass('active')){
      $(this).removeClass('active');
    } else {
      $(this).addClass('active');
    }
  });

  $('body').on('change','.stripe input',function(){
    var par = $(this).closest('.frame_box');
    if($(this).attr('type') == 'checkbox'){
      var val = $(this).prop('checked');
      if(val){
        par.addClass('active');
      } else {
        par.removeClass('active');
      }
    } else {
      var val = $(this).val();
    }
    var setting = $(this).attr('data-setting');
    s.pagearr.settings[setting] = val;
    console.log(s.pagearr.settings);
  });

  $('body').on('change','.mailchimp input',function(){
    if($(this).attr('type') == 'checkbox'){
      var val = $(this).prop('checked');
    } else {
      var val = $(this).val();
    }
    var setting = $(this).attr('data-setting');
    s.pagearr.settings[setting] = val;
    console.log(s.pagearr.settings);
  });

  $('body').on('change','.notes input',function(){
    if($(this).attr('type') == 'checkbox'){
      var val = $(this).prop('checked');
    } else {
      var val = $(this).val();
    }
    var setting = $(this).attr('data-setting');
    s.pagearr.settings[setting] = val;
  });

  $('body').on('change','.notify input',function(){
    var setting = $(this).attr('data-setting');
    var val = $(this).val();
    console.log(val);
    s.pagearr.settings[setting] = val;
  });








  //---load
  $('body').on('form_ready', function() {

    s = {};
    s.object = {};
    s.page = $('#drop');
    s.empty = $('<div></div>').appendTo($('body')).hide();

    s.pagearr = {
      content:[],
      settings:{
        type:'image_slider'
      }
    }

    s.object.item = {
      type:'input',
      settings:{
        name:'untitled',
        index:null,
        collapsed:false,
        required:false,
        options:false
      },
      content:[]
    }

    s.object.option = {
      type:'option',
      settings:{
        index:null,
        name:null
      }
    }

    init_default();

    loadSideItems();

    updateIndexes_1D(s);
    displayAll(s);



    var el = document.getElementById("pageItems");
    Sortable.create(el, {
      group: {name:"form", pull:"clone", put:false},
      sort: false,
    });

    var index;
    var el = document.getElementById("drop");
    Sortable.create(el, {
      group: {name:"form", pull:false, put:true},
      animation: 150,
      handle: ".handle",
      onChoose: function (evt) {
        var itm = evt.item;
        $('.module-content',itm).hide();
        $('.collapsable',itm).removeClass('active');
      },
      onAdd: function (evt) {
        var itm = evt.item;
        var type = $(itm).attr('data-type');
        var tmp = $(itm).attr('data-tmp');
        var item = $.extend(true, {}, s.object.item);
        var option = $.extend(true, {}, s.object.option);
        item.type = type;
        item.tmp = tmp;
        if(tmp != 'input'){
          item.content.push(option);
        }
        var _index = $(itm).index();
        s.pagearr.content.splice(_index, 0, item);
        updateIndexes_1D(s);
        displayAll(s);
      },
      onStart: function (evt) {
        var itm = evt.item;
        index = evt.oldIndex;
        $('.module-content',itm).hide();
        $('.collapsable',itm).removeClass('active');
      },
      onEnd: function (evt) {
        s.pagearr.content.move(index,evt.newIndex);
        updateIndexes_1D(s);
        displayAll(s);
      },
    });
  });

});
