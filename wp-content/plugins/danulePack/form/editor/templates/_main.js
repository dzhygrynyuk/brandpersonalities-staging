var templates = {};
jQuery(document).ready(function( $ ) {

  var dir = plugins_url+'editor/templates/';
  var files = ['input','multiple','_option','_extra','sideItem'];

  var len = files.length - 1;
  $.each(files,function(i,v){
    var f = dir+v+'.ejs?v='+Date.now();
    $.get(f, function (file) {
      templates[v] = file;
      if(i==len) {
        $('body').trigger('form_ready');
      }
    });
  });
});
