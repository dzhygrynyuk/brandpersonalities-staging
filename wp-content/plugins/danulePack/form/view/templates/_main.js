var templates = {};
jQuery(document).ready(function( $ ) {

  var dir = plugins_url+'view/templates/';
  var files = ['item','submission','pages'];

  var len = files.length - 1;
  $.each(files,function(i,v){
    var f = dir+v+'.ejs?v='+Date.now();
    $.get(f, function (file) {
      templates[v] = file;
      if(i==len) {
        $('body').trigger('form_submissions_ready');
      }
    });
  });
});
