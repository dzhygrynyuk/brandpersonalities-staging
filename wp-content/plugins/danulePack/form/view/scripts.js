var $global = {};
$global.settings = {};
$global.vars = {};

jQuery(document).ready(function( $ ) {
  var s;

  function load_template(x,data){
    $.each(data,function(i,v){
      data[i] = v=='false' ? false : v=='true' ? true : v;
    });
    var html = ejs.render(templates[x],data);
    return html;
  }

  function list_submissions(){
    var data = {
      action : 'submissionsList',
      page : s.page,
      limit : 10
    }
    if(s.search){
      data.search=s.search;
      // s.page = 1;
      // data.page = s.page;
    }
    $.post(ajaxurl, data, function(r) {
      page.html('');
      r = $.parseJSON(r);
      console.log(r);
      var html = load_template('pages',{pages:r.pages,page:s.page});
      $('.pages').html(html);
      $('.page[data-page="'+s.page+'"]').addClass('active disabled');
      $.each(r.results,function(i,v){
        var html = load_template('item',v);
        page.append(html);
      });
    });
  }

  $('body').on('click','.flex-row.action',function(){
    var $this = $(this);
    var id = $(this).attr('data-id');
    var formid = $(this).attr('form-id');
    var par = $('.slide-panel#info');

    $('.loader',par).show();
    $('.slide-panel#main').removeClass('active');
    $('.slide-panel#info').addClass('active');

    var data = {
      action : 'submissionGet',
      id : id
    }
    $.post(ajaxurl, data, function(r) {
      $('.loader',par).hide();
      r = $.parseJSON(r);
      console.log(r);
      var html = load_template('submission',{fields:r.data,name:r.name});
      $('.dyncont',par).html(html);
      $('#form-name div',par).html(r.name);
    });
  });

  $('body').on('click','.__back',function(){
    $('.slide-panel#info').removeClass('active');
    $('.slide-panel#main').addClass('active');
  });

  /*Search*/
  $('body').on('click','.button-search',function(e){
    var par = $(this).closest('.searchField');
    s.search = $('input',par).val();
    s.page = 1;
    list_submissions();
  });

  $('body').on('keydown','.searchField',function(e){
    var key = e.keyCode || e.which;
    s.search = $('input',this).val();
    if(key==13){
      s.page = 1;
      list_submissions();
    }
    if(key==27 && s.search){
      clearField($(this),true);
    }
  });

  $('body').on('click','.searchField .clearField',function(e){
    clearField($(this),true);
  });

  $('body').on('keypress','.searchField input',function(e){
    var par = $(this).closest('.searchField');
    var search = $('input',par).val();
    if(search) $('.clearField',par).show();
    else $('.clearField',par).hide();
  });

  $('body').on('click','.pages .page',function(){
    if($(this).hasClass('disabled')) return;
    if($(this).hasClass('page-arrow')){
      if($(this).hasClass('_more')) s.page += 1;
      if($(this).hasClass('_less')) s.page -= 1;
    } else {
      s.page = parseInt($('span',this).html());
    }
    list_submissions();
  });

  function clearField(x,reload){
    s.search = null;
    s.page = 1;
    var par = $('.searchField');
    var search = $('input',par);
    search.val('');
    search.focus();
    $('.clearField',par).hide();
    if(reload) list_submissions();
  }
  /*--*/

  //---load
  $('body').on('form_submissions_ready', function() {
    page = $('#dyncont');
    s = {};
    s.page = 1;
    s.search = null;
    list_submissions();
  });
});
