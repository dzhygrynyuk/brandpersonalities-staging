<div class="wrap">
  <div class="slide-panel active" id="main">
  <div class="searchField">
    <div class="clearField icc"></div>
    <input type="text" placeholder="search">
    <div class="button-search icc"></div>
  </div>

  <div class="pages"></div>

  <div class="flex">
    <div class="flex-row heading">
      <div class="flex-cell">Name</div>
      <div class="flex-cell">E-Mail</div>
      <div class="flex-cell">Form</div>
      <div class="flex-cell">Date</div>
    </div>
    <div id="dyncont"></div>
  </div>
  </div>

  <div class="slide-panel right" id="info">
    <div class="top-bar">
      <div class="button __back"><div class="icl _c">Back</div></div>
      <div id="form-name"><div class="_c">Hey</div></div>
    </div>
    <div class="content-area">
      <div class="loader icc"></div>
      <div class="dyncont"></div>
    </div>
  </div>
</div>
