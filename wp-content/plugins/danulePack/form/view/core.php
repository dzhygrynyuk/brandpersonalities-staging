<?php

add_action( 'wp_ajax_submissionsList', 'list_submissions' );
add_action( 'wp_ajax_submissionGet', 'get_submission' );

function get_submission(){
  global $wpdb;
  $id = intval($_POST['id']);
  $sql = "SELECT s.form_id as form_id, s.content as content, f.name as name
  FROM form_submission s LEFT JOIN form f ON s.form_id = f.ID
  WHERE s.ID=%d";
  $sql = $wpdb->prepare($sql,array($id));
  $sub = $wpdb->get_row($sql,ARRAY_A);

  $sub['content'] = json_decode($sub['content'],true);

  $sql = "SELECT content FROM form WHERE ID=%d";
  $sql = $wpdb->prepare($sql, array(intval($sub['form_id'])) );
  $form = $wpdb->get_var($sql);

  $form = json_decode($form,true);

  $res = $sub['content'];
  foreach($form['content'] as $c){
    $_s = $c['settings'];
    if(!array_key_exists($_s['name'],$res)){
      $res[$_s['name']] = null;
    }
  }

  echo json_encode(array('data'=>$res,'name'=>$sub['name']));
  wp_die();
}

function count_submissions($limit){
  global $wpdb;
  $search = $_POST['search'];

  if(isset($search)){
    $sql = "SELECT COUNT(*)
    FROM form_submission s LEFT JOIN form f ON s.form_id = f.ID
    WHERE s.first LIKE %s OR s.last LIKE %s OR s.email LIKE %s OR f.name LIKE %s";
    $_search = '%'.$search.'%';
    $sql = $wpdb->prepare($sql, array($_search,$_search,$_search,$_search));
    $count = $wpdb->get_var($sql);
  } else {
    $sql = "SELECT COUNT(*) FROM form_submission";
    $sql = $wpdb->prepare($sql, array($_search,$_search,$_search));
    $count = $wpdb->get_var($sql);
  }
  return ceil(intval($count)/$limit);
}

function list_submissions(){
  global $wpdb;
  $search = $_POST['search'];

  $limit = $_POST['limit'];
  $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
  $offset = ($page - 1) * $limit;

  if(isset($search)){
    $sql = "SELECT CONCAT(s.first,' ',s.last) as full_name, s.email as email, s.ID as ID, s.form_id as form_id, s.date as date, f.name as name
    FROM form_submission s LEFT JOIN form f ON s.form_id = f.ID
    WHERE s.first LIKE %s OR s.last LIKE %s OR s.email LIKE %s OR f.name LIKE %s
    ORDER BY date DESC LIMIT $limit OFFSET $offset";
    $_search = '%'.$search.'%';
    $sql = $wpdb->prepare($sql,array($_search,$_search,$_search,$_search));
    $results = $wpdb->get_results($sql,ARRAY_A);
  } else {
    $sql = "SELECT CONCAT(s.first,' ',s.last) as full_name, s.email as email, s.ID as ID, s.form_id as form_id, s.date as date, f.name as name
    FROM form_submission s LEFT JOIN form f ON s.form_id = f.ID
    ORDER BY date DESC LIMIT $limit OFFSET $offset";
    $sql = $wpdb->prepare($sql,array());
    $results = $wpdb->get_results($sql,ARRAY_A);
  }

  $i=0;
  foreach($results as $r){
    $r['date_dsp'] = date("jS M Y (h:ia)",strtotime($r['date']));
    $results[$i] = $r;
    $i++;
  }

  $pages = count_submissions($limit);
  echo json_encode(array('results'=>$results,'pages'=>$pages));
  wp_die();
}


?>
