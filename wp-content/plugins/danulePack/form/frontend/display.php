<?php

//make the payment
function make_payment($form,$email){
  global $wpdb;

  require_once(dirname(__DIR__).'/stripe/init.php');

  $stripe = array(
    "secret_key"      => "",
    "publishable_key" => ""
  );

  \Stripe\Stripe::setApiKey($stripe['secret_key']);

  $card = $_POST['card'];
  $token = $card['id'];

  $return = array();

  $price = floatval($form['stripe_price']);

  $_price = ($price + .30) / (1 - 0.0175);
  $fee = $_price - $price;
  $price = $_price;
  $price = number_format((float)$price, 2, '.', '');
  $return['price'] = $price;
  $fee = number_format((float)$fee, 2, '.', '');
  $price = intval(round($price*100));

  try {
    $customer = \Stripe\Customer::create(array(
      "email" => $email,
      "source"  => $token
    ));
    $charge = \Stripe\Charge::create(array(
      "customer" => $customer->id,
      "amount" => $price,
      "currency" => "aud",
      "description" => $form['name']
      // "source" => $token
    ));
  } catch (Exception $e) {
    echo $e;
    //error
    $return['success'] = false;
  }

  //success
  $return['success'] = true;

  return $return;

  // echo json_encode(array('charge'=>$charge,'price'=>$price));

  wp_die();
}


add_action( 'wp_ajax_submitForm', 'submit_form' );
add_action( 'wp_ajax_nopriv_submitForm', 'submit_form' );

function submit_form(){
  global $wpdb;

  $formData = array();
  $first = $_POST['first'];
  $last = $_POST['last'];
  $email = $_POST['email'];
  foreach($_POST['data'] as $d){
    if(array_key_exists($d['name'], $formData)){
      if(!is_array($formData[$d['name']])){
        $temp = $formData[$d['name']];
        $formData[$d['name']] = array();
        $formData[$d['name']][] = $temp;
      }
      $formData[$d['name']][] = $d['value'];
    } else {
      $formData[$d['name']] = $d['value'];
    }
  }
  $formID = $formData['id'];
  unset($formData['id']);
  $formName = $formData['formName'];
  unset($formData['formName']);
  if(isset($formData['mailchimpList'])){
    $list = $formData['mailchimpList'];
    unset($formData['mailchimpList']);
  }

  // INTEGRATE CAPTCHA AND RE-ENABLE
  $obj = array();
  $captcha = $formData['g-recaptcha-response'];
  unset($formData['g-recaptcha-response']);
  /** ADD SECRET **/
  $response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6LcSEGkUAAAAAK_Wg8nZMc-XKRxy8jJ-VXwqD6W3&response=".$captcha."&remoteip=".$_SERVER['REMOTE_ADDR']);
  $obj = json_decode($response,true);

  $sql = "SELECT *
  FROM form
  WHERE ID = %d";
  $sql = $wpdb->prepare($sql,array($formID));
  $db_form = $wpdb->get_row($sql,ARRAY_A);
  $stripe_enable = $db_form['stripe_enable'] == 1 ? true : false;

  // $obj['success'] = true; /* REMOVE THIS */

  $return = array();
  // $return['obj'] = $response;
  // $return['captcha'] = $captcha;

  //if Google Recaptcha success
  if($obj['success']==true){

    $charge = 0;
    if($stripe_enable) $charge = make_payment($db_form,$email);

    if(!$stripe_enable || ($stripe_enable && $charge['success'])){

      if($stripe_enable) {
        $formData['Amount Paid'] = $charge['price'];
      }

      $formData_json = json_encode($formData,JSON_UNESCAPED_SLASHES);

      $data = array(
        'form_id' => $formID,
        'first' => $first,
        'last' => $last,
        'email' => $email,
        'content' => $formData_json,
        'date' => current_time("Y-m-d H:i:s")
      );

      $wpdb->insert('form_submission',$data);

      if(isset($list)){
        post_to_mailchimp($data,$list);
      }

      $return['status'] = true;
      $return['message'] = 'Form Submitted';
    } else {
      //payment failed
      $return['status'] = false;
      $return['message'] = 'An error occurred. Please try again';
    }
  } else {
    //google captcha failed
    $return['status'] = false;
    $return['message'] = 'An error occurred. Please try again';
  }

  //send emails
  $values = $formData;
  $sql = "SELECT notification FROM form WHERE ID=%d";
    $sql = $wpdb->prepare($sql,array($formID));
    $emails = $wpdb->get_var($sql);
    $emails = json_decode($emails,true);
  if(count($emails)>0) include 'submit.php';

  echo json_encode($return);
  wp_die();
}


function form_shortcodes(){
  add_shortcode('form', 'display_form_scode');
}
add_action( 'init', 'form_shortcodes');


function display_form_scode($atts, $content = null) {
  extract(shortcode_atts(array(
    'slug' => null,
    'style' => 'window',
    'bgrnd' => null,
    'button_text' => null,
    'title' => true,
    'design' => null
  ), $atts));
  $title = $title=='false' ? false : true;
  $data = array(
    'slug'=>$slug,
    'style'=>$style,
    'bgrnd'=>$bgrnd,
    'button_text'=>$button_text,
    'title'=>$title,
    'design'=>$design
  );
  ob_start();
  display_form_core($slug,$data);
  return ob_get_clean();
}

function display_form($c){
  global $wpdb;
  $count = 0;
  $s = $c['settings'];
  $s['style'] = empty($s['style']) ? 'main' : $s['style'];
  $id = intval($s['link_id']);
  $sql = "SELECT * FROM form WHERE ID=%s";
  $sql = $wpdb->prepare($sql,array($id));
  $form = $wpdb->get_row($sql,ARRAY_A);

  $form['content'] = json_decode($form['content'],true);
  $settings = $form['content']['settings'];
  $payment = false;
  if($settings['stripe_enable']=='true'){
    $form['payment'] = true;
  } else {
    $form['payment'] = false;
  }
  if($s['style']=='embed'){
    display_form_embed($form,$c);
  } else if($s['style']=='inline') {
    $btn = true;
    if(isset($s['btn'])) $btn = false;
    display_form_window($form,$btn);
  } else if($s['style']=='single'){
    display_form_single($form);
  }
}


function display_form_core($slug,$d,$btn=true) {
  global $wpdb;

  if(!isset($slug)) return;

  $thing = 1;
  $count = 0;

  $sql = "SELECT * FROM form WHERE slug=%s";
  $sql = $wpdb->prepare($sql,array($slug));
  $form = $wpdb->get_row($sql,ARRAY_A);

  $form['content'] = json_decode($form['content'],true);
  if($d['style']=='embed'){
    display_form_embed($form,$d);
  } else if($d['style']=='inline') {
    display_form_window($form,$btn);
  } else if($d['style']=='single'){
    display_form_single($form);
  }
}

function display_form_single($form){
  $fields = $form['content']['content'];
  $_s = $form['content']['settings'];
  ?>
  <div class="form form-single"
  style="background-color:<?php echo $d['bgrnd']; ?>"
  data-form="<?php echo $form['slug']; ?>"
  data-design="<?php echo $d['design']; ?>"
  >
    <div class="form_wrap">
      <div class="close icc"></div>
      <div class="window-inner">
        <div class="loader icc"></div>
        <div class="inner-status"></div>
        <div class="form-content">
          <form name="<?php echo uniqid(); ?>" method="post">
            <div class="form-inner">
              <?php if($d['title']){ ?><h1><?php echo $form['name']; ?></h1><?php } ?>
              <input type="hidden" name="id" value="<?php echo $form['ID']; ?>">
              <input type="hidden" name="formName" value="<?php echo $form['name']; ?>">
              <?php if(!empty($_s['mailchimp_list'])){ ?><input type="hidden" name="mailchimpList" value="<?php echo $_s['mailchimp_list']; ?>"><?php } ?>
              <?php
              $count = 0;
              foreach($fields as $f){
                $_half = $f['settings']['half'] == 'true' ? true : false;
                if(!$_half) $count = 0;
                $nopad = $count%2!=0 ? 'nopad' : '';
                $half = $f['settings']['half'] == 'true' ? 'field-half' : '';
                if($half) $count++;
                ?>

                <div class="field <?php echo $half.' '.$nopad ?>">
                  <?php echo form_get_field($f,true); ?>
                </div>
              <?php

              } ?>
              <!-- RE-ENABLE CAPTCHA -->
              <div class="g-recaptcha"
                    data-sitekey="6LcSEGkUAAAAAPDWbf8UnDHrzjBLxrWR5zyfgPic"
                    data-size="invisible"
                    data-callback="formSubmit"></div>
              <?php
                if(empty($d['button_text'])) $d['button_text'] = 'Subscribe';
              ?>
              <div class="field <?php echo $half.' '.$nopad ?>">
                <div class="form-submit"><div><?php echo $d['button_text']; ?></div></div>
              </div>
            </div>

          </form>
        </div>
      </div>
  </div>
<?php }

function display_form_embed($form,$d){
  $fields = $form['content']['content'];
  $_s = $form['content']['settings'];

  // var_dump($form);

  if($form['payment']){
    $price = $form['stripe_price'];
    $_price = ($price + .30) / (1 - 0.0175);
    $fee = $_price - $price;
    $price = $_price;
    $price = number_format((float)$price, 2, '.', '');
    $fee = number_format((float)$fee, 2, '.', '');
  }

  ?>
  <div class="form form-embed"
  style="background-color:<?php echo $d['bgrnd']; ?>"
  data-form="<?php echo $form['slug']; ?>"
  data-design="<?php echo $d['design']; ?>"
  data-payment="<?php echo $form['payment']; ?>"
  >
    <div class="form_wrap">
      <div class="close icc"></div>
      <div class="window-inner">
        <div class="loader icc"></div>
        <div class="inner-status"></div>
        <div class="form-content">
          <form name="<?php echo uniqid(); ?>" method="post">
            <div class="form-inner">
              <!-- <h1><?php echo $form['name']; ?></h1> -->
              <input type="hidden" name="id" value="<?php echo $form['ID']; ?>">
              <input type="hidden" name="formName" value="<?php echo $form['name']; ?>">
              <?php if(!empty($_s['mailchimp_list'])){ ?><input type="hidden" name="mailchimpList" value="<?php echo $_s['mailchimp_list']; ?>"><?php } ?>

              <?php
              $count = 0;
              foreach($fields as $f){
                $_half = $f['settings']['half'] == 'true' ? true : false;
                if(!$_half) $count = 0;
                $nopad = $count%2!=0 ? 'nopad' : '';
                $half = $f['settings']['half'] == 'true' ? 'field-half' : '';
                if($half) $count++;
                ?>

                <div class="field <?php echo $half.' '.$nopad ?>">
                  <?php echo form_get_field($f,true); ?>
                </div>
              <?php

              } ?>
            </div>

            <?php
            if($form['payment']){ ?>
              <div class="price">
                <span>Price:</span>
                <span>$<?php echo $price; ?></span>
                <span>(incl. $<?php echo $fee; ?> booking fee)</span>
              </div>
              <div id="card-element">
              <!-- A Stripe Element will be inserted here. -->
              </div>

              <!-- Used to display form errors. -->
              <div id="card-errors" role="alert"></div>
            <?php } else { ?>

            <?php } ?>
            <!-- RE-ENABLE CAPTCHA -->
            <div class="g-recaptcha"
                  data-sitekey="6LcSEGkUAAAAAPDWbf8UnDHrzjBLxrWR5zyfgPic"
                  data-size="invisible"
                  data-callback="formSubmit">
            </div>
            <?php
              if(empty($d['button_text'])) $d['button_text'] = 'Submit';
            ?>
            <div class="form-submit"><div><?php echo $d['button_text']; ?></div></div>
            <div class="sub-note"><?php echo $_s['note']; ?></div>
          </form>
        </div>
      </div>
    </div>
  </div>
<?php
}

function display_form_window($form,$btn=true){
  $fields = $form['content']['content'];
  $_s = $form['content']['settings'];
  ?>
  <div class="form __window" data-form="<?php echo $form['slug']; ?>">
    <div class="form_wrap">
      <div class="close icc"></div>
      <div class="window-inner">
        <div class="loader icc"></div>
        <div class="inner-status"></div>
        <div class="form-content">
          <form name="<?php echo uniqid(); ?>" method="post">
            <div class="form-inner">
              <h1><?php echo $form['name']; ?></h1>
              <input type="hidden" name="id" value="<?php echo $form['ID']; ?>">
              <input type="hidden" name="formName" value="<?php echo $form['name']; ?>">

              <?php
              foreach($fields as $f){ ?>
                <div class="field">
                  <?php if(!$pholder){ ?><div class="field_label"><div><?php echo $f['settings']['name']; ?></div></div><?php } ?>
                  <?php echo form_get_field($f,$pholder); ?>
                </div>
              <?php } ?>
              <div class="sub-note"><?php echo $_s['note']; ?></div>
            </div>
            <!-- RE-ENABLE CAPTCHA -->
            <div class="g-recaptcha"
                  data-sitekey="6LcSEGkUAAAAAPDWbf8UnDHrzjBLxrWR5zyfgPic"
                  data-size="invisible"
                  data-callback="formSubmit">
            </div>
            <div class="submit-row">
              <div class="form-submit"><div>Submit</div></div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <?php if($btn){ ?>
    <div class="button-outer" align="<?php //echo $c['settings']['align']; ?>">
      <div class="form-button button" data-form="<?php echo $form['slug']; ?>">
        <div><?php echo $form['name']; ?></div>
      </div>
    </div>
  <?php } ?>
<?php
}

function form_get_field($f,$pholder){
  $req = $f['settings']['required'] == 'true' ? '*' : '';
  $p = $pholder ? 'placeholder="'.$f['settings']['name'].$req.'"' : '';
  $id = $f['settings']['identity'];
  ob_start();
  global $count;
  $required = $f['settings']['required'] == 'true' ? 'required' : '';
  switch($f['type']){
    case 'input' :
      $vali = isset($f['settings']['validate']) ? $f['settings']['validate'] : 'text';
      if($f['settings']['validate']=='number'){
        $attr = 'max="'.$f['settings']['validate_max'].'" min="'.$f['settings']['validate_min'].'"';
      }
      ?>
      <input type="<?php echo $vali; ?>" id="<?php echo $id; ?>"
      name="<?php echo $f['settings']['name']; ?>" <?php echo $p.' '.$attr; ?> <?php echo $required; ?>>
      <?php break;
    case 'textarea' : ?>
      <textarea name="<?php echo $f['settings']['name']; ?>" <?php echo $p; ?> <?php echo $required; ?>></textarea>
      <?php break;
    case 'radio' :
      $_s = $f['settings'];
      foreach($f['content'] as $opt){
        $count++;
        $_o = $opt['settings']; ?>
        <input type="radio" id="radio<?php echo $count; ?>" name="<?php echo $_s['name']; ?>" <?php echo $required; ?>>
        <label for="radio<?php echo $count; ?>"><?php echo $_o['name']; ?></label>
      <?php }
      break;
    case 'checkbox' :
      $_s = $f['settings'];
      foreach($f['content'] as $opt){
        $count++;
        $_o = $opt['settings'];
        $chk = $_o['checked']=='true' ? 'checked' : null;
        ?>
        <input type="checkbox" id="checkbox<?php echo $count; ?>"
        name="<?php echo $_s['name']; ?>" value="<?php echo $_o['name']; ?>"
        <?php echo $required.' '.$chk; ?>>
        <label for="checkbox<?php echo $count; ?>"><?php echo $_o['name']; ?></label>
      <?php }
      break;
    case 'select' :
      $_s = $f['settings'];
      echo '<div class="select-cont">';
        echo '<select name="'.$_s['name'].'" '.$required.'>';
        ?><option disabled selected><?php echo $_s['name']; ?></option><?php
        foreach($f['content'] as $opt){
          $_o = $opt['settings']; ?>
          <option><?php echo $_o['name']; ?></option>
        <?php }
        echo '</select>';
      echo '</div>';
      break;
  }
  return ob_get_clean();
}

function post_to_mailchimp($info, $list){
  global $wpdb;

  $id = intval($info['form_id']);
  $sql = "SELECT content FROM form WHERE ID=%d";
  $sql = $wpdb->prepare($sql,array($id));
  $content = $wpdb->get_var($sql);
  $content = json_decode($content,true);
  $key = $content['settings']['mailchimp_api'];

  $first = $info['first'];
  $last = $info['last'];
  $data = [
    'email'     => $info['email'],
    'status'    => 'subscribed',
    'firstname' => $first,
    'lastname'  => $last
  ];

  $apiKey = $key;
  $listId = $list;

  $memberId = md5(strtolower($data['email']));
  $dataCenter = substr($apiKey,strpos($apiKey,'-')+1);
  $url = 'https://' . $dataCenter . '.api.mailchimp.com/3.0/lists/' . $listId . '/members/' . $memberId;

  $json = json_encode([
      'email_address' => $data['email'],
      'status'        => $data['status'], // "subscribed","unsubscribed","cleaned","pending"
      'merge_fields'  => [
          'FNAME'     => $data['firstname'],
          'LNAME'     => $data['lastname']
      ]
  ]);

  $ch = curl_init($url);

  curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $apiKey);
  curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_TIMEOUT, 10);
  curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
  curl_setopt($ch, CURLOPT_POSTFIELDS, $json);

  $result = curl_exec($ch);
  $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
  curl_close($ch);
}
?>
