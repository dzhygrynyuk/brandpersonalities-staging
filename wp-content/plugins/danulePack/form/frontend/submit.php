<?php

// $values = $_POST['values'];
// $formName = $_POST['formName'];
$date = current_time('F j, Y g:i a');

$subject = $formName.': '.$date;

$headers = "From: noreply@brandpersonalities.com.au\r\n"; //need to get this email from a setting
$headers .= "MIME-Version: 1.0\r\n";
$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

$message = '<html><body>';
$message .= '<h1>'.$formName.'</h1>';
$message .= '<p>Submitted: '.$date.'</p>';
$message .= '<table rules="all" style="border-color: #666;" cellpadding="10">';

foreach($values as $k => $v){
  $message .= "<tr><td><strong>".$k."</strong> </td><td>".$v."</td></tr>";
}

$message .= "</table>";
$message.= '<a style="display:none;" href="{unsubscribe:#}"></a>';
$message .= "</body></html>";

foreach($emails as $to){
  wp_mail($to, $subject, $message, $headers);
}

?>
