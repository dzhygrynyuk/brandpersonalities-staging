var currentForm = null;
var stripe_token = null;
var card = null;

function formSubmit(){
  var par = currentForm.closest('.form');

  var formData = currentForm.serializeArray();
  var first = jQuery('#first_name',par).val();
  var last = jQuery('#last_name',par).val();
  var email = jQuery('#email',par).val();

  var status_div = jQuery('.inner-status',par).removeClass('error');
  jQuery('.loader',par).removeClass('done fail success');
  status_div.hide().html('');
  jQuery('.loader',par).show();
  jQuery('.form-content',par).hide();

  var data = {
    action : 'submitForm',
    first : first,
    last : last,
    email : email,
    data : formData
  };
  if(par.attr('data-payment') == '1') data['card'] = stripe_token;

  jQuery.post(ajaxurl, data, function(r) {
    console.log(r);
    r = jQuery.parseJSON(r);
    console.log(r);

    status_div.show();
    setTimeout(function() {
      if(r.status==true){
        if(data['card']) card.clear();
        currentForm.trigger('reset');

        jQuery('.loader',par).addClass('done success');
        status_div.append(r.message);
        status_div.append('<div class="button __reset"><div class="_c">New Submission</div></div>')
      } else {
        jQuery('.form-content',par).show();
        jQuery('.loader',par).addClass('done fail');
        status_div.html(r.message).addClass('error');
      }
      grecaptcha.reset(); //reset widget
    }, 250);
  });
}

jQuery(document).ready(function( $ ) {

  $('.form.__window').appendTo('body');
  var arr = {};
  $('.form.__window').each(function(i,v){
    var val = $(this).attr('data-form');
    if(arr[val]) {
      arr[val] += 1;
    } else {
      arr[val] = 1
    }
  });
  $.each(arr,function(key,val){
    for(var i=0; i<val; i++){
      $('.form.__window[data-form="'+key+'"]:eq('+i+')').remove();
    }
  });

  if($('#blackout').length==0){
    $('body').append('<div id="blackout"></div>');
  }

  function reset_box(par){
    $('form',par).trigger('reset');
    $('.loader',par).removeClass('done fail success').hide();
    $('.form-content',par).show();
    $('.inner-status',par).hide().html('');
    paymentSupport = false;
    $('.notification',par).hide();
    $('.price',par).show();
    $('#card-element',par).show();
    childCount = 0;
    $('.price div').hide();
    $('.price div[data-child="'+childCount+'"]').show();
  }

  $('body').on('click','.form.__window .close',function(){
    var win = $(this).closest('.form.__window').fadeOut(250);
    $('#blackout').fadeOut(250);
  });

  $('body').on('click','.__reset',function(e){
    var par = $(this).closest('.form');
    reset_box(par);
  });

  $('body').on('click','.form-button, .form-open',function(e){
    var form = $(this).attr('data-form');
    console.log(form);
    var par = $('.__window[data-form="'+form+'"]');
    par.fadeIn(250);
    reset_box(par)
    $('#blackout').fadeIn(250);
    document.body.style.overflow = 'hidden';
  });

  $('body').on('click','.form .form-submit',function(e){
    if($(this).hasClass('disabled')) return;
    var par = $(this).closest('.form');
    var form = $('form',par);
    console.log('clicked button');
    try_submit(form);
  });

  function try_submit(form){
    form.validate({
      ignore:'',
      // errorPlacement: function(error,element) {
        // return true;
      // },
      highlight: function(element, errorClass) {
          var $element = $(element);
          $element.parent().addClass(errorClass);
      },
      unhighlight: function(element, errorClass) {
          var $element = $(element);
          $element.parent().removeClass(errorClass);
      }
    });
    form.submit();
  }

  $(document).on('submit','.form form',function(e){
    e.preventDefault();
    if(!$(this).valid()) return;

    var par = $(this);
    currentForm = $(this);

    var cont = currentForm.closest('.form');
    var status_div = jQuery('.inner-status',cont).removeClass('error');
    jQuery('.loader',cont).removeClass('done fail success');
    status_div.hide().html('');
    jQuery('.loader',cont).show();
    jQuery('.form-content',cont).hide();

    if(cont.attr('data-payment') == '1') {

      stripe.createToken(card).then(function(result) {
        if (result.error) {
          // $('.overlay',par).hide();
          // Inform the user if there was an error.
          var errorElement = document.getElementById('card-errors');
          errorElement.textContent = result.error.message;

          window.setTimeout(function(){
            jQuery('.form-content',cont).show();
            jQuery('.loader',cont).hide();
          },500);
        } else {
          // $('.overlay',par).show();
          // Send the token to your server.
          // stripeTokenHandler(result.token);

          stripe_token = result.token;

          var par = currentForm.closest('.form');
          var status_div = $('.inner-status',par).removeClass('error');
          $('.loader',par).removeClass('done fail success');
          status_div.hide().html('');
          $('.loader',par).show();
          $('.form-content',par).hide();

          //RE-ENABLE
          grecaptcha.execute(currentForm.find(".g-recaptcha").attr("widget-id"));
          // formSubmit();

        }
      });

    } else {
      grecaptcha.execute(currentForm.find(".g-recaptcha").attr("widget-id"));
      console.log(currentForm.find(".g-recaptcha").attr("widget-id"));
      // formSubmit();
    }
  });


  //------ STRIPE

  if($('#card-element').length>0){
    // Create a Stripe client.
    var stripe = Stripe('pk_test_CNVoz85YWKJI4qr2QDwQlh32'); /* publishable key here */

    // Create an instance of Elements.
    var elements = stripe.elements();

    // Custom styling can be passed to options when creating an Element.
    // (Note that this demo uses a wider set of styles than the guide below.)
    var style = {
      base: {
        color: '#777777',
        lineHeight: '18px',
        fontSmoothing: 'antialiased',
        fontSize: '16px',
        '::placeholder': {
          color: '#777777'
        }
      },
      invalid: {
        color: '#fa755a',
        iconColor: '#fa755a'
      }
    };

    // Create an instance of the card Element.
    card = elements.create('card', {style: style});

    // Add an instance of the card Element into the `card-element` <div>.
    card.mount('#card-element');

    // Handle real-time validation errors from the card Element.
    card.addEventListener('change', function(event) {
      var displayError = document.getElementById('card-errors');
      if (event.error) {
        displayError.textContent = event.error.message;
      } else {
        displayError.textContent = '';
      }
    });
  }

});
