<?php

function load_page($id){
  global $wpdb;

  $id = intval($id);

  $sql = "SELECT content FROM pageBuilder WHERE post_id = %d";
  $sql = $wpdb->prepare($sql,array($id));
  $result = $wpdb->get_row($sql,ARRAY_A);

  $page = $result['content'];
  $page = json_decode($page,true);
  // var_dump($page);
  return display_page($page);
}

function get_bgrnd($s){
  $array = array();

  if(isset($s['image_1'])) $image1 = wp_get_attachment_image_src($s['image_1'],get_size())[0];
  if(isset($s['image_2'])) $image2 = wp_get_attachment_image_src($s['image_2'],get_size())[0];

  $array['image1'] = $image1;
  $array['image2'] = $image2;

  if($s['bgrnd_type']=='full') {
    $array['class'] = 'b_full';
    $array['bgrnd'] = "background-image:url('$image1')";
  }
  else if($s['bgrnd_type']=='left'){
    $array['class'] = 'b_left';
    $array['bgrnd'] = "background-image:url('$image1')";
    $array['k'] = 0;
  }
  else if($s['bgrnd_type']=='right'){
    $array['class'] = 'b_right';
    $array['bgrnd'] = "background-image:url('$image1')";
    $array['k'] = 1;
  }
  else if($s['bgrnd_type']=='both'){
    $array['class'] = 'b_both';
    $array['bgrnd'] = "background-image:url('$image1'), url('$image2')";
  }

  if(!isset($array['bgrnd'])) $array['bgrnd'] = '';
  $array['bgrnd'] .= "; background-color:#".$s['bgrnd_color'];

  return $array;
}

function display_page($page,$args=null,$temp=false){
  global $post;
  $color = get_post_meta($post->ID,'custom_color')[0];
  ob_start();
  ?>
  <style>
  .row[data-inherit="true"],
  .button[data-style="coloured"]{
    background-color:#<?php echo $color; ?> !important;
  }
  .col .textCell[data-inherit="true"] h1 {
    color:#<?php echo $color; ?> !important;
  }
  .col .card-contain[data-inherit="true"] {
    border-color:#<?php echo $color; ?>;
  }
  .col .card-contain[data-inherit="true"] .separate {
    background-color:#<?php echo $color; ?>;
  }
  </style>
  <?php
  foreach($page['content'] as $row){
    $hclass = !empty($row['settings']['display_height']) ? 'm_center' : '';
    // echo '<div class="wrdtest" style="display:none;"><pre>'.var_export($row['settings'],true).'</pre></div>';
    if(isset($row['settings']['display_height'])){
      if($row['settings']['display_units']=='percent') $height = $row['settings']['display_height'].'vh';
      else if($row['settings']['display_units']=='pixels') $height = $row['settings']['display_height'].'px';
    }
    $anchor = isset($row['settings']['anchor']) ? '<a class="_anchor" name="'.$row['settings']['anchor'].'"></a>' : '';
    $padding = isset($row['settings']['padding']) ? $row['settings']['padding'] : '';
    $animate = isset($row['settings']['animate']) ? $row['settings']['animate'] : '';
    $custom_class = isset($row['settings']['custom_class']) ? $row['settings']['custom_class'] : '';
    $pad = 'true';
    $wrap = $row['settings']['wrap']=='true' ? true : false;
    $wrapclass = $wrap ? 'is_wrap' : '_nowrap';
    $inherit = isset($row['settings']['colors']) ? $row['settings']['colors'] : '';

    if(!isset($padding) || $padding=='false'){$pad='false';}
    $b = get_bgrnd($row['settings']);
    $style = isset($row['settings']['style']) ? $row['settings']['style'] : null;

    $video_cell = false;
    $image_cell = false;
    if(count($row['content'])>1){
      $video_cell = ($row['content'][1]['content'][0]['type']=='video_cell') ? true : false;
      $video_cell_data = $row['content'][1]['content'][0];
      $row['content'][1]['content'][0]['settings']['mobile_hide'] = 'true';

      $image_cell = ($row['content'][1]['content'][0]['type']=='image_cell') ? true : false;
      $image_cell_data = $row['content'][1]['content'][0];
      $row['content'][1]['content'][0]['settings']['mobile_hide'] = 'true';
    }
    ?>
    <div class="row <?php echo '_'.$row['settings']['cols'].' '.
      ' m_'.$row['settings']['mcols'].' '.$style.' '.$custom_class.' '.$wrapclass; ?>"
      <?php if (isset($b['bgrnd'])) { echo 'style="' . $b['bgrnd'] . ';"'; } ?>
      data-animate="<?php echo $animate . ';'; ?>"
      data-inherit="<?php echo $inherit. ';'; ?>"
      >
      <?php echo $anchor; ?>

    <!-- MOBILE BGRND IMAGE -->
    <?php
      $img = null;
      if (isset($b['class'])) :
      // var_dump($b);
        if($b['class']=='b_left' || $b['class']=='b_right' || $b['class']=='b_both' || $b['bgrnd_type']=='full'){
          $img = $b['image1'];
        } else {
          $img = $b['image2'];
        }
      endif; 
    ?>

    <!-- <img class="mobile-image" src="<?php //echo $img ?>"> -->
    <div class="mobile-video">
      <?php if($video_cell) echo format_cell($video_cell_data); ?>
    </div>

    <div class="mobile-image">
      <?php
      $image_cell_data['settings']['mobile_hide'] = 'false';
      if($image_cell) echo format_cell($image_cell_data);
      ?>
    </div>

    <!--  -->

    <?php
    $is_colWidths = false;
    if(!empty($row['settings']['colWidths'])){
      $colWidths = explode(',',$row['settings']['colWidths']);
      $k = 0;
      $is_colWidths = true;
    } ?>

    <?php
    $k = 0;
    if($row['settings']['wrap']=='true'){ ?>
    <div class="wrap" data-pad="<?php echo $pad; ?>">
    <div class="wrapInner <?php echo $padding; ?>">
    <?php } ?>
      <?php foreach($row['content'] as $col){ ?>
        <div class="col <?php echo $hclass; ?><?php if($is_colWidths){ ?>custom_col<?php } ?>" style="vertical-align:<?php echo $row['settings']['align'] . ';'; ?><?php if (isset($height)) { echo 'height:' . $height . ';'; } ?><?php //if($b['k']==$k){ echo $b['bgrnd']; } ?><?php if($is_colWidths){ ?>width:<?php echo $colWidths[$k].'%'; ?><?php } ?>">
          <?php if($padding!=='false'){ ?>
          <div class="innerCol <?php echo $padding; ?>">
          <?php } ?>
            <?php if(isset($col['content'])) { ?>
              <?php foreach($col['content'] as $cell){ ?>
                <?php echo format_cell($cell); ?>
              <?php } ?>
            <?php } ?>
          <?php if($padding!=='false'){ ?></div><?php } ?>
        </div>
      <?php $k++; } ?>
    <?php if($row['settings']['wrap']=='true'){ ?>
    </div>
    </div>
    <?php } ?>
    </div>
  <?php }
  //insert template content
  $template = $page['settings']['template'];
  if(! (empty($template) || $template=='normal')){
    if($temp && !empty($args)) {
      echo call_user_func('template_'.$template, $args);
    } else {
      echo call_user_func('template_'.$template);
    }
  }
  return ob_get_clean();
}

function format_cell($c){
  // global $db, $cg;
  ob_start(); ?>
  <?php if($c['type']=='image_cell'){ //---Image Cell
    $id = $c['settings']['bgrnd'];
    $image = wp_get_attachment_image_src($id,get_size())[0];
    echo display_image($c['settings'],$image);
    ?>
  <?php } ?>
  <?php if($c['type']=='video_cell'){ //---Video Cell
    $id = isset($c['settings']['bgrnd']) ? $c['settings']['bgrnd'] : '';
    $image = isset($c['settings']['video_image']) ? $c['settings']['bgrnd'] : '';
    echo display_video($c['settings'],$image);
    ?>
  <?php } ?>
  <?php if($c['type']=='text_cell'){ //---Text Cell
    $text = $c['settings']['text'];
    $liStyle = $c['settings']['liStyle'];
    $style = $c['settings']['style'];
    $accent = isset($c['settings']['accent']) ? $c['settings']['accent'] : '';
    $inherit = isset($c['settings']['colors']) ? $c['settings']['colors'] : '';
    // $is_accent = $accent=='none' ? false : true;
    $is_accent = false;
    ?>
    <div class="textCell text" data-style="<?php echo $style; ?>" data-listyle="<?php echo $liStyle; ?>" data-inherit="<?php echo $inherit; ?>">
      <?php if($is_accent){ ?>
        <div class="accent-outer" align="<?php echo $accent; ?>">
          <div class="accent"></div>
        </div>
      <?php } ?>
      <?php echo stripslashes($text); ?>
    </div>
  <?php } ?>
  <?php if($c['type']=='header_cell'){ //---Header Cell
    echo display_header($c['settings']);
  } ?>
  <?php if($c['type']=='coupon_cell'){ //---Coupon Cell
    echo display_coupons($c['settings']);
  } ?>
  <?php if($c['type']=='slider_cell'){ //---Slider Cell
    echo display_slider($c['settings']);
  } ?>
  <?php if($c['type']=='portfolio_cell'){ //---Portfolio Cell
    echo display_portfolio($c);
  } ?>
  <?php if($c['type']=='form_cell'){ //---Form Cell
    echo display_form($c);
  } ?>
  <?php if($c['type']=='schedule_cell'){ //---Form Cell
    echo template_schedule($c);
  } ?>
  <?php if($c['type']=='separator_cell'){ //---Card Cell
    ?><div class="page-separator"></div><?php
  } ?>
  <?php if($c['type']=='card_cell'){ //---Card Cell
    echo display_card($c);
  } ?>
  <?php if($c['type']=='code_cell'){ //---Code Cell
    echo '<div class="code">';
    echo stripslashes(do_shortcode($c['settings']['code']));
    echo '</div>';
  } ?>
  <?php if($c['type']=='button_cell'){ //---Button Cell
    // var_dump($c['settings']);
    $style = $c['settings']['style'];
    // $url = $c['settings']['link_url'];
    $url = make_a_url($c['settings']['link_id'],$c['settings']['link_type'],$c['settings']['link_url']);
    ?>
    <div class="button-outer" align="<?php echo $c['settings']['align']; ?>">
      <a href="<?php echo $url; ?>">
        <div class="button <?php echo $c['settings']['style']; ?>" data-style="<?php echo $style; ?>"
          style="<?php if($style=='custom'){ echo 'background-color:#'.$c['settings']['btn_color']; } ?>">
          <div class="button-over"></div>
          <div class="__c"><?php echo stripslashes($c['settings']['link_name']); ?></div>
        </div>
      </a>
    </div>
    <?php
  } ?>
  <?php return ob_get_clean();
}

function make_a_url($id,$type,$link){
  if($type=='url') {
    return $link;
  } else {
    if($type == 'page'){
      $url = get_the_permalink($id);
    }
    return $url;
  }
}


function display_card($s){
  $_s = $s['settings']; ?>
  <div class="card-wrap">
    <?php if($_s['style'] == 'coloured'){ ?>
      <div class="card-contain" style="background-color:#<?php echo $_s['background_color']; ?>;">
    <?php } else { ?>
      <div class="card-contain __white" data-inherit="<?php echo $_s['colors']; ?>">
    <?php } ?>
      <div class="card-float" <?php if (isset($_s['text_style'])) { echo 'data-style="' . $_s['text_style'] . '"';  }?>>
        <h2><?php echo stripslashes($_s['heading']); ?></h2>
        <div class="separate"></div>
        <div class="card-text"><?php echo stripslashes($_s['text']); ?></div>
        <?php //var_dump($_s); ?>
      </div>
    </div>
  </div>
<?php }


function display_header($s){
  ob_start();
  $image = wp_get_attachment_image_src($s['bgrnd'],get_size(true))[0];
  $image_mobile = wp_get_attachment_image_src($s['mobile_bgrnd'],get_size(true))[0];
  if(empty($image_mobile)) $image_mobile = $image;
  ?>

  <div class="image-header"
  style="background-image:url('<?php //echo $image; ?>')"
  data-height="<?php echo $s['height']; ?>">
    <div class="bgrnd-img">
      <img class="bgrnd-img-desktop" src="<?php echo $image; ?>">
      <img class="bgrnd-img-mobile" src="<?php echo $image_mobile; ?>">
    </div>
    <div class="wrap">
      <div class="float">
        <div class="float-inner">
          <div class="text"><h1 <?php if (isset($s['text_color'])) { echo 'style="color:#' . $s['text_color'] . '"'; } ?>><?php echo stripslashes($s['caption']); ?></h1></div>
          <?php if (!empty($s['subtext'])){ ?>
            <div class="text text-small"><h2><?php echo stripslashes($s['subtext']); ?></h2></div>
          <?php } ?>
          <?php if (!empty($s['sub_link_url'])){ ?>
            <a href="<?php echo $s['sub_link_url']; ?>" class="button-link">
              <div class="button sub-button"><div><?php echo stripslashes($s['sub_link_name']); ?></div></div>
            </a>
          <?php } ?>
          <?php if (!empty($s['link_url'])){ ?>
            <a href="<?php echo $s['link_url']; ?>" class="button-link">
              <div class="button"><div><?php echo stripslashes($s['link_name']); ?></div></div>
            </a>
          <?php } ?>
        </div>
      </div>
    </div>
    <!-- <div class="page-down icc"></div> -->
  </div>



  <?php
  return ob_get_clean();
}



function display_video($s, $image){

  ob_start();
  $video = get_video($s['url']); ?>

  <div class="video" <?php if (isset($s['mobile_hide'])) { echo 'data-mobile-hide="' .  $s['mobile_hide'] . '"'; } ?>>
    <?php if($video['type']=='youtube'){ ?>
      <iframe src="https://www.youtube.com/embed/<?php echo $video['id'] ?>?ecver=2&autoplay=0" frameborder="0" allowfullscreen></iframe>
    <?php } else { ?>
      <iframe src="https://player.vimeo.com/video/<?php echo $video['id'] ?>?autoplay=0" frameborder="0" allowfullscreen></iframe>
    <?php } ?>
  </div>
  <?php return ob_get_clean();
}

function display_image($s, $image){
  ob_start();
  if($s['display_type']=='background') $class = 'bgrndImg';
  if($s['display_type']=='square') $class = 'squareImg';
  if($s['display_type']=='circle') $class = 'squareImg circle';
  if(isset($s['link_url'])) $url = $s['link_url'];
  if($s['title']) $title = stripslashes($s['title']);
  if($s['caption']) $caption = stripslashes($s['caption']);
  $height = null;

  if($s['display_type']=='full'){ ?>
    <?php if(!empty($url)){ ?><a class="box" href="<?php echo $url; ?>"><?php } ?>
      <div class="image fullImg fullImg_only"<?php if (isset($s['mobile_hide'])) { echo 'data-mobile-hide="' . $s['mobile_hide'] . '"'; } ?>>
        <img src="<?php echo $image; ?>">
        <?php if(!empty($title)){ ?><div class="title"><h1><?php echo $title; ?></h1></div><?php } ?>
      </div>
    <?php if(!empty($url)){ ?></a><?php } ?>
    <?php
  }
  else if($s['display_type']=='full_grid'){ ?>
    <?php if(!empty($url)){ ?><a class="inline" href="<?php echo $url; ?>"><?php } ?>
      <div class="fullGrid-contain">
        <div class="image fullImg fullGrid">
          <img src="<?php echo $image; ?>">
          <?php if(!empty($title)){ ?><div class="title"><h1><?php echo $title; ?></h1></div><?php } ?>
        </div>
      </div>
    <?php if(!empty($url)){ ?></a><?php } ?>
    <?php
  }
  else if($s['display_type']=='inline'){ ?>
    <?php if(!empty($url)){ ?><a class="box" href="<?php echo $url; ?>"><?php } ?>
      <div class="image image-inline">
        <div class="inline-row">
          <div class="img-wrap">
            <img src="<?php echo $image; ?>">
          </div>
          <?php if(!empty($title)){ ?><div class="title"><h1><?php echo $title; ?></h1></div><?php } ?>
        </div>
        <div class="caption"><?php echo $caption; ?></div>
      </div>
    <?php if(!empty($url)){ ?></a><?php } ?>
    <?php
  }
  else if($s['display_type']=='icon'){ ?>
    <?php if(!empty($url)){ ?><a class="box" href="<?php echo $url; ?>"><?php } ?>
      <div class="image image-icon">
        <div class="the-image" style="background-image:url('<?php echo $image; ?>');"></div>
        <?php if(!empty($title)){ ?><div class="title"><h1><?php echo $title; ?></h1></div><?php } ?>
      </div>
    <?php if(!empty($url)){ ?></a><?php } ?>
    <?php
  }
  else { ?>
    <?php if(!empty($url)){ ?><a class="box" href="<?php echo $url; ?>"><?php } ?>
      <div class="image <?php echo $class; ?>"
        style="background-image:url('<?php echo $image ?>');"
      >
      </div>
      <?php if(!empty($title)){ ?>
        <div class="image-caption"><?php echo $title; ?></div>
      <?php } ?>
    <?php if(!empty($url)){ ?></a><?php } ?>
    <?php
  }
  return ob_get_clean();
}

function setting_get($name,$decode){
  global $db;
  $sql = "SELECT value FROM settings WHERE name=?";
  $value = $db->select_var($sql,array($name));
  if($decode) $value = json_decode($value,true);
  return $value;
}






function get_video($url){

  $url_data = parse_url( $url );
  if (
    strpos($url_data['host'], 'youtube') !== false ||
    strpos($url_data['host'], 'youtu.be') !== false
  ) {
    //youtube
    $type = 'youtube';
    if(strpos($url_data['host'], 'youtube') !== false){
      parse_str(parse_url( $url, PHP_URL_QUERY ), $vars);
      $id = $vars['v'];
    } else {
      $id = str_replace('/','',$url_data['path']);
    }

    // $option = array(
    //   'id' => $id,
    //   'part' => 'snippet',
    //   'key' => 'AIzaSyCa6F4TK0xHvIYt9FXEjdK93_khyjIY5Oo'
    // );
    // $url = "https://www.googleapis.com/youtube/v3/videos?".http_build_query($option, 'a', '&');
    // $curl = curl_init($url);
    // curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
    // curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
    // curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    // curl_setopt($curl, CURLOPT_HTTPHEADER, $curlheader);
    // $response = json_decode(curl_exec($curl),true);
    // curl_close($curl);
    // $img = $response['items'][0]['snippet']['thumbnails']['standard']['url'];
    // $title = $response['items'][0]['snippet']['title'];
  }

  else if (
      strpos($url_data['host'], 'vimeo') !== false
  ) {
    $type = 'vimeo';
    $id = str_replace('/','',$url_data['path']);

    // $url = "http://vimeo.com/api/v2/video/".$id.".json";
    // $curl = curl_init($url);
    // curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
    // curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
    // curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    // curl_setopt($curl, CURLOPT_HTTPHEADER, $curlheader);
    // $response = json_decode(curl_exec($curl),true);
    // curl_close($curl);
    // $img = $response[0]['thumbnail_large'];
    // $title = $response[0]['title'];
  }

  $array = array(
    'image' =>  isset($img) ? $img : '',
    'type'  =>  $type,
    'id'    =>  $id,
    'url'   =>  isset($videourl) ? $videourl : '',
    'title' =>  isset($title) ? $title : '',
  );
  return $array;
}















?>
