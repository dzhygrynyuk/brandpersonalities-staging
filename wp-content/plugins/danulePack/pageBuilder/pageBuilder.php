<?php
//--AJAX / Files
if(is_admin()){
	include 'editor/core.php';
} else {
	include 'frontend/display.php';
}

//-----

function pageBuilder_styles() {
	$screen = get_current_screen();
	global $pagenow, $post;
	if ( 	$screen->post_type == 'page' &&
		 		in_array( $pagenow, array( 'post.php', 'post-new.php' ) )
		 ) {
		wp_enqueue_style( 'pageBuilder_style', plugins_url( '/', __FILE__ ) . 'editor/style.css?v=1.09');
		wp_enqueue_style( 'selection_style', plugins_url( '/../', __FILE__ ) . 'modules/selection/style.css?v=1.03');
		wp_enqueue_style( 'dropy', plugins_url( '/../', __FILE__ ) . 'modules/dropy/dropy.css' );
		//scripts
    wp_enqueue_script( 'slider_tinymce', plugins_url( '/../', __FILE__ ) . 'modules/tinymce/tinymce.min.js', array('jquery') );
		wp_enqueue_script( 'slider_tinymceJ', plugins_url( '/../', __FILE__ ) . 'modules/tinymce/jquery.tinymce.min.js', array('jquery') );
		wp_enqueue_script( 'pageBuilder_jscolor', plugins_url( '/', __FILE__ ) . 'js/jscolor.min.js', array('jquery') );
		wp_enqueue_script( 'pageBuilder_dropy', plugins_url( '/../', __FILE__ ) . 'modules/dropy/dropy.js', array('jquery') );
		wp_enqueue_script( 'pageBuilder_ejs', plugins_url( '/', __FILE__ ) . 'js/ejs.min.js', array('jquery') );
		wp_enqueue_script( 'pageBuilder_sortable', plugins_url( '/', __FILE__ ) . 'js/sortable.min.js', array('jquery') );
		wp_enqueue_script( 'pageBuilder_js_load', plugins_url( '/', __FILE__ ) . 'editor/templates/_main.js', array('jquery') );
		wp_enqueue_script( 'pageBuilder_js_manage', plugins_url( '/', __FILE__ ) . 'editor/manage.js', array('jquery') );
		wp_enqueue_script( 'pageBuilder_js_main', plugins_url( '/', __FILE__ ) . 'editor/scripts.js?v=1.09', array('jquery') );
		wp_enqueue_script( 'selection_js', plugins_url( '/../', __FILE__ ) . 'modules/selection/scripts/core.js?v=1.09', array('jquery') );
		wp_localize_script( 'pageBuilder_js_load', 'plugins_url', plugins_url( '/', __FILE__ ) );
		wp_localize_script( 'pageBuilder_js_main', 'postID', (string) $post->ID );
	}
}
add_action( 'admin_enqueue_scripts', 'pageBuilder_styles' );

function pageBuilder_editor_add() {
	$screens = array( 'page' );
	foreach ( $screens as $screen ) {
		add_meta_box(
			'pb_editor',
			'Page Builder',
			'pageBuilder_editor_callback',
			$screen, 'normal', 'high'
		);
	}
}
add_action( 'add_meta_boxes', 'pageBuilder_editor_add' );

//MAIN AREA
function pageBuilder_editor_callback( $post ) {
  global $core;
  wp_enqueue_media();

  ?><div id="blackout"></div><?php
  include $core.'modules/selection/display_window.php';
	include 'editor/display.php';
	include $core.'modules/window/settings.php';
	include $core.'modules/window/text.php';
}

function pageBuilder_meta_add() {
	$screens = array( 'page' );
	foreach ( $screens as $screen ) {
		add_meta_box(
			'pb_meta',
			'Metadata',
			'pageBuilder_meta_callback',
			$screen, 'side', 'high'
		);
	}
}
add_action( 'add_meta_boxes', 'pageBuilder_meta_add' );

function pageBuilder_meta_callback( $post ) {
	$meta = get_post_meta( $post->ID, 'custom_metadata', true );
	$meta = json_decode($meta,true);
	?>

	<textarea name="pb_meta" placeholder="meta"><?php echo $meta['meta']; ?></textarea>
	<style>
		#pb_meta textarea, #pb_meta input {width:100%; resize: none;}
	</style>
	<?php
}

function pageBuilder_color_add() {
	$screens = array( 'page' );
	foreach ( $screens as $screen ) {
		add_meta_box(
			'pb_color',
			'Color',
			'pageBuilder_color_callback',
			$screen, 'side', 'high'
		);
	}
}
add_action( 'add_meta_boxes', 'pageBuilder_color_add' );

function pageBuilder_color_callback( $post ) {
	$color = get_post_meta( $post->ID, 'custom_color', true );
	// $color = json_decode($color,true);
	?>

	<input class="jscolor" name="pb_color" id="pb_color" value="<?php echo $color; ?>">
	<style>
		#pb_color input {width:100%; resize: none;}
	</style>
	<?php
}

// function pageBuilder_page_save($post_id) {
// 	// If this is a revision, get real post ID
// 	if ( $parent_id = wp_is_post_revision( $post_id ) )
// 		$post_id = $parent_id;

// 	global $pagenow, $post;
// 	$screen = get_current_screen();
// 	if ( 	$screen->post_type == 'page' &&
// 		 		in_array( $pagenow, array( 'post.php', 'post-new.php' ) ) ) {

// 		$array = array(
// 			'meta'=>$_POST['pb_meta']
// 		);
// 		$array = json_encode($array);
// 		update_post_meta( $post_id, 'custom_metadata', $array);
// 		update_post_meta( $post_id, 'custom_color', $_POST['pb_color']);

// 	}

// }
// add_action( 'save_post', 'pageBuilder_page_save' );



function pageBuilder_page_save($post_id) {
  // $screen = get_current_screen();

  if ( $parent_id = wp_is_post_revision( $post_id ) ) {
    $post_id = $parent_id;
  }
  
  $postType = get_post_type($post_id);

//  if (  $screen->post_type == 'post' || $screen->post_type =='page' ) {
  if (isset($_POST['pb_meta'])) :

    if (  $postType == 'post' || $postType =='page' ) {
      $array = array(
        'meta'=>$_POST['pb_meta']
      );
      $array = json_encode($array);
      update_post_meta( $post_id, 'custom_metadata', $array);
      update_post_meta( $post_id, 'custom_color', $_POST['pb_color']);

    }

  endif;

}
add_action( 'save_post', 'pageBuilder_page_save' );



?>
