<div class="page_contain pg">

  <div class="modules">
    <div class="inner">
      <ul id="pageItems" class="me_test">
        <li><div class="fa fa-align-left drag-item" data-type="text_cell">Text</div></li>
        <li><div class="fa fa-picture-o drag-item" data-type="image_cell">Image</div></li>
        <li><div class="fa fa-external-link-square drag-item" data-type="button_cell">Button</div></li>
        <!-- <li><div class="fa fa-ticket drag-item" data-type="coupon_cell">Coupons</div></li> -->
        <li><div class="fa fa-youtube-play drag-item" data-type="video_cell">Video</div></li>
        <li><div class="fa fa-caret-square-o-right drag-item" data-type="slider_cell">Slider</div></li>
        <!-- <li><div class="fa fa-tasks drag-item" data-type="portfolio_cell">Portfolio</div></li> -->
        <li><div class="fa fa-address-book drag-item" data-type="form_cell">Form</div></li>
        <!-- <li><div class="fa fa-clock-o drag-item" data-type="schedule_cell">Schedule</div></li> -->
        <li><div class="fa fa-code drag-item" data-type="code_cell">Code</div></li>
        <li><div class="fa fa-header drag-item" data-type="header_cell">Header</div></li>
        <li><div class="fa fa-id-card drag-item" data-type="card_cell">Card</div></li>
        <li><div class="fa fa-minus drag-item" data-type="separator_cell">Separator</div></li>
      </ul>
    </div>
    <div class="expander icc"></div>
  </div>

  <div class="main">

    <div class="button button-fullwidth __addRow" data-action="prepend">
      <div>Add Row</div>
    </div>

    <div id="page_content"></div>

    <div class="button button-fullwidth __addRow" data-action="append">
      <div>Add Row</div>
    </div>

  </div>

  <!-- <div class="sideMenu __left __settings">
    <div class="expandMenu icc"></div>

    <div class="inner">

      <div class="saveOptions">
        <div class="button __delete icc"></div>
        <div class="button button-primary __save"><div>Save</div></div>
        <div class="button __cancel"><div>Cancel</div></div>
      </div>
      <div class="loader"><i class="fa fa-refresh fa-spin fa-3x fa-fw"></i></div>
      <div class="__content"></div>
    </div>
  </div> -->

</div>

<div class="invis" style="display:none;">
  <textarea name="pageBuilder_data"><?php echo $post->post_content; ?></textarea>
</div>

<div class="__window code">
  <div class="closebtn winclose icc"></div>
  <div class="box">
    <textarea class="code"></textarea>
    <div class="box-controls">
      <div class="button __save winclose"><div>Save</div></div>
    </div>
  </div>
</div>
