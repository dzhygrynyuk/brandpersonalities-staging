<?php


// action=wp_post_page_clone

add_action( 'wp_ajax_pageSave', 'save_page' );
add_action( 'wp_ajax_pageDelete', 'delete_page' );
add_action( 'wp_ajax_pageGetSingle', 'get_single_page' );

// function on_page_saved( $id ) {
//   global $wpdb;
//
//   if ( wp_is_post_revision( $id ) ) return;
//
//   $data = array(
//     'post_status' => 'publish'
//   );
//   $wpdb->update('wp_posts',$data,array('ID'=>$id));
// }
// add_action( 'save_post', 'on_page_saved' );

function save_page(){
  global $wpdb;

  $page = $_POST['page'];
  $s = $page['settings'];
  $content = json_encode($page,JSON_UNESCAPED_SLASHES);

  $content = str_replace("&nbsp;", '',$content);
  $content = str_replace("\n", '',$content);

  $name = $s['title'];
  $id = intval($_POST['id']);

  $array = array(
    'post_id'=>$id,
    'content'=>$content
  );
  $wpdb->replace('pageBuilder',$array);
  // $wpdb->update('wp_posts',array('post_content'=>$content),array('ID'=>$id));

  echo json_encode($array);
  wp_die();
}

function delete_page(){
  global $db;

  $where = array(
    'ID' => $_POST['id']
  );
  $db->delete('page',$where);
}

function get_single_page(){
  global $wpdb;

  $id = intval($_POST['id']);

  $sql = "SELECT * FROM pageBuilder WHERE post_id = %d";
  $sql = $wpdb->prepare($sql,array($id));
  $page = $wpdb->get_row($sql,ARRAY_A);

  $page_color = get_post_meta( $id, 'custom_color', true );

  echo json_encode(array('page'=>$page,'page_color'=>$page_color));
  wp_die();
}

function get_all_pages(){
  global $db;

  $sql = "SELECT ID, title, date
  FROM page
  WHERE status!='hidden'
  ORDER BY date DESC";
  $pages = $db->select($sql,array());

  echo display_page_list($pages);
}

function display_page_list($pages){
  ob_start();
  ?>
  <div class="flex">
    <div class="flex-row heading">
      <div class="flex-cell double">Title</div>
      <div class="flex-cell">Date</div>
      <div class="flex-cell"></div>
    </div>

    <?php foreach($pages as $p){
      $p['date'] = date('j M Y, g:ia',strtotime($p['date']));
      ?>
      <div class="flex-row" data-id="<?php echo $p['ID']; ?>">
        <div class="flex-cell double"><?php echo $p['title']; ?></div>
        <div class="flex-cell"><?php echo $p['date']; ?></div>
        <div class="flex-cell center">
          <div class="button button-delete">
            <div class="ic">Delete</div>
          </div>
          <div class="button loadpage">
            <div class="ic">Edit</div>
          </div>
        </div>
      </div>
    <?php } ?>

  </div>
  <?php return ob_get_clean();
}

?>
