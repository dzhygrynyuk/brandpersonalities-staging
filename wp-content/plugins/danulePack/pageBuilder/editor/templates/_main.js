var pb_templates = {};
$(function(){

  console.log(ajaxurl);
  console.log(plugins_url);

  var dir = plugins_url+'editor/templates/';
  var files = ['controls_row','controls_cell','col','page_settings','page_temp'];
  var items = [
    'separator_cell',
    'card_cell',
    'coupon_cell',
    'image_cell',
    'video_cell',
    'button_cell',
    'header_cell',
    'text_cell',
    'slider_cell',
    'portfolio_cell',
    'form_cell',
    'schedule_cell',
    'code_cell',
    'row'
  ];
  $.each(items,function(i,v){
    files.push(v);
    files.push(v+'_settings');
  });

  var len = files.length - 1;
  $.each(files,function(i,v){
    var f = dir+v+'.ejs?v='+Date.now();
    $.get(f, function (file) {
      pb_templates[v] = file;
      if(i==len) {
        $('body').trigger('pageBuilder_ready');
        console.log('PAGE READY');
        // var html = ejs.render(pb_templates['row'],{index:9001});
        // console.log(html);
        // console.log(pb_templates);
        // _parts.check();
      }
    });
  });
});
