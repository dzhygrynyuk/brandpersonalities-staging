//only store functions in here

var array_move = function (arr, old_index, new_index) {
  if (new_index >= arr.length) {
    var k = new_index - arr.length;
    while ((k--) + 1) {
      arr.push(undefined);
    }
  }
  arr.splice(new_index, 0, arr.splice(old_index, 1)[0]);
  return arr; // for testing purposes
};

function findBlock(s,index,remove){
  var block = s.pagearr;
  var blockparent = s.pagearr;
  if (index.indexOf(',') > -1){
    var indexes = index.split(',');
  } else {
    indexes = [index];
  }
  for(var i=0; i<indexes.length; i++){
    blockparent=block.content;
    block=block.content[indexes[i]];
    if(remove && i==indexes.length-1){
      blockparent.splice(indexes[i],1);
    }
  }
  return block;
}

function replaceBlock(s,index,obj){
  var block = s.pagearr;
  var blockparent = s.pagearr;
  if (index.indexOf(',') > -1){
    var indexes = index.split(',');
  } else {
    indexes = [index];
  }
  for(var i=0; i<indexes.length; i++){
    blockparent=block.content;

    block.content[indexes[i]] = obj;

    block=block.content[indexes[i]];
    // block.content = obj;
  }
  return block;
}

function managePageParts(block,s){
  if(block.type=='row'){
    var index = block.settings.index;
    current_cols = block.content.length;
    cols = block.settings.cols;

    if(current_cols != cols) {
      $.each(block.content,function(i,v){
        console.log(block.content[i].settings.width);
        delete block.content[i].settings.width;
      });
    }
    if(current_cols < cols){
      for(var i=current_cols; i<cols; i++){
        var col = create_col(s);
        col.settings.index = index+','+i;
        block.content.push(col);
      }
    }
    else if(current_cols > cols){
      for(var i=current_cols-1; i>=cols; i--){
        //CODE: move content of col to previous index
        var _index = index+','+i;
        findBlock(s,_index,true);
      }
    }
  }
  return block;
}

function create_row(s){
  var row = $.extend(true, {}, s.object.row);
  s.pagearr.content.push(row);
  row.settings.index = s.pagearr.content.indexOf(row).toString();
  for(var i=0; i<row.settings.cols; i++){
    col = create_col(s);
    row.content.push(col);
    col.settings.index = row.content.indexOf(col);
    col.settings.index = row.settings.index+','+col.settings.index;
  }
  return row;
}

function create_col(s){
  var col = $.extend(true, {}, s.object.col);
  return col;
}

function create_cell(s,colID,copy){
  var block = findBlock(s,colID);
  if(!block.content) block.content = [];
  if(copy){
    var cell = s.current;
    block.content.push(cell);
  } else {
    var cell = $.extend(true, {}, s.object[s.cellType]);
    block.content.push(cell);
  }
  var len = block.content.length-1;
  array_move(block.content,len,s.index);
  return cell;
}

function remove_cell(s,cellID){
  var block = findBlock(s,cellID);
  s.current = $.extend(true, {}, block);
  findBlock(s,cellID,true);
}

function update_indexes(s,colIndex){
  var block = findBlock(s,colIndex);
  for(var i=0; i<block.content.length; i++){
    block.content[i].settings.index = colIndex+','+i;
  }
}

function updateAllIndexes(s){
  for(var i=0; i<s.pagearr.content.length; i++){
    s.pagearr.content[i].settings.index=i.toString();
    for(var j=0; j<s.pagearr.content[i].content.length; j++){
      s.pagearr.content[i].content[j].settings.index=i+','+j;
      if(s.pagearr.content[i].content[j].content){
        for(var k=0; k<s.pagearr.content[i].content[j].content.length; k++){
          s.pagearr.content[i].content[j].content[k].settings.index=i+','+j+','+k;
        }
      } else {
        s.pagearr.content[i].content[j].content = [];
      }
    }
  }
}
