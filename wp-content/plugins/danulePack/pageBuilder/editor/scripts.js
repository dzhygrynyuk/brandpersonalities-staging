var $global = {};
$global.settings = {};
$global.vars = {};
console.log(postID);

function isEmpty(string){
  if(!string) return true;
  return /^\s+$/.test(string);
}

String.prototype.stripSlashes = function(){
    return this.replace(/\\(.)/mg, "$1");
}

$(function(){
  var _ = $('.pg');
  var s = {};
  s.page = $('#page_content',_);

//---functions
  function load_template(x,data){
    var html = ejs.render(pb_templates[x],data);
    return html;
  }

  function setEditing(x){
    s.editing = x.closest('.capsule');
    s.editingIndex = s.editing.attr('data-index');
    var block = findBlock(s,s.editingIndex);
    s.editingBlock = block;
  }

  function menuToggle(x,show){
    $('.__delete',x).removeClass('selected');
    if(x.is(':visible') && show!=true){
      x.removeClass('active');
      x.hide();
    }
    else if(!x.is(':visible') && show!=false){
      x.addClass('active');
      x.show();
    }
  }

  function menuLoad(x,y,data){

    // $('.loader',y).addClass('visible');
    var extra = x+'_extra';
    x = x+'_settings';
    if(!data) var data = {};
    var part = load_template(x,data);
    $('.__content',y).html(part);
    $('.loader',y).removeClass('visible');
    $('.dropy',y).dropy();
    jscolor.installByClassName("jscolor");
  }

  function display_update_row(s,rowIndex){
    block = findBlock(s,rowIndex);
    var index = block.settings.index;
    var row = $('.capsule[data-index="'+index+'"]',s.page);
    var settings = $.extend(true, {}, block.settings);
    settings.page_color = s.pagearr.settings.page_color;
    var _row = $(load_template(block.type,settings));
    if(row.length){
      row.replaceWith(_row);
      row = _row;
    } else {
      row = $(_row).appendTo(s.page);
    }
    var controls = load_template('controls_row',{});
    row.append(controls);
    var cols = String(block.settings.colWidths);
    var colsCustom = false;
    if(cols!='undefined') {
      console.log(cols);
      colsCustom = true;
      cols = cols.split(',');
    }

    if(!block.content) block.content = [];
    var default_width = 100 / block.content.length;
    for(var k=0; k<block.content.length; k++){
      var _this = block.content[k];
      var settings = $.extend(true, {}, _this.settings);
      // if(colsCustom){
        // settings['width'] = cols[k];
      // } else {
        settings['width'] = default_width;
      // }
      var col = load_template(_this.type,settings);
      var c = $(col).appendTo(row);
      display_update_cells(s,_this.settings.index);
    }
    afterDisplay(row);
  }

  function display_update_cells(s,colIndex){
    cellblock = findBlock(s,colIndex);
    var index = cellblock.settings.index;
    var col = $('.capsule[data-index="'+index+'"] .innerCol',s.page);
    col.html('');
    var controls = load_template('controls_cell',{});
    if(cellblock.content){
      for(var i=0; i<cellblock.content.length; i++){
        var _this_cell = cellblock.content[i];
        $.each(_this_cell.settings,function(i,v){
          if(v){
            if(typeof v == 'string'){
              _this_cell.settings[i] = v.stripSlashes(); //STRIP SLASHES FOR ALL STORED VALUES
            }
          }
        });
        var cell = load_template(_this_cell.type,_this_cell.settings);
        var cell = $(cell).appendTo(col);
        $(controls).appendTo(cell);
      }
    }
  }

  function afterDisplay(item){
    var colIndexStart, colIndex, colID, started=false;
    var el = $('.innerCol',item);
    $(el).each(function (i,e) {
      var sortable = Sortable.create(e, {
        group: {name:"pageBuilder_cells", put:true},
        sort: true,
        handle: ".handle",
        filter: ".control_toggle",
        onStart: function(evt){
          added = false;
          started = true;
          console.log('started!');
          var itm = $(evt.item);
          if(itm.hasClass('cell')){
            colIndex = itm.closest('.col').attr('data-index');
            colIndexStart = colIndex;
            cellID = itm.attr('data-index');
            remove_cell(s,cellID);
            update_indexes(s,colIndex);
          } else {
            s.cellType = itm.attr('data-type');
            itm.addClass('__new');
          }
        },
        onAdd: function(evt){
          var itm = $(evt.item);
          if(itm.hasClass('drag-item')){
            console.log('added!');
            s.cellType = itm.attr('data-type');
            var itm = $(evt.item);
            colIndex = itm.closest('.col').attr('data-index');
            colID = itm.closest('.col').attr('data-index');
            s.index = itm.index();
            block = create_cell(s,colID);
            update_indexes(s,colIndex);
            display_update_cells(s,colIndex);
            started = false;
            console.log(s.pagearr);
          }

        },
        onEnd: function(evt){
          console.log('moved!');
          var itm = $(evt.item);
          s.cellType = itm.attr('data-type');
          var itm = $(evt.item);
          colIndex = itm.closest('.col').attr('data-index');
          colID = itm.closest('.col').attr('data-index');
          s.index = itm.index();

          console.log(started);
          console.log(itm);

          if(itm.hasClass('__new')){
            block = create_cell(s,colID);
            update_indexes(s,colIndex);
          }
          else {
            block = create_cell(s,colID,true);
            update_indexes(s,colIndex);
            display_update_cells(s,colIndexStart);
          }
          display_update_cells(s,colIndex);
          started = false;
          console.log(s.pagearr);
        }
      });
    });







  }

  function displayAllBlocks(){
    for(var j=0; j<s.pagearr.content.length; j++){
      display_update_row(s,s.pagearr.content[j].settings.index);
    }
  }

//---events
  // $('body').on('click','.expandMenu',function(){
  //   console.log('clicked');
  //   var menu = $(this).closest('.sideMenu');
  //   menuToggle(menu);
  // });

  $('body').on('click','.__addRow',function(e){
    var action = $(this).attr('data-action');
    var block = create_row(s);
    if(action=='prepend'){
      var len = s.pagearr.content.length-1;
      array_move(s.pagearr.content,len,0);
    }
    updateAllIndexes(s);
    displayAllBlocks();
  });

  $('body').on('click','.control_toggle',function(){
    setEditing($(this));
    var block = s.editingBlock;
    var menu = $('.__settings');
    menuToggle(menu,true);
    if(menu.hasClass('active')){
      menuLoad(block.type,menu,block.settings);
    }
  });

  $('body').on('click','.counter .ctrl',function(){
    var par = $(this).closest('.counter');
    var inp = $('input',par);
    var val = parseInt(inp.val());
    if($(this).hasClass('__up') && val<5){
      inp.val(val+1);
    }
    if($(this).hasClass('__down') && val>1){
      inp.val(val-1);
    }
  });

  $('body').on('click','.__settings .__save',function(){
    var menu = $('.__settings.__window');

    var block = findBlock(s,s.editingIndex);

    $(':input',menu).each(function(index,data) {
      if($(this).closest('.tRow').hasClass('double')){
        var name = $(this).closest('.tCell').attr('value');
      } else {
        var name = $(this).closest('.table').find('.tCell:first').attr('value');
      }
      var value = $(this).val();
      var label = $(this).attr('data-label');
      if(label) name = name+'_'+label;
      block.settings[name] = value;
      console.log(block);
    });

    managePageParts(block,s);
    if(block.type == 'row'){
      display_update_row(s,block.settings.index);
    } else {
      var div = $('.capsule[data-index="'+block.settings.index+'"]');
      var index = div.closest('.row.capsule').attr('data-index');
      display_update_row(s,index);
    }

    menuToggle(menu,false);
  });

  $('body').on('click','.__settings .__cancel',function(){
    var menu = $('.__settings.__window');
    menuToggle(menu,false);
  });

  $('body').on('click','.__settings .__delete',function(){
    if(!$(this).hasClass('selected')) {
      $(this).addClass('selected');
    } else {
      $(this).removeClass('selected');
      console.log(s.editingIndex);
      findBlock(s,s.editingIndex,true); //delete block
      console.log(s.pagearr);

      s.page.html('');
      var menu = $('.__settings.__window');
      menuToggle(menu,false);
      updateAllIndexes(s);
      displayAllBlocks();
    }
  });

  $(document).mouseup(function (e){
    var thing = $('.__settings .__delete');
    if (!thing.is(e.target)
      && thing.has(e.target).length === 0
      ){
        thing.removeClass('selected');
      }
  });


  $('body').on('click','.__selector',function(e){
    $global.settings.mode = 'default';
    s.editRow = $(this).closest('.tCell');
    $global.settings.editRow = s.editRow;
    $('#sel').trigger({
       type: "win:open",
       ele: $(this)
    });
  });

//--selection
  $('body').on('selection:updated',function(e){
    if(e.vars.action=='cancel'){
      $('#sel').hide();
      return;
    }

    console.log(e.vars);
    var checked = e.vars.checked;

    var replaced = false;
    if($global.settings.mode=='editor'){
      nodes = $global.nodes;
      nodes.forEach(function(v,i) {
        console.log(v.tagName);
        if(v.tagName == 'A'){
          console.log('anchor link! Index: '+i);
          nodes[i].setAttribute('href', checked.url);
          nodes[i].setAttribute('data-mce-href', checked.url);
          nodes[i].setAttribute('data-id', checked.id);
          nodes[i].setAttribute('data-type', checked.type);
          replaced = true;
          return;
        }
      });
      if(!replaced){
        var text = $global.editor.selection.getContent();
        $global.editor.selection.setContent("<a href='"+checked.url+"' data-id='"+checked.id+"' data-type='"+checked.type+"'>"+text+"</a>");
      }
    } else {
      var set = s.editRow;
      $('input[data-label="id"]',set).val(checked.id);
      $('input[data-label="name"]',set).val(checked.name);
      $('input[data-label="type"]',set).val(checked.type);
      $('input[data-label="url"]',set).val(checked.url);
    }

    $('#sel').hide();
  });

//---TinyMCE
tinymce.init({
  selector: '#mytextarea',
  theme: 'modern',
  convert_urls: false,
  relative_urls: false,
  plugins: 'link lists textcolor colorpicker code',
  menubar: false,
  height: 400,
  toolbar: 'formatselect bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | forecolor removeformat | bullist numlist | blockquote | undo redo | mybutton unlink | code',
  setup: function (editor) {
    editor.addButton('mybutton', {
      text: false,
      icon: 'link',
      onclick: function () {
        $global.settings.mode = 'editor';
        $global.nodes = editor.dom.getParents(editor.selection.getNode());
        $global.editor = editor;
        $('#sel').trigger({
           type: "win:open",
           ele: this.$el
        });
      },
      onpostrender: function() {
        var btn = this;
        editor.on('init', function() {
          $global.editor = editor;
          btn.$el.attr('source','url');
        });
      }
    });
  }
});


$('body').on('click','.__log',function(){
  console.log(tinymce.get('mytextarea').getContent()); //Get the content
});
$('body').on('click','.__add',function(){
  tinymce.get('mytextarea').setContent('Some text here'); //Set the content
});

//custom links for text window...
$('body').on('click','.__selection',function(){
  var ed = tinymce.get('mytextarea');
  // console.log(tinymce.get('mytextarea').selection.getContent()); //Get the selected content
  var nodes = ed.dom.getParents(ed.selection.getNode());
  nodes.forEach(function(v,i) {
    console.log(v.tagName);
    if(v.tagName == 'A'){
      console.log('anchor link! Index: '+i);
      nodes[i].setAttribute('href', 'foo');
      nodes[i].setAttribute('data-mce-href', 'foo');
    }
  });
  // tinymce.get('mytextarea').selection.setNode(tinymce.get('mytextarea').dom.create('a', {href: 'some.gif'}, 'hey'));

});

$('body').on('click','.winopen',function(){
  s.editRow = $(this).closest('.tCell');
  $global.settings.editRow = s.editRow;
  var open = $('.__window:visible').length;
  var win = $(this).attr('window');
  if(!win) return;

  if(win == 'gp'){
    show_gallery();
    return;
  }
  win = $('.__window.'+win);
  if(open > 0){
    win.show();
    win.css('box-shadow','0px 0px 15px 0px rgba(0,0,0,0.75)');
  } else {
    $('#blackout').fadeIn(250);
    win.fadeIn(250);
  }

  if(s.editingBlock.type=="text_cell"){
    if(s.editingBlock.settings.text){
      $global.editor.setContent($('input',s.editRow).val());
    } else {
      $global.editor.setContent('');
    }
  }

});

$('body').on('click','.text_editor .__save',function(){
  var text = $global.editor.getContent();
  console.log(text);
  $('input',s.editRow).val(text);
  // s.editingBlock.settings.text = text;
  // console.log(s.editingBlock);
});

$('body').on('click','.winclose',function(){
  if($(this).is('.button-disabled, .disabled')) return;
  var open = $('.__window:visible').length;
  var win = $(this).closest('.__window');
  console.log(open);
  if(open == 1){
    $('#blackout').fadeOut(250);
    win.fadeOut(250);
  } else {
    win.hide();
  }
});

//saving
$('form#post').submit(function(e) {

  // e.preventDefault(); //REMOVE THIS

  var $form = $(this);

  // $form.unbind(); //PAUSE DEFAULT WORDPRESS BEHAVIOUR
  //
  var activeEditor = tinyMCE.get('content');
  activeEditor.setContent(JSON.stringify(s.pagearr));

  $('.invis textarea').val(JSON.stringify(s.pagearr));

  var data = {
    action: 'pageSave',
    page: s.pagearr,
    id: postID,
  };
  var $this = $(this);
  $.post(ajaxurl, data, function(r){
    r = $.parseJSON(r);
    console.log(r);
    s.pagearr.settings.id = r.id;
    // setTimeout(function(){
      // $this.removeClass('active');
      // $this.removeClass('disabled');
    // },600);
    // $form.submit(); //RESUME DEFAULT WORDPRESS BEHAVIOUR

    return true; //RE-ENABLE

  });

  // submit after 5 seconds
  // setTimeout(function() {
  //   $form.submit();
  // }, 5000);
});






$('body').on('click','.modules .expander',function(){
  var par = $(this).closest('.modules');
  var height = $('.inner ul',par).height();
  if($(this).hasClass('active')){
    $(this).removeClass('active');
    $('.inner',par).css('height','100px');
  } else {
    $(this).addClass('active');
    $('.inner',par).css('height',height+'px');
  }
});


//---WP Gallery
// Set all variables to be used in scope
var frame,
    metaBox = $('body'), // Your meta box id here
    addImgLink = metaBox.find('.upload-custom-img'),
    delImgLink = metaBox.find('.delete-custom-img'),
    imgContainer = metaBox.find('.custom-img-container'),
    imgIdInput = metaBox.find('.custom-img-id');

// ADD IMAGE LINK
function show_gallery(){
    // If the media frame already exists, reopen it.
    if (frame) {
        frame.open();
        return;
    }
    // Create a new media frame
    frame = wp.media({
        title: 'Select Image',
        button: {
            text: 'Use this media'
        },
        multiple: false // Set to true to allow multiple files to be selected
    });
    // When an image is selected in the media frame...
    frame.on('select', function() {
        // Get media attachment details from the frame state
        var attachment = frame.state().get('selection').first().toJSON();
        console.log(attachment);
        $('input[name="bgrnd"]',s.editRow).val(attachment.id);
        $('input[data-label="url"]',s.editRow).val(attachment.sizes.thumbnail.url);
    });
    // Finally, open the modal on click
    frame.open();
}

//copy / Paste
$('body').on('click','.__copy',function(){
  var str = JSON.stringify(s.editingBlock);
  localStorage.setItem("pageBuilder_clipboard", str);
});

$('body').on('click','.__paste',function(){
  var row = JSON.parse(localStorage.getItem("pageBuilder_clipboard"));
  replaceBlock(s,s.editingIndex,row);

  updateAllIndexes(s);
  s.page.html('');
  displayAllBlocks();

  var menu = $('.__settings.__window');
  menuToggle(menu,false);
});

$('body').on('click','.winopen.__code',function(){
  console.log(s.editingBlock);
  $('.__window.code textarea').val($('input',s.editRow).val());
});
$('body').on('click','.__window.code .__save',function(){
  console.log($('.__window.code textarea').val());
  $('input',s.editRow).val($('.__window.code textarea').val());
});

//---load
$('body').on('pageBuilder_ready', function() {

  $('#blackout').appendTo('body');

  // var activeEditor = tinyMCE.get('content');
  // var content = String(activeEditor.getContent());
  // content = content.replace('<p>{','{');
  // content = content.replace('}</p>','}');
  // if(content) var post_content = JSON.parse(content);

  s.pagearr = {
    type:'page',
    settings:{
      title:null,
      id:null,
      template:'normal'
    },
    content:[]
  };

  //get page data
  var data = {
    action: 'pageGetSingle',
    id: postID
  };
  $.post(ajaxurl, data, function(r){
    r = JSON.parse(r);
    var page = r.page;
    try {
      console.log('loading in from pageBuilder');
      page.content = JSON.parse(page.content);
      s.pagearr = page.content;
    }
    catch(e) {
      var val = $('.invis textarea').val();
      var json = JSON.parse(val);
      s.pagearr = json;
      console.log('loading in from wp_posts');
    }

    // s.pagearr = JSON.parse($('.invis textarea').val());
    // console.log($('.invis textarea').val());

    if(!s.pagearr) s.pagearr = {};
    if(!("settings" in s.pagearr)) {
      s.pagearr.settings = {};
    }
    if(!("content" in s.pagearr)) {
      s.pagearr.content = [];
    }

    s.pagearr.settings.page_color = r.page_color;

    console.log(s.pagearr);
    displayAllBlocks();
  });


  s.object=[];
  s.object.row = {
    type:'row',
    settings:{
      cols:3,
      wrap:'true',
      padding:'grid',
      align: 'middle',
      mcols: 1
    },
    content:[]
  }
  s.object.col = {
    type:'col',
    settings:{},
    content:[]
  }
  s.object.text_cell = {
    type:'text_cell',
    settings:{ //put some default settings in here x:x
      text:null,
      text_color: '000000'
    },
    content:[]
  }
  s.object.button_cell = {
    type:'button_cell',
    settings:{ //put some default settings in here x:x
      text:null
    },
    content:[]
  }
  s.object.image_cell = {
    type:'image_cell',
    settings:{ //put some default settings in here x:x
      bgrnd:null,
      bgrnd_url:null,
      text:null,
      display_type:'full'
    },
    content:[]
  }
  s.object.video_cell = {
    type:'video_cell',
    settings:{ //put some default settings in here x:x
      //
    },
    content:[]
  }
  s.object.slider_cell = {
    type:'slider_cell',
    settings:{ //put some default settings in here x:x
      sliderID: null,
      sliderName: null
    },
    content:[]
  }
  s.object.header_cell = {
    type:'header_cell',
    settings:{height:'half'},
    content:[]
  }
  s.object.form_cell = {
    type:'form_cell',
    settings:{ //put some default settings in here x:x
      formID: null,
      formName: null
    },
    content:[]
  }
  s.object.schedule_cell = {
    type:'schedule_cell',
    settings:{ //put some default settings in here x:x
    },
    content:[]
  }
  s.object.code_cell = {
    type:'code_cell',
    settings:{ //put some default settings in here x:x
      code:null
    },
    content:[]
  }
  s.object.coupon_cell = {
    type:'coupon_cell',
    settings:{},
    content:[]
  }
  s.object.portfolio_cell = {
    type:'portfolio_cell',
    settings:{ //put some default settings in here x:x
      folioID: null,
      folioName: null
    },
    content:[]
  }
  s.object.card_cell = {
    type:'card_cell',
    settings:{ //put some default settings in here x:x
    },
    content:[]
  }
  s.object.separator_cell = {
    type:'separator_cell',
    settings:{ //put some default settings in here x:x
    },
    content:[]
  }

  //cell items
  var el = $('li','#pageItems');
  $(el).each(function (i,e) {
    var sortable = Sortable.create(e, {
      group: {name:"pageBuilder_cells", pull:"clone", put:false},
      sort: false,
    });
  });



  //cell items
  // var el = document.getElementById("pageItems");
  // Sortable.create(el, {
  //   group: {name:"pageBuilder_cells", pull:"clone", put:false},
  //   sort: false,
  //   onEnd: function(evt){
  //
  //   }
  // });

  //rows
  var rowIndex;
  var el = document.getElementById("page_content");
  Sortable.create(el, {
    group: {name:"pageBuilder_rows", put:true},
    handle: ".handle",
    sort: true,
    filter: ".control_toggle, .cell",
    onStart: function(evt){
      var itm = $(evt.item);
      rowIndex = itm.index() - 1;
    },
    onEnd: function(evt){
      var itm = $(evt.item);
      var newRowIndex = itm.index();
      console.log(rowIndex+':'+newRowIndex);
      array_move(s.pagearr.content,rowIndex,newRowIndex);
      console.log(s.pagearr);
      updateAllIndexes(s);
      var scroll = $('.main','.page').scrollTop();
      s.page.html('');
      displayAllBlocks();
      $('.main','.page').scrollTop(scroll);
    }
  });

  // var el = $('.row');
  // $(el).each(function (i,e) {
  //   var sortable = Sortable.create(e, {
  //     group: {name:"pageBuilder", pull:"clone", put:false},
  //     sort: false,
  //   });
  // });

  // var index;
  // var el = document.getElementById("drop");
  // Sortable.create(el, {
  //   group: {name:"form", pull:false, put:true},
  //   animation: 150,
  //   handle: ".handle",
  //   onChoose: function (evt) {
  //     var itm = evt.item;
  //     $('.module-content',itm).hide();
  //     $('.collapsable',itm).removeClass('active');
  //   },
  //   onAdd: function (evt) {
  //     var itm = evt.item;
  //     var type = $(itm).attr('data-type');
  //     var tmp = $(itm).attr('data-tmp');
  //     var item = $.extend(true, {}, s.object.item);
  //     var option = $.extend(true, {}, s.object.option);
  //     item.type = type;
  //     item.tmp = tmp;
  //     if(tmp != 'input'){
  //       item.content.push(option);
  //     }
  //     var _index = $(itm).index();
  //     s.pagearr.content.splice(_index, 0, item);
  //     updateIndexes_1D(s);
  //     displayAll(s);
  //   },
  //   onStart: function (evt) {
  //     var itm = evt.item;
  //     index = evt.oldIndex;
  //     $('.module-content',itm).hide();
  //     $('.collapsable',itm).removeClass('active');
  //   },
  //   onEnd: function (evt) {
  //     s.pagearr.content.move(index,evt.newIndex);
  //     updateIndexes_1D(s);
  //     displayAll(s);
  //   },
  // });

});

});
