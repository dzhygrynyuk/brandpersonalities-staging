var $global = {};
$global.settings = {};
$global.vars = {};

String.prototype.stripSlashes = function(){
    return this.replace(/\\(.)/mg, "$1");
}

Array.prototype.move = function (old_index, new_index) {
    if (new_index >= this.length) {
        var k = new_index - this.length;
        while ((k--) + 1) {
            this.push(undefined);
        }
    }
    this.splice(new_index, 0, this.splice(old_index, 1)[0]);
    return this; // for testing purposes
};

jQuery(document).ready(function( $ ) {
  var s = {};

  function setEditing(x){
    s.editing = x.closest('.list-item');
    s.editingIndex = s.editing.attr('data-index');
    var block = findBlock_1D(s,s.editingIndex);
    s.editingBlock = block;
  }

  function menuToggle(x,show){
    $('.__delete',x).removeClass('selected');
    if(x.is(':visible') && show!=true){
      x.removeClass('active');
      x.hide();
    }
    else if(!x.is(':visible') && show!=false){
      x.addClass('active');
      x.show();
    }
  }

  function menuLoad(x,y,data){
    var extra = x+'_extra';
    x = x+'_settings';
    if(!data) var data = {};
    var part = load_template(x,data);
    $('.__content',y).html(part);
    $('.loader',y).removeClass('visible');
    $('.dropy',y).dropy();
  }

  function load_template(x,data){
    var html = ejs.render(templates[x],data);
    return html;
  }

  function findBlock_1D(s,index,remove){
    index = parseInt(index);
    var block = s.pagearr.content[index];
    if(remove) s.pagearr.content.splice(index,1);
    return block;
  }

  function updateIndexes_1D(s){
    var block = s.pagearr;
    for(var i=0; i<block.content.length; i++){
      block.content[i].settings.index = i;
    }
  }

  function displayAll(s){
    s.page.html('');
    for(var k=0; s.pagearr.content.length>k; k++){
      $.each(s.pagearr.content[k].settings,function(i,v){
        if(v){
          if(typeof v == 'string'){
            s.pagearr.content[k].settings[i] = v.stripSlashes(); //STRIP SLASHES FOR ALL STORED VALUES
          }
        }
      });
      var template = load_template('item',s.pagearr.content[k].settings);
      s.page.append(template);
    }
  }

  $('body').on('click','.action-row .__delete',function(){
    if(! s.pagearr.settings.id) return;
    var data = {
      action: 'sliderDelete',
      id: s.pagearr.settings.id
    };
    $.post(ajaxurl, data, function(r){
      s.pagearr = {
        content:[],
        settings:{}
      }
      s.page.html('');
      $('#slider_name').val('');
      loadSideItems();
    });
  });

  $('body').on('click','.save-row .__save',function(){
    if($(this).hasClass('disabled')) return;
    $(this).addClass('disabled');
    $(this).addClass('active');

    var data = {
      action: 'couponsSave',
      content: s.pagearr
    };
    var $this = $(this);
    $.post(ajaxurl, data, function(r){
      $this.removeClass('disabled');
      setTimeout(function(){
        $this.removeClass('active');
      },600);
    });
  });

  $('body').on('click','.control.__del',function(){
    var index = $(this).closest('.list-item').attr('data-index');
    s.editing = findBlock_1D(s,index,true);
    updateIndexes_1D(s);
    displayAll(s);
  });

  $('body').on('click','.__coupon',function(){
    var item = $.extend(true, {}, s.object.item);
    // item.settings = {id:id,url:url};
    s.pagearr.content.push(item);
    updateIndexes_1D(s);
    console.log(s.pagearr);
    displayAll(s);
  });

  $('body').on('click','.__set',function(){
    setEditing($(this));
    var block = s.editingBlock;
    var menu = $('.__settings');
    menuToggle(menu,true);
    if(menu.hasClass('active')){
      menuLoad(block.type,menu,block.settings);
    }
  });

  $('body').on('click','.__settings .__save',function(){
    var menu = $('.__settings.__window');

    var block = s.editingBlock;

    $(':input',menu).each(function(index,data) {
      if($(this).closest('.tRow').hasClass('double')){
        var name = $(this).closest('.tCell').attr('value');
      } else {
        var name = $(this).closest('.table').find('.tCell:first').attr('value');
      }
      var value = $(this).val();
      var label = $(this).attr('data-label');
      if(label) name = name+'_'+label;
      block.settings[name] = value;
      console.log(block);
    });

    updateIndexes_1D(s);
    displayAll(s);

    menuToggle(menu,false);
  });

  $('body').on('click','.__settings .__cancel',function(){
    var menu = $('.__settings.__window');
    menuToggle(menu,false);
  });

  $('body').on('click','.__settings .__delete',function(){
    if(!$(this).hasClass('selected')) {
      $(this).addClass('selected');
    } else {
      $(this).removeClass('selected');
      findBlock_1D(s,s.editingIndex,true); //delete block

      s.page.html('');
      var menu = $('.__settings.__window');
      menuToggle(menu,false);
      updateIndexes_1D(s);
      displayAll(s);
    }
  });

  $(document).mouseup(function (e){
    var thing = $('.__settings .__delete');
    if (!thing.is(e.target)
      && thing.has(e.target).length === 0
      ){
        thing.removeClass('selected');
      }
  });

  $('body').on('click','.jcheck',function(){
    if($(this).hasClass('disabled')) return;

    var editing = $(this).closest('.list-item');
    var editingIndex = editing.attr('data-index');
    var block = findBlock_1D(s,editingIndex);

    if($(this).hasClass('active')){
      $(this).removeClass('active');
      block.settings.enabled = false;
    } else {
      $(this).addClass('active');
      block.settings.enabled = true;
    }
  });

  //---load
  $('body').on('coupons_ready', function() {

    s = {};
    s.object = {};
    s.page = $('#sortable');

    s.pagearr = {
      content:[],
      settings:{
        type:'coupons'
      }
    }

    s.object.item = {
      type:'coupon',
      settings:{
        enabled:true
      }
    }

    var data = {
      action: 'couponsGet'
    };
    var $this = $(this);
    $.post(ajaxurl, data, function(r){
      r = JSON.parse(r);
      s.pagearr = r;
      $.each(s.pagearr.content,function(i,v){
        $.each(v.settings,function(key,val){
          if(val=='true') v.settings[key] = true;
          if(val=='false') v.settings[key] = false;
        });
      });
      console.log(s.pagearr);
      if(!s.pagearr) s.pagearr = {};
      if(!("settings" in s.pagearr)) {
        s.pagearr.settings = {};
      }
      if(!("content" in s.pagearr)) {
        s.pagearr.content = [];
      }
      updateIndexes_1D(s);
      displayAll(s);
    });

    //--sortable
    var el = document.getElementById("sortable");
    Sortable.create(el, {
      animation: 150,
      handle: ".handle",
      onStart: function (evt) {
        var itm = evt.item;
        s.index = evt.oldIndex;
      },
      onEnd: function (evt) {
        var itm = evt.item;
        s.pagearr.content.move(s.index,evt.newIndex);
        updateIndexes_1D(s);
        displayAll(s);
      },
    });
  });

});
