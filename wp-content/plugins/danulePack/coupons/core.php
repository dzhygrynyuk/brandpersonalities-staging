<?php

add_action( 'wp_ajax_couponsSave', 'save_coupons' );
add_action( 'wp_ajax_couponsGet', 'get_coupons' );

function save_coupons(){
  $content = $_POST['content'];
  update_option( 'coupons', $content );
  wp_die();
}

function get_coupons(){
  $coupons = get_option('coupons');
  echo json_encode($coupons);
  wp_die();
}

?>
