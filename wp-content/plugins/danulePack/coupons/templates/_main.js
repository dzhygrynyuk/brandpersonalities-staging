var templates = {};
jQuery(document).ready(function( $ ) {

  console.log(ajaxurl);
  console.log(plugins_url);

  var dir = plugins_url+'templates/';
  var files = ['item','coupon_settings'];

  var len = files.length - 1;
  $.each(files,function(i,v){
    var f = dir+v+'.ejs?v='+Date.now();
    $.get(f, function (file) {
      templates[v] = file;
      if(i==len) {
        $('body').trigger('coupons_ready');
      }
    });
  });
});
