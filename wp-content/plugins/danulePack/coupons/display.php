<div class="wrap">

  <div class="side"></div>

  <div class="main">

    <div class="save-row">
      <input type="text" id="coupons_name" placeholder="Coupons" value="Coupons" class="disabled" disabled>
      <div class="button button-primary icr __save"><div class="_c">Save</div></div>
    </div>

    <div class="action-row">
      <div class="button icl __coupon"><div class="_c">Add Coupon</div></div>
    </div>

    <div class="loader spin icc"></div>

    <div id="items">
      <ul id="sortable"></ul>
    </div>

  </div>


</div>
