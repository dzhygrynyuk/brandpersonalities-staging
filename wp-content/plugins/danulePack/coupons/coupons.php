<?php
//--AJAX / Files
if(is_admin()){
	include 'core.php';
} else {
	include 'frontend/display.php';
}

function coupons_load_frontend() {
	wp_enqueue_style( 'coupons_style', plugins_url( '/', __FILE__ ) .	'frontend/style.css?v=1.02' );
}
add_action( 'wp_enqueue_scripts', 'coupons_load_frontend' );

//---------SETTINGS PAGE

function coupons_styles() {
	if($_GET['page']=='coupons'){
		wp_enqueue_style( 'coupons_style', plugins_url( '/', __FILE__ ) . 'style.css');
		wp_enqueue_style( 'dropy', plugins_url( '/../', __FILE__ ) . 'modules/dropy/dropy.css' );

		//scripts
		wp_enqueue_script( 'dropy', plugins_url( '/../', __FILE__ ) . 'modules/dropy/dropy.js', array('jquery') );
		wp_enqueue_script( 'coupons_ejs', plugins_url( '/', __FILE__ ) . 'js/ejs.min.js', array('jquery') );
		wp_enqueue_script( 'coupons_tmp', plugins_url( '/', __FILE__ ) . 'templates/_main.js', array('jquery') );
		wp_enqueue_script( 'coupons_sortable', plugins_url( '/', __FILE__ ) . 'js/sortable.min.js', array('jquery') );
		wp_enqueue_script( 'coupons_script', plugins_url( '/', __FILE__ ) . 'scripts.js', array('jquery') );
		wp_localize_script( 'coupons_tmp', 'plugins_url', plugins_url( '/', __FILE__ ) );
	}
}
add_action( 'admin_enqueue_scripts', 'coupons_styles' );


// add the admin options page
function coupons_admin_add_pages() {
	//Main menu item
	add_menu_page('Coupons', 'Coupons', 'manage_options', 'coupons', 'coupons_options_page','dashicons-tickets-alt');
}
add_action('admin_menu', 'coupons_admin_add_pages');

//MAIN
function coupons_options_page() {
  global $core;

	include $core.'modules/window/settings.php';
  include 'display.php';
}

?>
