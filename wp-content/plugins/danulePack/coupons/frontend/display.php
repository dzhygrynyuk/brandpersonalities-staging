<?php

function display_coupons($settings){
  ob_start();
  $coupons = get_option('coupons');
  ?>
  <div class="coupons-contain">
    <?php foreach($coupons['content'] as $c){
      $s = $c['settings'];
      if($s['enabled']!='true') continue;
      ?>
      <div class="c-col">
        <div class="coupon">
          <img src="<?php echo get_stylesheet_directory_uri() ?>/img/coupon.png">
          <div class="coupon-float">
            <div class="c-pre"><?php echo $s['pre']; ?></div>
            <div class="c-discount"><?php echo $s['discount']; ?></div>
            <div class="c-text"><?php echo $s['text']; ?></div>
            <div class="c-post"><?php echo $s['post']; ?></div>
          </div>
        </div>
      </div>
    <?php } ?>
  </div>

  <?php return ob_get_clean();
}


?>
