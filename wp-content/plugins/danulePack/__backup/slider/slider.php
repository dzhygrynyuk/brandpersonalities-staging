<?php
//--AJAX / Files
if(is_admin()){
	include 'core.php';
} else {
	include 'frontend/display.php';
}

function slider_load_frontend() {
	wp_enqueue_style( 'slider_style', plugins_url( '/', __FILE__ ) .	'frontend/style.css?v=1.02' );
	wp_enqueue_style( 'owl_style', plugins_url( '/', __FILE__ ) .	'frontend/owl.carousel.css' );
	wp_enqueue_style( 'owl_theme_style', plugins_url( '/', __FILE__ ) .	'frontend/owl.theme.default.css' );

	wp_enqueue_script( 'owl_script', plugins_url( '/', __FILE__ ) .	'frontend/owl.carousel.min.js', array('jquery') );
	wp_enqueue_script( 'slider_script', plugins_url( '/', __FILE__ ) .	'frontend/scripts.js?v=1.02', array('jquery') );
}
add_action( 'wp_enqueue_scripts', 'slider_load_frontend' );

//---------SETTINGS PAGE

function slider_styles() {
	if($_GET['page']=='slider'){
		wp_enqueue_style( 'slider_style', plugins_url( '/', __FILE__ ) . 'style.css?v=1.01');
    wp_enqueue_style( 'selection_style', plugins_url( '/../', __FILE__ ) . 'modules/selection/style.css?v=1.01');

		//scripts
		wp_enqueue_script( 'slider_tinymce', plugins_url( '/../', __FILE__ ) . 'modules/tinymce/tinymce.min.js', array('jquery') );
		wp_enqueue_script( 'slider_tinymceJ', plugins_url( '/../', __FILE__ ) . 'modules/tinymce/jquery.tinymce.min.js', array('jquery') );
		wp_enqueue_script( 'slider_ejs', plugins_url( '/', __FILE__ ) . 'js/ejs.min.js', array('jquery') );
		wp_enqueue_script( 'slider_tmp', plugins_url( '/', __FILE__ ) . 'templates/_main.js', array('jquery') );
		wp_enqueue_script( 'slider_sortable', plugins_url( '/', __FILE__ ) . 'js/sortable.min.js', array('jquery') );
		wp_enqueue_script( 'slider_script', plugins_url( '/', __FILE__ ) . 'scripts.js?v=1.02', array('jquery') );
    wp_enqueue_script( 'selection_js', plugins_url( '/../', __FILE__ ) . 'modules/selection/scripts/core.js?v=1.02', array('jquery') );
    wp_enqueue_script( 'jscolor_js', plugins_url( '/', __FILE__ ) . 'js/jscolor.min.js', array('jquery') );
		wp_localize_script( 'slider_tmp', 'plugins_url', plugins_url( '/', __FILE__ ) );
	}
}
add_action( 'admin_enqueue_scripts', 'slider_styles' );


// add the admin options page
function slider_admin_add_pages() {
	//Main menu item
	add_menu_page('Slider', 'Slider', 'manage_options', 'slider', 'slider_options_page','dashicons-format-gallery');
}
add_action('admin_menu', 'slider_admin_add_pages');

//MAIN
function slider_options_page() {
  global $core;
  wp_enqueue_media();

  include $core.'modules/selection/display_window.php';
  include $core.'modules/window/text.php';
  include $core.'modules/window/settings.php';
  include 'display.php';
}

?>
