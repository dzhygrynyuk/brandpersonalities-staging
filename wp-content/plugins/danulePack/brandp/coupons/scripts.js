jQuery(document).ready(function( $ ) {

  var s = {};
  s.adding = false;

  function load_template(x,data){
    var html = ejs.render(templates[x],data);
    return html;
  }

  function display(){
    s.adding = false;

    var data = {
      action:'bpGetCoupons'
    };

    $.post(ajaxurl, data, function(r){
      r = JSON.parse(r);
      console.log(r);

      $('#content').html('');
      $.each(r,function(i,v){
        var html = load_template('item',{data:v});
        $('#content').append(html);
      });

    });
  }

  $('body').on('click','.__add',function(){
    if(s.adding == true) return;

    s.adding = true;

    var html = load_template('item',{action:'insert'});
    $('#content').prepend(html);
  });

  $('body').on('click','.__save',function(){
    if($(this).hasClass('disabled')) return;
    var par = $(this).closest('.flex-row');
    var code = $('.__code',par).val();
    var amount = $('.amount',par).val();
    var type = $('.type',par).val();

    $this = $(this);
    $this.addClass('active disabled');

    var data = {
      action:'bpSaveCoupon',
      code:code,
      amount:amount,
      type:type
    };

    $.post(ajaxurl, data, function(r){
      console.log(r);
      $this.removeClass('active');
      if($this.hasClass('__insert')){
        s.adding = false;
        display();
      }
      if($this.hasClass('__update')){
        $this.removeClass('disabled');
      }
    });

  });

  $('body').on('click','.__delete',function(){
    if($(this).hasClass('disabled')) return;
    var par = $(this).closest('.flex-row');
    var code = $('.__code',par).val();

    $this = $(this);
    $this.addClass('disabled');

    var data = {
      action:'bpDeleteCoupon',
      code:code
    };

    $.post(ajaxurl, data, function(r){
      display();
    });

  });

  $('body').on('brandp_ready',function(){

    display();

  });

});
