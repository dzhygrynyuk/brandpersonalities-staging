<div class="wrap">

  <div class="action-row">
    <div class="__add button icl">Add Coupon</div>
    <!-- <div class="__save button button-primary icl">Save</div> -->
  </div>

  <div class="flex-contain">
    <div class="flex-row header">
      <div class="flex-cell">Code</div>
      <div class="flex-cell">Discount</div>
      <div class="flex-cell">Type</div>
      <div class="flex-cell __small"></div>
    </div>
    <div id="content"></div>
  </div>

</div>
