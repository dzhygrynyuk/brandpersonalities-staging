jQuery(document).ready(function( $ ) {

  var s = {};

  function loadMatrix(type){
    $('.pulse').removeClass('active');
    $('.loader').addClass('active');
    $('.matrix-contain').html('');

    var data = {
      action:'bpGetMatrix',
      type:type
    }
    $.post(ajaxurl, data, function(r){
      $('.loader').removeClass('active');
      $('.matrix-contain').html(r);
    });
  }

  $('body').on('change','#bp-type',function(){
    s.type = $(this).val();
    loadMatrix(s.type);
  });

  $('body').on('change','.mtrx input',function(){
    var table = $(this).attr('data-table');
    var id = $(this).attr('data-id');
    var value = $(this).val();

    var par = $(this).closest('.flex-cell');
    par.addClass('pending');

    var data = {
      action:'bpSave',
      table:table,
      id:id,
      value:value
    }
    $.post(ajaxurl, data, function(r){
      par.removeClass('pending');
      if(r==1){
        par.addClass('saved');
        window.setTimeout(function(){
          par.removeClass('saved');
        },500);
      }
    });
  });

});
