var templates = {};
jQuery(document).ready(function( $ ) {

  console.log(ajaxurl);
  console.log(plugins_url);

  var dir = plugins_url+'descriptions/templates/';
  var files = [
    'item',
    'sideItem',
    'settings',
    'empty',
    'editor',
    'editor_messages',
    'editor_messages_email'
  ];

  var len = files.length - 1;
  $.each(files,function(i,v){
    var f = dir+v+'.ejs?v='+Date.now();
    $.get(f, function (file) {
      templates[v] = file;
      if(i==len) {
        $('body').trigger('brandp_ready');
        console.log(templates);
      }
    });
  });
});
