var $global = {};
$global.settings = {};
$global.vars = {};

jQuery(document).ready(function( $ ) {

  var s;
  $global.save_mode = 'personality';

  function load_template(x,data){
    var html = ejs.render(templates[x],data);
    return html;
  }

  function display(){
    var html = load_template('editor',s.pagearr);
    $('.right .__content').html(html);
    tinymce_init();
    // $global.editor.setContent(s.pagearr.description);
  }

  function loadSideItems(){
    $('.sideItems_content').html('');
    $('.left .loader').addClass('active');
    var data = {
      action: 'bpLoadPersonalities'
    };
    $.post(ajaxurl, data, function(r){
      r = JSON.parse(r);
      console.log(r);
      $.each(r,function(i,v){
        var html = load_template('sideItem',v);
        $('.sideItems_content').append(html);
      });
      $('.left .loader').removeClass('active');
      $('#sideItems').show();
    });
  }

  //---TinyMCE
  function tinymce_init(){
    tinymce.remove();
    tinymce.init({
      selector: '.mytextarea',
      theme: 'modern',
      convert_urls: false,
      relative_urls: false,
      plugins: 'link lists',
      menubar: false,
      height: 200,
      toolbar: 'formatselect bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | removeformat | bullist numlist | blockquote | undo redo | mybutton link unlink',
      setup: function (editor) {
        $global.editor = editor;
        editor.on('blur', function(e) {
          var text = editor.getContent();
          var setting = $(editor.getContainer()).closest('.flex-cell').attr('value');
          s.pagearr[setting] = text;
        });
      }
    });
  }

  //---WP Gallery
  var frame;
  function show_gallery(){
    // If the media frame already exists, reopen it.
    if (frame) {
        frame.open();
        return;
    }
    // Create a new media frame
    frame = wp.media({
        title: 'Select Image',
        button: {
            text: 'Choose'
        },
        multiple: false // Set to true to allow multiple files to be selected
    });
    // When an image is selected in the media frame...
    frame.on('select', function() {
      // Get media attachment details from the frame state
      var attachment = frame.state().get('selection').first().toJSON();

      console.log(attachment);



      s.pagearr[s.editing_option+'_url'] = attachment.url;
      s.pagearr[s.editing_option] = attachment.id;

      display();

    });
    // Finally, open the modal on click
    frame.open();
  }

  $('body').on('click','.__media',function(){
    s.editing_option = $(this).closest('.flex-cell').attr('value');
    show_gallery();
  });

  $('body').on('click','#sideItems .sideItems_content li',function(){
    $global.save_mode = 'personality';
    var id = $(this).attr('data-id');
    $('#sideItems li').removeClass('active');
    $(this).addClass('active');

    var data = {
      action: 'bpGetPersonality',
      id: id
    };
    $.post(ajaxurl, data, function(r){
      r = JSON.parse(r);
      console.log(r);
      s.pagearr = r;
      display();
    });
  });

  $('body').on('click','#sideItems .sideItems_other li',function(){
    $global.save_mode = 'messages';
    var id = $(this).attr('data-id');
    $('#sideItems li').removeClass('active');
    $(this).addClass('active');

    var template = $(this).attr('data-template');

    var data = {
      action: 'bpGetMessages',
      messages: ['message_success','message_fail','message_before','message_before_fail','message_after']
    };
    $.post(ajaxurl, data, function(r){
      r = JSON.parse(r);
      console.log(r);
      s.pagearr = r;
      var html = load_template(template,s.pagearr);
      $('.right .__content').html(html);
      tinymce_init();
    });

  });

  $('body').on('click','.save-row .__save',function(){
    if($(this).hasClass('disabled')) return;
    $(this).addClass('disabled');
    $(this).addClass('active');

    if($global.save_mode == 'personality'){
      var data = {
        action: 'bpSavePersonality',
        content: s.pagearr
      };
      var $this = $(this);
      $.post(ajaxurl, data, function(r){
        $this.removeClass('disabled');
        setTimeout(function(){
          $this.removeClass('active');
        },600);
        // loadSideItems();
      });
    }

    if($global.save_mode == 'messages'){
      var data = {
        action: 'bpSaveMessages',
        content: s.pagearr
      };
      var $this = $(this);
      $.post(ajaxurl, data, function(r){
        $this.removeClass('disabled');
        setTimeout(function(){
          $this.removeClass('active');
        },600);
        // loadSideItems();
      });
    }

  });

  $('body').on('brandp_ready',function(){

    s = {};
    s.pagearr = {};

    loadSideItems();
    var html = load_template('empty',{});
    $('.right .__content').html(html);

    // display();

  });

});
