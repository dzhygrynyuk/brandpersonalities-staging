<div class="wrap">

  <div class="left">
    <div class="loader spin icc"></div>

    <ul id="sideItems">
      <div class="sideItems_other">
        <li class="side-item icr" data-template="editor_messages">Messages</li>
        <li class="side-item icr" data-template="editor_messages_email">Messages (E-Mail)</li>
      </div>
      <div class="sideItems_content"></div>
    </ul>
  </div>

  <div class="right">
    <div class="__content"></div>
  </div>

</div>
