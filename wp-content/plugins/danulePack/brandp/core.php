<?php

add_action( 'wp_ajax_bpGetResults', 'bp_get_results' );
function bp_get_results(){
  global $wpdb;

  $sql = "SELECT
  r.date as date,
  u.first as first,
  u.last as last,
  GROUP_CONCAT(p.value ORDER BY p.ID SEPARATOR ', ') personality,
  u.business as business,
  u.email as email,
  u.country as country,
  u.mode as mode,
  u.optin as optin,
  u.token as token,
  r.final_twelve as finaltwelve,
  r.final_choices as finalfive
  FROM bg_results r
  LEFT JOIN bg_users u ON r.user_id = u.ID
  LEFT JOIN bg_personalities p ON FIND_IN_SET(p.ID, r.result) > 0
  GROUP BY r.ID
  ORDER BY r.date DESC";

  $sql = $wpdb->prepare($sql,array());
  $result = $wpdb->get_results($sql,ARRAY_A);

  date_default_timezone_set('Australia/Sydney');
  $i = 0;
  foreach($result as $r){
    $r['date'] = date('Y-m-d g:ia',$r['date']);

    $result[$i] = $r;
    $i++;
  }

  echo json_encode($result);

  wp_die();
}

add_action( 'wp_ajax_bpDeleteCoupon', 'bp_delete_coupon' );
function bp_delete_coupon(){
  global $wpdb;

  $result = $wpdb->delete( 'bg_coupons', array( 'code' => $_POST['code'] ) );

  echo $result;

  wp_die();
}

add_action( 'wp_ajax_bpGetCoupons', 'bp_get_coupons' );
function bp_get_coupons(){
  global $wpdb;

  $sql = "SELECT * FROM bg_coupons ORDER BY code ASC";
  $sql = $wpdb->prepare($sql,array());
  $result = $wpdb->get_results($sql,ARRAY_A);

  echo json_encode($result);

  wp_die();
}

add_action( 'wp_ajax_bpSaveCoupon', 'bp_save_coupon' );
function bp_save_coupon(){
  global $wpdb;

  if(empty($_POST['code'])){
    echo 0;
    wp_die();
    return;
  }

  $data = array(
    'code'=>$_POST['code'],
    'amount'=>$_POST['amount'],
    'type'=>$_POST['type']
  );

  $result = $wpdb->replace( 'bg_coupons', $data );
  echo $result;

  wp_die();
}

add_action( 'wp_ajax_bpGetMatrix', 'bp_get_matrix' );
function bp_get_matrix(){
  $type = $_POST['type'];

  $q_table = '';
  $a_table = '';

  if($type == 'personal'){
    $q_table = 'bg_questions';
    $a_table = 'bg_answers';
  }
  if($type == 'business'){
    $q_table = 'bg_questions_business';
    $a_table = 'bg_answers_business';
  }
  include 'matrix.php';
  wp_die();
}

add_action( 'wp_ajax_bpSaveMessages', 'bp_save_messages' );
function bp_save_messages(){
  global $wpdb;

  $s = $_POST['content'];

  foreach($s as $k => $v){
    update_option($k,$v);
  }

  wp_die();
}

add_action( 'wp_ajax_bpGetMessages', 'bp_get_messages' );
function bp_get_messages(){
  global $wpdb;

  $s = $_POST['messages'];

  $options = array();
  foreach($s as $k){
    $options[$k] = stripslashes(get_option($k));
  }

  echo json_encode($options);

  wp_die();
}

add_action( 'wp_ajax_bpSavePersonality', 'bp_save_personality' );
function bp_save_personality(){
  global $wpdb;

  $s = $_POST['content'];

  $result = $wpdb->update('bg_personalities',
    array(
      'description'=>$s['description'],
      'image'=>$s['image'],
      'image_mobile'=>$s['image_mobile']
    ),
    array('ID'=>$s['ID'])
  );

  wp_die();
}

add_action( 'wp_ajax_bpGetPersonality', 'bp_get_personality' );
function bp_get_personality(){
  global $wpdb;

  $id = $_POST['id'];

  $sql = "SELECT *
  FROM bg_personalities
  WHERE ID = %s";
  $sql = $wpdb->prepare($sql,array($id));
  $result = $wpdb->get_row($sql,ARRAY_A);

  $result['image_url'] = wp_get_attachment_image_src($result['image'],'large')[0];
  $result['image_mobile_url'] = wp_get_attachment_image_src($result['image_mobile'],'large')[0];

  echo json_encode($result);

  wp_die();
}



add_action( 'wp_ajax_bpLoadPersonalities', 'bp_load_personalities' );
function bp_load_personalities(){
  global $wpdb;

  $sql = "SELECT *
  FROM bg_personalities";
  $sql = $wpdb->prepare($sql,array());
  $result = $wpdb->get_results($sql,ARRAY_A);

  echo json_encode($result);
  wp_die();
}

add_action( 'wp_ajax_bpSave', 'bp_save_input' );

function bp_save_input(){
  global $wpdb;

  $table = $_POST['table'];
  $id = $_POST['id'];
  $value = $_POST['value'];

  $result = $wpdb->update($table,
    array('value'=>$value),
    array('ID'=>$id)
  );
  if($result === false){
    echo 0;
  } else {
    echo 1;
  }
  wp_die();
}

?>
