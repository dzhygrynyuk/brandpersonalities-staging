jQuery(document).ready(function( $ ) {

  var s = {};

  function load_template(x,data){
    var html = ejs.render(templates[x],data);
    return html;
  }

  function display(){

    var data = {
      action:'bpGetResults'
    };

    $.post(ajaxurl, data, function(r){
      r = JSON.parse(r);
      console.log(r);

      $('#content').html('');
      $.each(r,function(i,v){
        console.log(v);
        var html = load_template('item',{data:v});
        $('#content').append(html);
      });

    });
  }

  $('body').on('brandp_ready',function(){

    display();

  });

});
