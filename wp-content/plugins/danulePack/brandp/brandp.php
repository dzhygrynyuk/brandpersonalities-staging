<?php
//--AJAX / Files
if(is_admin()){
	include 'core.php';
} else {
	// include 'frontend/display.php';
}

function brandp_load_frontend() {
	// wp_enqueue_style( 'slider_style', plugins_url( '/', __FILE__ ) .	'frontend/style.css?v=1.02' );
	// wp_enqueue_script( 'owl_script', plugins_url( '/', __FILE__ ) .	'frontend/owl.carousel.min.js', array('jquery') );
}
add_action( 'wp_enqueue_scripts', 'brandp_load_frontend' );

//---------SETTINGS PAGE

function brandp_styles() {
  if (isset($_GET['page'])) :
  	if($_GET['page']=='brandp'){
  		wp_enqueue_style( 'brandp_style', plugins_url( '/', __FILE__ ) . 'answers/style.css?v=1.01');
  		wp_enqueue_script( 'brandp_script', plugins_url( '/', __FILE__ ) . 'answers/scripts.js?v=1.02', array('jquery') );
  	}
  	if($_GET['page']=='brandp_coupons'){
  		wp_enqueue_style( 'brandp_style', plugins_url( '/', __FILE__ ) . 'coupons/style.css?v=1.01');

  		wp_enqueue_script( 'brandp_ejs', plugins_url( '/', __FILE__ ) . 'coupons/js/ejs.min.js', array('jquery') );
  		wp_enqueue_script( 'brandp_tmp', plugins_url( '/', __FILE__ ) . 'coupons/templates/_main.js', array('jquery') );
  		wp_enqueue_script( 'brandp_script', plugins_url( '/', __FILE__ ) . 'coupons/scripts.js?v=1.02', array('jquery') );
  		wp_localize_script( 'brandp_tmp', 'plugins_url', plugins_url( '/', __FILE__ ) );
  	}
  	if($_GET['page']=='brandp_results'){
  		wp_enqueue_style( 'brandp_style', plugins_url( '/', __FILE__ ) . 'results/style.css?v=1.01');

  		wp_enqueue_script( 'brandp_ejs', plugins_url( '/', __FILE__ ) . 'results/js/ejs.min.js', array('jquery') );
  		wp_enqueue_script( 'brandp_tmp', plugins_url( '/', __FILE__ ) . 'results/templates/_main.js', array('jquery') );
  		wp_enqueue_script( 'brandp_script', plugins_url( '/', __FILE__ ) . 'results/scripts.js?v=1.02', array('jquery') );
  		wp_localize_script( 'brandp_tmp', 'plugins_url', plugins_url( '/', __FILE__ ) );
  	}
  	if($_GET['page']=='brandp_descriptions'){
  		wp_enqueue_style( 'brandp_style', plugins_url( '/', __FILE__ ) . 'descriptions/style.css?v=1.02');
  		wp_enqueue_style( 'selection_style', plugins_url( '/../', __FILE__ ) . 'modules/selection/style.css?v=1.01');

  		wp_enqueue_script( 'brandp_tinymce', plugins_url( '/../', __FILE__ ) . 'modules/tinymce/tinymce.min.js', array('jquery') );
  		wp_enqueue_script( 'brandp_tinymceJ', plugins_url( '/../', __FILE__ ) . 'modules/tinymce/jquery.tinymce.min.js', array('jquery') );
  		wp_enqueue_script( 'brandp_ejs', plugins_url( '/', __FILE__ ) . 'descriptions/js/ejs.min.js', array('jquery') );
  		wp_enqueue_script( 'brandp_tmp', plugins_url( '/', __FILE__ ) . 'descriptions/templates/_main.js', array('jquery') );
  		wp_enqueue_script( 'brandp_sortable', plugins_url( '/', __FILE__ ) . 'descriptions/js/sortable.min.js', array('jquery') );
  		wp_enqueue_script( 'brandp_script', plugins_url( '/', __FILE__ ) . 'descriptions/scripts.js?v=1.03', array('jquery') );
      // wp_enqueue_script( 'selection_js', plugins_url( '/../', __FILE__ ) . 'modules/selection/scripts/core.js?v=1.02', array('jquery') );
      wp_enqueue_script( 'jscolor_js', plugins_url( '/', __FILE__ ) . 'descriptions/js/jscolor.min.js', array('jquery') );
  		wp_localize_script( 'brandp_tmp', 'plugins_url', plugins_url( '/', __FILE__ ) );
  	}
  endif;
}
add_action( 'admin_enqueue_scripts', 'brandp_styles' );

// add the admin options page
function brandp_admin_add_pages() {
	//Main menu item
	add_menu_page('Brand Personalities', 'Brand Personalities', 'manage_options', 'brandp', 'brandp_options_page','dashicons-star-filled');
	add_submenu_page( 'brandp', 'Personality Descriptions', 'Game Result', 'manage_options', 'brandp_descriptions', 'brandp_descriptions_page' );
	add_submenu_page( 'brandp', 'Coupons', 'Coupons', 'manage_options', 'brandp_coupons', 'brandp_coupons_page' );
	add_submenu_page( 'brandp', 'Results', 'Results', 'manage_options', 'brandp_results', 'brandp_results_page' );

}
add_action('admin_menu', 'brandp_admin_add_pages');

//MAIN
function brandp_options_page() {
  global $core;
  wp_enqueue_media();

  include 'answers/display.php';
}

function brandp_descriptions_page() {
  global $core;
  wp_enqueue_media();

	include $core.'modules/selection/display_window.php';
  include 'descriptions/display.php';
}

function brandp_coupons_page() {
  global $core;

  include 'coupons/display.php';
}

function brandp_results_page() {
  global $core;

  include 'results/display.php';
}

?>
