<?php
/*
SELECT qp.question_id as qid, b.brand_id as pid, p.per_name as word
FROM question_personalities_personal qp
LEFT JOIN personalities_personal p
LEFT JOIN brands_personal b ON b.brand_id = p.brand_id
ON qp.personality_id = p.per_id
ORDER BY qid ASC
*/


global $wpdb;

$sql = "SELECT *
FROM bg_personalities
ORDER BY ID ASC";
$wpdb->prepare($sql,array());
$p = $wpdb->get_results($sql,ARRAY_A);

$sql = "SELECT *
FROM $q_table
ORDER BY ID ASC";
$wpdb->prepare($sql,array());
$questions = $wpdb->get_results($sql,ARRAY_A);

$sql = "SELECT a.ID as aid, a.qid, a.pid, a.value as answer, p.value as personality, q.value as question
FROM $a_table a JOIN bg_personalities p ON a.pid = p.ID
JOIN $q_table q ON a.qid = q.ID
ORDER BY q.ID ASC, p.ID ASC";
$wpdb->prepare($sql,array());
$answers = $wpdb->get_results($sql,ARRAY_A);

$matrix = array();
foreach($answers as $a){
  if(!array_key_exists($a['qid'],$matrix)){
    // $matrix[$a['qid']] = array();
  }
  $matrix[$a['qid']][$a['pid']] = $a;
}

// echo '<pre>'.var_export($matrix,true).'</pre>';

?>


<div class="questions-contain">
  <div class="flex-contain">
    <div class="flex-row">
      <div class="flex-cell highlight">
        <input type="text" disabled>
      </div>
    </div>
    <?php foreach($questions as $q){ ?>
      <div class="flex-row">
        <div class="flex-cell highlight">
          <input type="text" data-table="<?php echo $q_table; ?>" data-id="<?php echo $q['ID']; ?>" value="<?php echo $q['value']; ?>">
        </div>
      </div>
    <?php } ?>
  </div>
</div>

<?php
$q = $questions;
?>

<div class="scroll-contain">
  <div class="flex-contain">
    <?php for($i=1; $i<13; $i++){ ?>
      <div class="flex-row">
        <?php for($k=0; $k<13; $k++){
          $class='';
          if($i==0 || $k==0) $class = 'highlight';
          $disable = '';
          if($i==0 && $k==0) $disable = 'disabled';
          $content = '';
          if($k==0 && $i!=0) $content = $p[$i-1]['value'];
          if($k!=0 && $i==0) $content = $q[$k-1]['value'];
          if($i==0) $class.=' wider';
          $num = "($i)($k)";
          if(!empty($content)) $num = '';
          ?>
          <div class="flex-cell <?php echo $class; ?>">
            <?php if($k==0 && $i!=0){ //personality ?>
              <input type="text" data-table="bg_personalities" data-id="<?php echo $p[$i-1]['ID']; ?>" value="<?php echo "$num $content"; ?>" <?php echo $disable; ?>>
            <?php } else { //answer ?>
              <input type="text" data-table="<?php echo $a_table; ?>" data-id="<?php echo $matrix[$k][$i]['aid']; ?>" value="<?php echo $matrix[$k][$i]['answer']; ?>" <?php echo $disable; ?>>
            <?php } ?>
          </div>
        <?php } ?>
      </div>
    <?php } ?>
  </div>
</div>
