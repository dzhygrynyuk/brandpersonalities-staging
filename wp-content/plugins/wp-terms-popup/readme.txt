=== WP Terms Popup ===
Contributors: linksoftware
Tags: terms and conditions, terms of service, privacy policy, age verification, popup
Requires at least: 5.0
Tested up to: 5.5
Requires PHP: 5.6
Stable tag: 2.0.1
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Use WP Terms Popup to ask users to agree to a terms of service, privacy policy or age verification request before they are given to access your site.

== Description ==

= Website Popups for WordPress =

You can use WP Terms Popup to ask users to agree to your terms of service, privacy policy or age verification request before they are allowed to access your website.

= How Does WP Terms Popup Work? =

Our plugin gives your users a simple three step process for gaining access to your website.

**Step #1: Your user must read the popup first.**

You decide what your popup shows to visitors: a terms of service, a privacy policy, age verification, etc.

**Step #2: The user agrees to your conditions.**

Each popup contains two buttons: one to show acceptance and another that redirects away from your site.

**Step #3: Website access is granted to your user.**

When the user accepts your conditions they are immediately taken to your site without any further interaction.

= Getting Started =

After installing and activating WP Terms Popup, go to "Terms Popups" in your WordPress admin menu and select "Add New" to create your first piece of popup content.

Go to "Settings" in the same "Terms Popups" menu to set the global settings that will apply to all popups. You can override these settings by editing each individual piece of popup content. 

From the "Settings" screen you can assign a popup to be shown sitewide or you can assign popups to individual pieces of content using the standard post editing screen.

== Frequently Asked Questions ==

= How can I change the look of the popup? =

If you have knowledge of CSS you can style the popups however you want by changing your theme's CSS file. For a simpler and code free option, you can purchase and install our [WP Terms Popup Designer](https://termsplugin.com/designer) add-on.

= How can I collect information about visitors who agree to my popups? =

We offer a premium add-on for this purpose called [WP Terms Popup Collector](https://termsplugin.com/collector) which offers you conversion statistics and collects details about each visitor to your site when they agree to one of your popups.

= How do I use a single popup for my entire site? =

After you've created your popup content, go to the "Settings" page and select the checkbox for "Enable only one popup sitewide?" The select your "Sitewide Terms Popup" from the dropdown. Set an "Agreement Expiration" time if the default isn't right for you and once you hit the "Save Settings" button you are all done.

= Can I have different popups be shown on different pages and posts? =

1. Create as many pieces of popup content as you want.

2. Go to the edit screen of the post or page you want to show a popup on.

3. On the right hand side of the edit screen, you will see the "WP Terms Popup" option box. Configure according to your needs.

4. Save the post or page and you are all set.

= How do I disable a sitewide popup from appearing on a page or post? =

1. Go to the edit screen of the post or page you want to disable the sitewide popup on.

2. On the right hand side of the edit screen, you will see the "WP Terms Popup" option box. Check the "Disable Popup?" box.

3. Save the post or page and your sitewide popup will no longer appear.

= How do I show a popup on a custom post type? =

Simply find the shortcode for the popup content you want. You can find this information under "All Popups" in the admin menu. Copy the shortcode that matches the popup content you want and the paste it into the content of your custom post type.

= Is the popup responsive and viewable on mobile devices? =

Yes, the popup is responsive and will resize according to a device's browser dimensions.

= Will my visitors be able to see the popup if they have Javascript disabled on their browser? =

Yes, on the condition that you are not using the "Load popup content after page load?" option available in the settings for WP Terms Popup. That setting uses Javascript to load popup content and to help deal with caching plugins and solutions.

== Screenshots ==

1. WP Terms Popup Example
2. Create and Manage Multiple Pieces of Popup Content
3. WP Terms Popup Settings
4. Enable a Popup on an Individual Post/Page
5. Disable a Popup on an Individual Post/Page

== Changelog ==

= 2.0.1 =
* Bug fixes.

= 2.0.0 =
* Refactored code base.
* WordPress 5.5 compatibility changes.
* New "User Visibility" setting.