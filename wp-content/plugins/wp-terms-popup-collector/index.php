<?php
/*
Plugin Name: WP Terms Popup Collector
Plugin URI: https://termsplugin.com
Description: Gather data about your WP Terms Popups with Collector.
Version: 1.1.2
Author: Link Software LLC
Author URI: https://linksoftwarellc.com
*/

define('WP_TERMS_POPUP_COLLECTOR_NAME', 'wp-terms-popup-collector');
define('WPTPC_VERSION', '1.1.2');
define('WPTPC_FILE', __FILE__);
define('WPTPC_STORE_URL', 'https://termsplugin.com');
define('WPTPC_STORE_PRODUCT', 'WP Terms Popup Collector');
define('WPTPC_STORE_PRODUCT_ID', 1208);

require_once 'inc/wptp-collector-licensing.php';
require_once 'inc/wptp-collector-activation.php';
require_once 'inc/wptp-collector-settings.php';
require_once 'inc/wptp-collector-columns.php';
require_once 'inc/wptp-collector-functions.php';

register_activation_hook(__FILE__, 'wptp_collector_activation');

function wptp_collector_activation_warning() {
    ?>
    <div class="notice notice-error is-dismissible">
        <p>WP Terms Popup Collector requires the <a href="https://wordpress.org/plugins/wp-terms-popup/">latest free version of WP Terms Popup</a> to be installed and activated. You must have both plugins installed and active at the same time for it to work properly!</p>
    </div>
    <?php
}

function wptp_collector_check_free() {
    if (! function_exists('is_plugin_inactive')) {
        require_once ABSPATH.'/wp-admin/includes/plugin.php';
    }

    if (is_plugin_inactive('wp-terms-popup/index.php') && is_plugin_inactive('wp-terms-popup/wp-terms-popup.php')) {
        add_action('admin_notices', 'wptp_collector_activation_warning');

        return;
    }
}
add_action('plugins_loaded', 'wptp_collector_check_free');

function wptp_collector_scripts() {
    wp_register_script('wptp-collector-datatables', plugins_url('wp-terms-popup-collector/assets/js/datatables.min.js'), ['jquery'], null, false);
    wp_enqueue_script('wptp-collector-datatables');

    wp_register_script('wptp-collector-datatables-fixedColumns', plugins_url('wp-terms-popup-collector/assets/js/datatables.fixedColumns.min.js'), ['jquery'], null, false);
    wp_enqueue_script('wptp-collector-datatables-fixedColumns');

    wp_register_script('wptp-collector', plugins_url('wp-terms-popup-collector/assets/js/wptp-collector.js'), ['jquery'], null, false);
    wp_enqueue_script('wptp-collector');

    wp_register_style('wptp-collector-css-datatables', plugins_url('wp-terms-popup-collector/assets/css/datatables.min.css'));
    wp_enqueue_style('wptp-collector-css-datatables');

    wp_register_style('wptp-collector-css-datatables-fixedColumns', plugins_url('wp-terms-popup-collector/assets/css/dataTables.fixedColumns.min.css'));
    wp_enqueue_style('wptp-collector-css-datatables-fixedColumns');

    wp_register_style('wptp-collector-css', plugins_url('wp-terms-popup-collector/assets/css/wptp-collector.css'));
    wp_enqueue_style('wptp-collector-css');
}
add_action('admin_enqueue_scripts', 'wptp_collector_scripts', 1);
?>