(function($) {
	
	'use strict';

	jQuery(document).ready(function($) {

        $.fn.dataTable.ext.classes.sPageButton = 'button-secondary';
        
        $('#wptp-collector-results').DataTable({

            "lengthMenu": [[25, 50, 1000, -1], [25, 50, 100, "All"]],
            "order": [[ 1, "desc" ]],
            scrollX: true,
            "aoColumns": [
                { "bSortable": false },
                null,
                null,
                null,
                null,
                null,
                null
              ],
            "fixedColumns": { leftColumns: 3 },
            dom: 'Blfrtip',
            buttons: [
                {
                    extend: 'copyHtml5',
                    text: 'Copy to Clipboard',
                    className: 'button-secondary',
                    exportOptions: {

                        columns: [ 1, 2, 3, 4, 5, 6 ]

                    }
                },

                {
                    extend: 'csvHtml5', 
                    text: 'Export to CSV', 
                    className: 'button-secondary', 
                    title: $('#wptp-collector-filename').val(),
                    exportOptions: {

                        columns: [ 1, 2, 3, 4, 5, 6 ]

                    }
                },
            ]

        });

    });

})(jQuery);