<?php function wptp_collector_settings() { ?>

<?php
    if ((isset($_GET['popup_id']) && is_numeric($_GET['popup_id'])) && (isset($_GET['result_id']) && is_numeric($_GET['result_id']))) { 
    
        if (wptp_collector_delete_result($_GET['popup_id'], $_GET['result_id'])) {

            echo '<div class="notice notice-success"><p>'.__('The result was deleted successfully.', WP_TERMS_POPUP_COLLECTOR_NAME).'</p></div>';

        } else {

            echo '<div class="notice notice-error"><p>'.__('The result failed to delete.', WP_TERMS_POPUP_COLLECTOR_NAME).'</p></div>';

        }
    
    }
?>

<?php $wptpc_license_status = get_option('wptpc_license_status'); ?>
<div class="wrap wptp-wrap" style="margin-top:30px;">
    <?php if (!isset($_GET['popup_id'])) : ?>
	<div class="postbox-container" style="float:right; width:30%;">
		<div class="meta-box-sortables">
			<div class="postbox">
				<div class="inside">
					<h3><?php _e('WP Terms Popup Collector License', WP_TERMS_POPUP_COLLECTOR_NAME); ?></h3>
					<p><?php printf( __( 'If you are having problems activating your license key please contact WP Terms Popup <a href="%s">support</a>.', WP_TERMS_POPUP_COLLECTOR_NAME ), esc_url( 'https://termsplugin.com/support' ) ); ?></p>
			    
					<form method="post" action="options.php">
						<?php wp_nonce_field( 'wptpc_license_nonce', 'wptpc_license_nonce' ); ?>
						
						<table class="form-table">
							<tbody>
								<tr>
									<th><?php _e('License Key', WP_TERMS_POPUP_COLLECTOR_NAME); ?></th>
								</tr>
								
								<tr>
									<td style="padding:0;">
										<input style="width:100%;" id="wptpc_license_key" name="wptpc_license_key" type="text" value="<?php echo get_option('wptpc_license_key'); ?>" />
									</td>
								</tr>
							</tbody>
						</table>
										
						<?php if ('valid' == ($wptpc_license_status = get_option('wptpc_license_status'))) : ?>
						
						<?php submit_button(__('Deactivate License Key', WP_TERMS_POPUP_COLLECTOR_NAME), 'primary', 'wptpc_license_deactivate'); ?>
						
						<?php else : ?>
						
						<?php submit_button(__('Activate License Key', WP_TERMS_POPUP_COLLECTOR_NAME), 'primary', 'wptpc_license_activate'); ?>
						
						<?php endif; ?>
					</form>
				</div>
			</div>
		</div>
	</div>
    <?php endif; ?>
	
    <?php if (get_option('wptpc_license_activated') == true) : ?>
    
    <?php
        $args_popups = array (
            
            'post_type' => 'termpopup',
            'post_status' => 'publish',
            'orderby' => 'title',
            'order' => 'ASC',
            'posts_per_page'=> -1
            
        );
							
		$popups = new WP_Query( $args_popups );
	?>

    <?php if (isset($_GET['popup_id']) && is_numeric($_GET['popup_id'])) : ?>

        <h3 class="wptp-title"><?php echo get_the_title($_GET['popup_id']); ?> <?php _e('Results', WP_TERMS_POPUP_COLLECTOR_NAME); ?></h3>

        <h4 class="wptp-title">
            <?php
                $views = wptp_collector_results($_GET['popup_id'], 'views');
                $conversions = wptp_collector_results($_GET['popup_id'], 'conversions');
            ?>
            <?php _e('Views', WP_TERMS_POPUP_COLLECTOR_NAME); ?>: <?php echo number_format($views); ?>, 
            <?php _e('Conversions', WP_TERMS_POPUP_COLLECTOR_NAME); ?>: <?php echo number_format($conversions).''.($views > 0 ? ' ('.number_format(($conversions / $views) * 100).'%)' : ''); ?>
        </h4>

        <?php if ($logs = wptp_collector_logs($_GET['popup_id'])) : ?>

        <input type="hidden" id="wptp-collector-filename" value="<?php echo get_the_title($_GET['popup_id']); ?> <?php _e('Results', WP_TERMS_POPUP_COLLECTOR_NAME); ?>">

        <table id="wptp-collector-results" class="widefat stripe row-border order-column nowrap">
            <thead>
                <tr>
                    <th></th>
                    <th><?php _e('Date', WP_TERMS_POPUP_COLLECTOR_NAME); ?></th>
                    <th><?php _e('IP Address', WP_TERMS_POPUP_COLLECTOR_NAME); ?></th>
                    <th><?php _e('User Agent', WP_TERMS_POPUP_COLLECTOR_NAME); ?></th>
                    <th><?php _e('Source', WP_TERMS_POPUP_COLLECTOR_NAME); ?></th>
                    <th><?php _e('User', WP_TERMS_POPUP_COLLECTOR_NAME); ?></th>
                    <th><?php _e('Expiration', WP_TERMS_POPUP_COLLECTOR_NAME); ?></th>
                </tr>
            </thead>

            <tbody>
                <?php foreach ($logs as $log) : ?>
                <?php $url = admin_url('edit.php?post_type=termpopup&page=terms-popup-plugin-collector&popup_id='.$_GET['popup_id'].'&result_id='.$log->id); ?>
                <tr>
                    <td><a href="<?php echo $url; ?>"><span class="dashicons dashicons-trash"></span></a></td>
                    <td data-sort="<?php echo strtotime($log->timestamp); ?>"><?php echo date(get_option('date_format').' '.get_option('time_format'), strtotime($log->timestamp)); ?></td>
                    <td><?php echo $log->ip_address; ?></td>
                    <td><?php echo $log->user_agent; ?></td>
                    <td><a target="_blank" href="<?php echo get_site_url(null, $log->source); ?>"><?php echo $log->source; ?></a></td>
                    <td>
                        <?php if ($log->user_id != 0) : ?>
                        <a target="_blank" href="<?php echo get_edit_user_link($log->user_id); ?>"><?php echo $log->display_name; ?> (<?php echo $log->user_login; ?>)</td>
                        <?php else : ?>
                        <?php _e('N/A', WP_TERMS_POPUP_COLLECTOR_NAME); ?>
                        <?php endif; ?>
                    </td>
                    <td data-sort="<?php echo strtotime($log->agreement_expiration); ?>"><?php echo date(get_option('date_format').' '.get_option('time_format'), strtotime($log->agreement_expiration)); ?></td>
                </tr>
                <?php endforeach; ?>
            </tbody>

            <tfoot>
                <tr>
                    <th></th>
                    <th><?php _e('Date', WP_TERMS_POPUP_COLLECTOR_NAME); ?></th>
                    <th><?php _e('IP Address', WP_TERMS_POPUP_COLLECTOR_NAME); ?></th>
                    <th><?php _e('User Agent', WP_TERMS_POPUP_COLLECTOR_NAME); ?></th>
                    <th><?php _e('Source', WP_TERMS_POPUP_COLLECTOR_NAME); ?></th>
                    <th><?php _e('User', WP_TERMS_POPUP_COLLECTOR_NAME); ?></th>
                    <th><?php _e('Expiration', WP_TERMS_POPUP_COLLECTOR_NAME); ?></th>
                </tr>
            </tfoot>
        </table>

        <?php else : ?>

        <p><?php _e('There is no conversion data for this popup.', WP_TERMS_POPUP_COLLECTOR_NAME); ?></p>

        <?php endif; ?>

    <?php else : ?>

    <?php if ($popups->have_posts()) : ?>
    
    <h3 class="wptp-title"><?php _e('Popups', WP_TERMS_POPUP_COLLECTOR_NAME); ?></h3>

    <div style="float: left; width:60%;">
        <table class="widefat">
            <thead>
                <tr>
                    <th><?php _e('Name', WP_TERMS_POPUP_COLLECTOR_NAME); ?></<th>
                    <th><?php _e('Views', WP_TERMS_POPUP_COLLECTOR_NAME); ?></<th>
                    <th><?php _e('Conversions', WP_TERMS_POPUP_COLLECTOR_NAME); ?></th>
                    <th>&nbsp;</th>
                </tr>
            </thead>

            <tbody>
                <?php $row_counter = 0; ?>
                <?php while ( $popups->have_posts() ) : $row_counter++; ?>
                <?php
                    $popups->the_post();
                    $url = admin_url('edit.php?post_type=termpopup&page=terms-popup-plugin-collector&popup_id='.get_the_ID());
                    $views = wptp_collector_results(get_the_ID(), 'views');
                    $conversions = wptp_collector_results(get_the_ID(), 'conversions');
                ?>
                <tr <?php if ($row_counter % 2) { echo 'class="alternate"'; } ?>>
                    <td><a href="<?php echo $url; ?>"><?php the_title(); ?></a></td>
                    <td><a href="<?php echo $url; ?>"><?php echo number_format($views); ?></a></td>
                    <td><a href="<?php echo $url; ?>"><?php echo number_format($conversions).''.($views > 0 ? ' ('.number_format(($conversions / $views) * 100).'%)' : ''); ?></a></td>
                    <td><a href="<?php echo $url; ?>"><?php _e('View Results', WP_TERMS_POPUP_COLLECTOR_NAME); ?></a></td>
                </tr>
                <?php endwhile; ?>
            </tbody>
        </table>
    </div>

    <?php else : ?>

    <?php printf( __( 'Please <a href="%s">create your first Terms Popup</a> to proceed.', WP_TERMS_POPUP_COLLECTOR_NAME ), esc_url( 'post-new.php?post_type=termpopup' ) ); ?>					

    <?php endif; ?>

    <?php endif; ?>

	<?php else : ?>

	<p><?php _e('Enter and activate your WP Terms Popup Collector License Key using the form to the right.', WP_TERMS_POPUP_COLLECTOR_NAME); ?></p>

	<?php endif; ?>
</div>

<?php } ?>