<?php
    function wptp_collector_update_logs($popup_id)
    {
        global $wpdb;

        $table_name_logs = $wpdb->prefix.'wptp_collector_logs';
        $timestamp = current_time('mysql');

        // Find Agreement Expiration
        if (get_option('termsopt_expiry') && get_option('termsopt_expiry') != '' && get_option('termsopt_expiry') > 0) {
            $agreement_expiration = date('Y-m-d H:i:s', strtotime($timestamp) + (get_option('termsopt_expiry')) * 60 * 60);
        } else {
            if (get_option('termsopt_expiry') != '' && get_option('termsopt_expiry') == 0) {
                $agreement_expiration = $timestamp;
            } else {
                $agreement_expiration = date('Y-m-d H:i:s', strtotime($timestamp) + 3 * 24 * 60 * 60);
            }
        }

        // Find Agree Button Text
        if ((get_post_meta($popup_id, 'terms_agreetxt', true)) != '') {
            $agreement_text = get_post_meta($popup_id, 'terms_agreetxt', true);
        } elseif (get_option('termsopt_agreetxt') != '') {
            $agreement_text = get_option('termsopt_agreetxt');
        } else {
            $agreement_text = __('I Agree', WP_TERMS_POPUP_NAME);
        }

        // Determine IP Address, using Pippin's Example: https://gist.github.com/pippinsplugins/9641841
        $ip_address = 'Unknown';
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            //check ip from share internet
            $ip_address = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            //to check ip is pass from proxy
            $ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip_address = $_SERVER['REMOTE_ADDR'];
        }

        // Insert data into logs table
        $sql_logs_data = "INSERT INTO `$table_name_logs` (`popup_id`, `user_id`, `ip_address`, `user_agent`, `source`, `agreement_expiration`, `agreement_text`, `timestamp`) 
            VALUES (%d, %d, %s, %s, %s, %s, %s, %s)";

        $logs_data = $wpdb->prepare(
            $sql_logs_data,
            $popup_id,
            get_current_user_id(),
            $ip_address,
            $_SERVER['HTTP_USER_AGENT'],
            $_SERVER['REQUEST_URI'],
            $agreement_expiration,
            $agreement_text,
            $timestamp
        );

        $wpdb->query($logs_data);

        wptp_collector_update_results($popup_id, 'conversions');
    }

    function wptp_collector_update_results($popup_id, $increment = 'views')
    {
        global $wpdb;

        $table_name_results = $wpdb->prefix.'wptp_collector_results';

        // Update data in results table; insert row if none exists already for given popup ID
        $result = $wpdb->get_row($wpdb->prepare("SELECT popup_id FROM $table_name_results WHERE popup_id = %d", $popup_id));
        if (is_null($result)) {
            $sql_results_data = "INSERT INTO `$table_name_results` (`popup_id`, `views`) 
            VALUES (%d, %d)";

            $results_data = $wpdb->prepare(
                $sql_results_data,
                $popup_id,
                '1'
            );

            $wpdb->query($results_data);
        } else {
            $wpdb->query($wpdb->prepare("UPDATE `$table_name_results` SET `$increment` = `$increment` + 1 WHERE popup_id = %d", $popup_id));
        }
    }

    function wptp_collector_logs($popup_id)
    {
        global $wpdb;

        $table_name_users = $wpdb->prefix.'users';
        $table_name_logs = $wpdb->prefix.'wptp_collector_logs';

        $results = [];

        $arr_results = $wpdb->get_results(
            $wpdb->prepare(
                "
                SELECT `$table_name_logs`.*, `$table_name_users`.`display_name`, `$table_name_users`.`user_login` 
                FROM `$table_name_logs` 
                LEFT JOIN `$table_name_users` ON `$table_name_users`.`id` = `$table_name_logs`.`user_id`
                WHERE popup_id = %d 
                ORDER BY `timestamp` DESC
                ",
                $popup_id
            )
        );

        if ($arr_results) {
            $results = $arr_results;
        }

        return $results;
    }

    function wptp_collector_results($popup_id, $column)
    {
        global $wpdb;

        $table_name_results = $wpdb->prefix.'wptp_collector_results';

        $results = 0;

        $arr_results = $wpdb->get_col($wpdb->prepare("SELECT $column FROM `$table_name_results` WHERE popup_id = %d", $popup_id));
        if ($arr_results) {
            $results = 0 + $arr_results[0];
        }

        return $results;
    }

    function wptp_collector_delete_result($popup_id, $result_id)
    {
        global $wpdb;

        $table_name_logs = $wpdb->prefix.'wptp_collector_logs';
        $table_name_results = $wpdb->prefix.'wptp_collector_results';

        if ($wpdb->query($wpdb->prepare("DELETE FROM `$table_name_logs` WHERE id = %d", $result_id))) {
            $wpdb->query($wpdb->prepare("UPDATE `$table_name_results` SET `views` = `views` - 1 WHERE popup_id = %d", $popup_id));
            $wpdb->query($wpdb->prepare("UPDATE `$table_name_results` SET `conversions` = `conversions` - 1 WHERE popup_id = %d", $popup_id));

            return true;
        } else {
            return false;
        }
    }
