<?php
    function wptp_collector_activation() {
        
    require_once(ABSPATH.'wp-admin/includes/upgrade.php');

    global $wpdb;

    $charset_collate = $wpdb->get_charset_collate();
    $table_name_logs = $wpdb->prefix.'wptp_collector_logs';
    $table_name_results = $wpdb->prefix.'wptp_collector_results';

    // Create table for logs
    $sql_logs = "CREATE TABLE $table_name_logs (
        `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
        `popup_id` bigint(20) unsigned DEFAULT NULL,
        `user_id` bigint(20) unsigned DEFAULT NULL,
        `ip_address` varchar(39) NOT NULL,
        `user_agent` varchar(255) NOT NULL DEFAULT '',
        `source` varchar(255) NOT NULL DEFAULT '',
        `agreement_expiration` datetime DEFAULT NULL,
        `agreement_text` varchar(255) NOT NULL DEFAULT '',
        `timestamp` datetime DEFAULT NULL,
        PRIMARY KEY (`id`)
    ) $charset_collate;";
    dbDelta($sql_logs);

    // Create table for results
    $sql_results = "CREATE TABLE $table_name_results (
        `popup_id` bigint(20) unsigned DEFAULT NULL,
        `views` bigint(20) unsigned DEFAULT '0',
        `conversions` bigint(20) unsigned DEFAULT '0',
        `declines` bigint(20) unsigned DEFAULT '0'
    ) $charset_collate;";
    dbDelta($sql_results);

}