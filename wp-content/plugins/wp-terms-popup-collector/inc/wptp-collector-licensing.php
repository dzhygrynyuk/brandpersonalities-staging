<?php
if (! class_exists('WPTPC_Plugin_Updater')) {
    include dirname(__FILE__).'/WPTPC_Plugin_Updater.php';
}

function wptpc_settings() {
    // License Key
    register_setting('wptpc_license', 'wptpc_license_key');

    if (get_option('wptpc_license_status') == 'valid') {
        update_option('wptpc_license_activated', true);
    }
}
add_action('admin_init', 'wptpc_settings');

function wptpc_sanitize_license($new) {
    $old = get_option('wptpc_license_key');

    if ($old && $old != $new) {
        delete_option('wptpc_license_status');
    }

    return $new;
}

function wptpc_plugin_check() {
    $wpwcp_installed_version = get_option('WPTPC_VERSION');

    if ($wpwcp_installed_version != WPTPC_VERSION) {
        update_option('WPTPC_VERSION', WPTPC_VERSION);
    }

    if (false === ($wpwcp_license_status = get_transient('wptpc_license_status'))) {
        $license_status = wptpc_license_check();

        if ($license_status != false) {
            set_transient('wptpc_license_status', $license_status, 1 * DAY_IN_SECONDS);
            update_option('wptpc_license_status', $license_status);
        }
    }
}
add_action('plugins_loaded', 'wptpc_plugin_check');

function wptpc_plugin_updater() {
    $license_key = trim(get_option('wptpc_license_key'));

    $edd_updater = new WPTPC_Plugin_Updater(
        WPTPC_STORE_URL,
        WPTPC_FILE,
        [
            'version' => WPTPC_VERSION,
            'license' => $license_key,
            'item_id' => WPTPC_STORE_PRODUCT_ID,
            'author' => 'Link Software LLC',
        ]
    );
}
add_action('admin_init', 'wptpc_plugin_updater');

function wptpc_license_activate() {
    if (isset($_POST['wptpc_license_activate']) || isset($_POST['wptpc_license_deactivate'])) {
        if (! check_admin_referer('wptpc_license_nonce', 'wptpc_license_nonce')) {
            return;
        } else {
            $wptpc_license_key = sanitize_text_field($_POST['wptpc_license_key']);
            update_option('wptpc_license_key', $wptpc_license_key);
        }

        if (isset($_POST['wptpc_license_activate'])) {
            $edd_action = 'activate_license';
        } elseif (isset($_POST['wptpc_license_deactivate'])) {
            $edd_action = 'deactivate_license';
        }

        // retrieve the license from the database
        $license = trim(get_option('wptpc_license_key'));

        // data to send in our API request
        $api_params = [
            'edd_action' => $edd_action,
            'license' => $license,
            'item_id' => urlencode(WPTPC_STORE_PRODUCT_ID),
            'url' => home_url(),
        ];

        // Call the custom API.
        $response = wp_remote_post(WPTPC_STORE_URL, ['timeout' => 15, 'sslverify' => false, 'body' => $api_params]);

        // make sure the response came back okay
        if (is_wp_error($response) || 200 !== wp_remote_retrieve_response_code($response)) {
            $get_error_message = $response->get_error_message();
            $message = (is_wp_error($response) && ! empty($get_error_message)) ? $response->get_error_message() : __('An error occurred, please try again.', WP_TERMS_POPUP_COLLECTOR_NAME);
        } else {
            $license_data = json_decode(wp_remote_retrieve_body($response));

            if (false === $license_data->success) {
                switch ($license_data->error) {
                    case 'expired':
                        $message = sprintf(
                            __('Your license key expired on %s.', WP_TERMS_POPUP_COLLECTOR_NAME),
                            date_i18n(get_option('date_format'), strtotime($license_data->expires, current_time('timestamp')))
                        );
                        break;

                    case 'revoked':
                        $message = __('Your license key has been disabled.', WP_TERMS_POPUP_COLLECTOR_NAME);
                        break;

                    case 'missing':
                        $message = __('Invalid license.', WP_TERMS_POPUP_COLLECTOR_NAME);
                        break;

                    case 'invalid':

                    case 'site_inactive':
                        $message = __('Your license is not active for this URL.', WP_TERMS_POPUP_COLLECTOR_NAME);
                        break;

                    case 'item_name_mismatch':
                        $message = sprintf(__('This appears to be an invalid license key for %s.'), WPTPC_STORE_PRODUCT, WP_TERMS_POPUP_COLLECTOR_NAME);
                        break;

                    case 'no_activations_left':
                        $message = __('Your license key has reached its activation limit.', WP_TERMS_POPUP_COLLECTOR_NAME);
                        break;

                    default:
                        $message = __('An error occurred, please try again.', WP_TERMS_POPUP_COLLECTOR_NAME);
                        break;
                }
            }
        }

        // Check if anything passed on a message constituting a failure
        if (! empty($message)) {
            update_option('wptpc_license_status', $license_data->error);
            set_transient('wptpc_license_status', 'invalid', 1 * DAY_IN_SECONDS);

            $base_url = admin_url('edit.php?post_type=termpopup&page=terms-popup-plugin-collector');
            $redirect = add_query_arg(['sl_activation' => 'false', 'message' => urlencode($message)], $base_url);

            wp_redirect($redirect);

            exit();
        }

        if (empty($message)) {
            if ($edd_action == 'activate_license') {
                update_option('wptpc_license_activated', true);
                update_option('wptpc_license_status', $license_data->license);
                set_transient('wptpc_license_status', 'valid', 1 * DAY_IN_SECONDS);

                $base_url = admin_url('edit.php?post_type=termpopup&page=terms-popup-plugin-collector');
                $redirect = add_query_arg(['sl_activation' => 'true', 'message' => urlencode(__('Your license key was successfully activated.'))], $base_url);
            } elseif ($edd_action == 'deactivate_license') {
                update_option('wptpc_license_activated', false);
                update_option('wptpc_license_status', $license_data->license);
                set_transient('wptpc_license_status', 'invalid', 1 * DAY_IN_SECONDS);

                $base_url = admin_url('edit.php?post_type=termpopup&page=terms-popup-plugin-collector');
                $redirect = add_query_arg(['sl_activation' => 'true', 'message' => urlencode(__('Your license key was successfully deactivated.'))], $base_url);
            }

            wp_redirect($redirect);

            exit();
        }
    }
}
add_action('admin_init', 'wptpc_license_activate');

function wptpc_license_check() {
    $store_url = WPTPC_STORE_URL;
    $item_id = WPTPC_STORE_PRODUCT_ID;
    $license = get_option('wptpc_license_key');

    $api_params = [
        'edd_action' => 'check_license',
        'license' => $license,
        'item_id' => urlencode($item_id),
        'url' => home_url(),
    ];

    $response = wp_remote_post($store_url, ['body' => $api_params, 'timeout' => 15, 'sslverify' => false]);

    if (is_wp_error($response)) {
        return false;
    }

    $license_data = json_decode(wp_remote_retrieve_body($response));

    return $license_data->license;
}

function wptp_collector_license_notification() {
    if (current_user_can('administrator') && isset($_GET['post_type']) && $_GET['post_type'] == 'termpopup') {
        if (isset($_GET['page']) && $_GET['page'] == 'terms-popup-plugin-collector' && isset($_GET['sl_activation']) && isset($_GET['message'])) {
            $notification_message = sanitize_text_field($_GET['message']);
            $notification_type = ($_GET['sl_activation'] == 'false' ? 'notice-error' : 'notice-success');
        } else {
            if (get_option('wptpc_license_status') == 'invalid' || get_option('wptpc_license_status') == 'deactivated') {
                $settings_url = admin_url('edit.php?post_type=termpopup&page=terms-popup-plugin-collector');
                $notification_message = sprintf(wp_kses(__('Please <a href="%s">activate a valid WP Terms Popup Collector license key</a> to guarantee you receive updates and support.', WP_TERMS_POPUP_COLLECTOR_NAME), ['a' => ['href' => []]]), esc_url($settings_url));
                $notification_type = 'notice-warning';
            } elseif (get_option('wptpc_license_status') == 'expired') {
                $settings_url = admin_url('edit.php?post_type=termpopup&page=terms-popup-plugin-collector');
                $notification_message = sprintf(wp_kses(__('Your WP Terms Popup Collector license has expired. <a target="_blank" href="%s">Renew your subscription</a> to continue to receive updates and support.', WP_TERMS_POPUP_COLLECTOR_NAME), ['a' => ['target' => [], 'href' => []]]), esc_url('https://termsplugin.com/renew'));
                $notification_type = 'notice-error';
            }
        }

        if (isset($notification_message)) {
            echo '<div class="notice '.$notification_type.'">';
            echo '<p>'.$notification_message.'</p>';
            echo '</div>';
        }

        unset($notification_message);
    }
}
add_action('admin_notices', 'wptp_collector_license_notification');
