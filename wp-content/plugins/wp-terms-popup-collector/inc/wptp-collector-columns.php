<?php
	function wptp_collector_columns_termpopup($columns) {

        $columns['views'] = __('Views', WP_TERMS_POPUP_COLLECTOR_NAME);
        $columns['conversions'] = __('Conversions', WP_TERMS_POPUP_COLLECTOR_NAME);
		
		return $columns;
		
    }
    
	function wptp_collector_columns_termpopup_content($column_name, $post_id) {

        $views = 0;
        $conversions = 0;

        if ($column_name == 'views' || $column_name == 'conversions') {

            $views = wptp_collector_results($post_id, 'views');
            $conversions = wptp_collector_results($post_id, 'conversions');

        }
		
		switch ($column_name) {
			
            case 'views' :

				echo number_format($views);
				
				break;
			
            case 'conversions' :
            
                $url = admin_url('edit.php?post_type=termpopup&page=terms-popup-plugin-collector&popup_id='.$post_id);
				echo '<a href="'.$url.'">'.number_format($conversions).''.($views > 0 ? ' ('.number_format(($conversions / $views) * 100).'%)' : '').'</a>';
                
                break;
				
			default :
			
				break;
			
		}
		
	}

    add_filter('manage_edit-termpopup_columns', 'wptp_collector_columns_termpopup', 999);
    add_action('manage_termpopup_posts_custom_column', 'wptp_collector_columns_termpopup_content', 10, 2);