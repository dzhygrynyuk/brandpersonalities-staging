=== WP Terms Popup Collector ===
Contributors: linksoftware
Tags: terms and conditions, privacy policy, terms of service, age verification, popup, age verify, conditions, policy, privacy, terms, agree
Requires at least: 5.0
Tested up to: 5.5
Stable tag: 1.1.2

Collect information about your website's visitors after they agree to you popups.

== Description ==

WP Terms Popup Collector gathers information about your website's visitors after they agree to the popups created by the WP Terms Popup plugin.

== Installation ==

1. Upload the plugin files to the `/wp-content/plugins/wp-terms-popup-collector` directory, or install the plugin through the WordPress plugins screen directly.
2. Activate the plugin through the 'Plugins' screen in WordPress
3. Use the "Collector" screen to access WP Terms Popup Collector's features.

== Changelog ==

= 1.1.2 =
* WordPress 5.0 compatability changes.
* WP Terms Popup 2.0.0 compatability changes.

= 1.1.1 =
* Improvements to the License Key activation process.

= 1.1.0 =
* Bug fix for storing IP address on web hosts using proxy setups.

= 1.0.0 =
* Initial release.