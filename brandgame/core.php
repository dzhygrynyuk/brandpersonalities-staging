<?php

//use wordpress functions
require_once('../wp-load.php');

include_once 'config.php';
include_once 'db.php';

require_once 'dompdf/autoload.inc.php';
use Dompdf\Dompdf;
use Dompdf\Options;

if(isset($_POST['action']) && !empty($_POST['action'])) {
  $action = $_POST['action'];
  switch($action) {
    case 'getQuestions' : get_questions(); break;
    case 'getSelections' : get_selections(); break;
    case 'getPersonality' : get_personality(); break;
    case 'submitPayment' : submit_payment(); break;
    case 'checkValid' : check_valid(); break;
    case 'checkCoupon' : check_coupon(); break;
    case 'getPaymentData' : get_payment_data(); break;
  }
}

function generate_pdf($data){
  $data = $data;
  //send e-mail
  ob_start();
  include 'pdf-template.php';
  $pdf_html = ob_get_clean();

  // instantiate and use the dompdf class
  $options = new Options();
  $options->setIsRemoteEnabled(true);
  
  $dompdf = new Dompdf($options);
  $dompdf->loadHtml($pdf_html);
  $dompdf->setPaper('A4', 'portrait');
  $dompdf->render();

  $output = $dompdf->output();
  $pdf_success = file_put_contents('pdf/invoice_'.$data['token'].'.pdf', $output);

  global $siteurl;

  if($pdf_success !== false){
    return $siteurl.'/pdf/invoice_'.$data['token'].'.pdf';
  }else{
    return false;
  }
}

function get_payment_data(){
  $mode = $_POST['mode'];
  $str = 'wrd-'.$mode.'-price';
  $price = get_option($str);

  $price = number_format((float)$price, 2, '.', '');
  $price_dsp = '$'.$price;

  echo json_encode(array('price'=>$price,'price_dsp'=>$price_dsp));
  die();
}

function check_coupon(){
  $mode = $_POST['mode'];
  $coupon = $_POST['coupon'];
  $r = check_coupon_fn($mode,$coupon);

  echo json_encode($r);
  die();
}

function check_coupon_fn($mode,$coupon){
  global $db;

  $str = 'wrd-'.$mode.'-price';
  $price = get_option($str);
  $price = number_format((float)$price, 2, '.', '');
  $price_dsp = '$'.$price;

  $sql = "SELECT * FROM bg_coupons WHERE code = ?";
  $r = $db->select_row($sql,array($coupon));

  $discount = floatval($r['amount']);
  if($r['type']=='percent'){
    $_discount = $discount / 100;
    $new_price = $price - ($price * $_discount);
  } else if($r['type']=='dollars') {
    $new_price = $price - $discount;
  }

  if($new_price < 0) $new_price = 0;

  $new_price = number_format((float)$new_price, 2, '.', '');
  $new_price_dsp = '$'.$new_price;

  if(!empty($r)){
    return array('status'=>'success','price_original'=>$price,'price'=>$new_price,'price_dsp'=>$new_price_dsp, 'discount'=>$discount, 'discount_type'=>$r['type']);
  } else {
    return array('status'=>'fail','price'=>$price,'price_dsp'=>$price_dsp, 'price_original'=>$price);
  }
}

function check_valid(){
  $token = $_POST['token'];
  $result = check_valid_fn($token);
  echo json_encode($result);

  die();
}

function check_valid_fn($token = null){
  global $db;
  $return = array();

  $sql = "SELECT * FROM bg_users WHERE token = ?";
  $result = $db->select_row($sql,array($token));

  if(empty($result)){
    //not valid
    $return['valid'] = false;
    // $return['msg'] = 'No token provided';
  } else {
    //not empty
    $return['data'] = $result;
    if($result['plays_avail'] == 0) {
      //token invalid
      $return['valid'] = false;
      $return['msg'] = 'Game already played. Please purchase again.';
    }
    else if($result['plays_avail'] > 0) {
      //token valid
      $return['valid'] = true;
      $return['msg'] = 'Session Valid';
    }
  }

  return $return;
}

function submit_payment(){
  global $db;

  $return = array();
  $formData = array();

  // The mandatory fields are first name, email address, why are you interested, and country.
  // All other fields may be empty

  foreach($_POST['data'] as $d){
    if(array_key_exists($d['name'], $formData)){
      if(!is_array($formData[$d['name']])){
        $temp = $formData[$d['name']];
        $formData[$d['name']] = array();
        $formData[$d['name']][] = $temp;
      }
      $formData[$d['name']][] = stripcslashes($d['value']);
    } else {
      $formData[$d['name']] = stripcslashes($d['value']);
    }
  }

  $mode = $_POST['mode'];

  $return['coupon'] = $formData['coupon'];

  $r = check_coupon_fn($mode,$formData['coupon']);
  
  $return['rdata'] = $r;
  //only charge card if price is > 0
  $r['price'] = floatval($r['price']);

  $price = floatval($r['price']);
  $price = intval(round($price*100));

  if($r['price']>0){
    $email = $formData['email'];

    require_once('stripe/init.php');

    $stripe = array(
      "secret_key"      => "sk_live_nfEeRUPGpYqeblLrrz39aBrz",
      "publishable_key" => "pk_live_paBUM2VNEoYEmwRKLj9RbGKQ"
    );

    \Stripe\Stripe::setApiKey($stripe['secret_key']);

    $card = $_POST['card'];
    $token = $card['id'];

    $price = floatval($r['price']);

    // $_price = ($price + .30) / (1 - 0.0175);
    // $fee = $_price - $price;
    // $price = $_price;
    // $price = number_format((float)$price, 2, '.', '');
    // $return['price'] = $price;
    // $fee = number_format((float)$fee, 2, '.', '');
    $price = intval(round($price*100));

    try {
      $customer = \Stripe\Customer::create(array(
        "email" => $email,
        "source"  => $token
      ));
      $charge = \Stripe\Charge::create(array(
        "customer" => $customer->id,
        "amount" => $price,
        "currency" => "usd",
        "description" => $form['name']
        // "source" => $token
      ));
    } catch (Exception $e) {
      $return['error'] = $e;
      //error
      $return['success'] = false;
    }
  }


  if($return['success']!==false){
    //success
    $return['success'] = true;

    $return['data'] = $formData;

    if($return['success'] == true){

      $result = 1;
      $passes = 0;
      while($result > 0){
        $token = strrev(base_convert(bin2hex(hash('sha512', uniqid(mt_rand() . microtime(true) * 10000, true), true)), 3, 36));
        $sql = "SELECT COUNT(*) FROM bg_users WHERE token = ?";
        $result = $db->select_var($sql,array($token));
        $passes++;
      }
      $__passes = intval(get_option('brand_game_token_passes'));
      if($passes > $__passes) update_option('brand_game_token_passes',$passes);

      $return['token'] = $token;
      // $optin = isset($formData['optin']) ? 1 : 0;
      $optin = 1;
      $now = strtotime('now');
      $coupon = ($r['status'] == 'success') ? $formData['coupon'] : 'null';

      $data = array(
        'first'=>$formData['first'],
        'last'=>$formData['last'],
        'business'=>$formData['business'],
        'email'=>$formData['email'],
        'country'=>$formData['country'],
        'optin'=>$optin,
        'token'=>$token,
        'plays_avail'=>2,
        'mode'=>$_POST['mode'],
        'date'=>$now,
        'payment_amount'=>$price,
        'payment_coupon'=>$coupon,
        'birthday'=>$formData['birthday'],
        'hear_about'=>$formData['hear_about'],
        'interested'=>$formData['interested']
      );
      $db->insert('bg_users',$data);

      //send e-mail
      ob_start();

      $_email['content'] = '<img src="https://brandpersonalities.com.au/wp-content/uploads/2018/08/Branding-with-Personality-Brand-Personalities-developed-by-White-River-Design-Play-The-Game-Test-Online-1400x634.jpg" style="display: block;border: none;-ms-interpolation-mode: bicubic;max-width: 100%;position: relative;margin: 0 auto;">';
      $_email['content'] .= "<br>Hi {name},<br><p>Thanks for purchasing Brand Personalities!</p><p>Take your time and have some fun. This can be a bit challenging which is why we give you two chances to do the quiz.</p><p>Click the button below to give the quiz a go!</p><p></p>";
      $_email['content'] = str_replace("{name}", $formData['first'], $_email['content']);

      global $pagelink;
      $_email['link'] = $pagelink.'/?token='.$token;
      $_email['button_text'] = 'Play The Quiz!';

      include 'email-template-token.php';
      $message = ob_get_clean();
      $to = $formData['email'];
      $subject = 'Your Brand Personality Game Purchase';
    //   $headers = array('Content-Type: text/html; charset=UTF-8');
      $headers = array('Content-Type: text/html; charset=UTF-8', 'From: Brand Personalities <info@brandpersonalities.com.au>');
      $attachments = 'pdf/invoice_'.$token.'.pdf';

      $pdf_data = array(
        'first'=>$formData['first'],
        'last'=>$formData['last'],
        'token'=>$token,
        'business'=>$formData['business'], 
        'price_original'=>$r['price_original'],
        'price'=>number_format($r['price'], 2),
        'discount_status'=>$r['status'],
        'discount'=>$r['discount'],
        'discount_type'=>$r['discount_type']
      );  
      $pdf_result = generate_pdf($pdf_data);

      $mail_result = wp_mail( $to, $subject, $message, $headers, $attachments);
      if($mail_result){
        unlink($attachments);
      }

      //push to mailchimp
      $info = array(
        'first'=>$formData['first'],
        'last'=>$formData['last'],
        'email'=>$formData['email'], 
        'business'=>$formData['business'], 
        'birthday'=>$formData['birthday'],
        'hear_about'=>$formData['hear_about'],
        'interested'=>$formData['interested']
      );
      try {
        brandp_post_to_mailchimp($info, '4917447e3e');
      } catch (Exception $e) {
        error_log( 'Error updating mailchimp: ' . $e);
      }
    }
  }

  echo json_encode($return);

  die();
}

function get_personality(){
  /*
  PERSONALITY CHOICES:
  ---------------------
  5:0 = dominant personality
  4:1 = dominant personality
  3:1:1 = dominant personality
  3:2 = a combination of 2 brand personalities
  2:2:1 = a combination of 2 brand personalities (ignore the single one)
  2:1:1:1 or all different = they are confused and need to contact us for an accurate result
  */

  /*
  SELECT p.ID, p.value as personality, GROUP_CONCAT(a.value SEPARATOR '\n') as keywords
  FROM bg_personalities p
  JOIN bg_answers a ON p.ID = a.pid
  GROUP BY p.ID
  */

  global $db;

  $type = $_POST['mode'];
  if($type == 'personal'){
    $q_table = 'bg_questions';
    $a_table = 'bg_answers';
  }
  if($type == 'business'){
    $q_table = 'bg_questions_business';
    $a_table = 'bg_answers_business';
  }

  //finalTwelve
  $finaltwelve = $_POST['finaltwelve'];
  unset($finaltwelve[0]);
  $finaltwelve = array_values($finaltwelve);
  $sql = "SELECT * FROM $a_table WHERE ID IN (".implode(',',$finaltwelve).")";
  $finaltwelve = $db->select($sql,array());
  $_finaltwelve = array();
  foreach($finaltwelve as $f){
    $_finaltwelve[] = $f['value'];
  }
  $_finaltwelve = implode('|',$_finaltwelve);

  //finalFive
  $ids = $_POST['ids'];
  $sql = "SELECT * FROM $a_table WHERE ID IN (".implode(',',$ids).")";
  $finalfive = $db->select($sql,array());
  $_finalfive = array();
  foreach($finalfive as $f){
    $_finalfive[] = $f['value'];
  }
  $_finalfive = implode('|',$_finalfive);



  // $ids = array_filter($ids);

  $ids_p = array();
  foreach($ids as $id){
    $arr = explode(",",$id);
    foreach($arr as $a){
      $ids_p[] = $a;
    }
  }

  $ids = $ids_p;

  $_ids = implode(",",$ids);

  $sql = "SELECT p.ID as ID, p.value as personality, a.value
  FROM bg_personalities p
  JOIN $a_table a ON p.ID = a.pid
  WHERE a.ID IN($_ids)";

  $result = $db->select($sql,array());

  $result_personalities = array();
  foreach ($result as $value) {
    $result_personalities[] = (int)$value['ID'];
  }
  $_result_personalities = implode(",",$result_personalities);
  //$_result_personalities = json_encode($result_personalities);


  $count = array();
  foreach($result as $r){
    if(! array_key_exists($r['ID'],$count)) {
      $count[$r['ID']] = 1;
    } else {
      $count[$r['ID']]++;
    }
  }

  $max = array_keys($count, max($count));

  // 5:0 = dominant personality
  // 4:1 = dominant personality
  // 3:1:1 = dominant personality
  // 3:2 = a combination of 2 brand personalities
  // 2:2:1 = a combination of 2 brand personalities (ignore the single one)
  // 2:1:1:1 or all different = they are confused and need to contact us for an accurate result

  $personalityID = array();

  if(count($max) == 1){
    // 5:0 = dominant personality
    if($count[$max[0]]==5){
      $personalityID[] = array_search(5, $count);
    }
    // 4:1 = dominant personality
    else if($count[$max[0]]==4) {
      $personalityID[] = array_search(4, $count);
    }
    else if($count[$max[0]]==3){
      // 3:2 = a combination of 2 brand personalities
      if(in_array(3,$count) && in_array(2,$count)){
        $personalityID[] = array_search(3, $count);
        $personalityID[] = array_search(2, $count);
      }
      // 3:1:1 = dominant personality
      else if(in_array(3,$count)){
        $personalityID[] = array_search(3, $count);
      }
    }
    // 2:1:1:1 or all different = they are confused and need to contact us for an accurate result
    else if($count[$max[0]]==2){
      $personalityID[] = 13;
    }
  }
  else if(count($max)==2){
    // 2:2:1 = a combination of 2 brand personalities (ignore the single one)
    if(in_array(2,$count)){
      foreach($count as $key => $val){
        if($val==1) continue;
        $personalityID[] = $key;
      }
    }
  }
  //confused
  else {
    $personalityID[] = 13;
  }

  $sql = "SELECT * FROM bg_personalities WHERE ID IN (".implode(',',$personalityID).")";
  $personality = $db->select($sql,array());

  $i = 0;
  foreach($personality as $p){
    $p['image_url'] = wp_get_attachment_image_src($p['image'],'large')[0];

    $personality[$i] = $p;
    $i++;
  }

  //update plays_avail for token
  $token = $_POST['token'];

  $sql = "SELECT * FROM bg_users WHERE token = ?";
  $user = $db->select_row($sql,array($token));
  $new_plays = intval($user['plays_avail']) - 1;
  $db->update('bg_users',array('plays_avail'=>$new_plays),array('token'=>$token));

  if($new_plays === 1){
    $play_result = "first";
  }elseif($new_plays === 0){
    $play_result = "second";
  }

  //log result
  $user_result = implode(',',$personalityID);
  $now = strtotime('now');
  $data = array(
    'user_id'=>$user['ID'],
    'result'=>$user_result,
    'date'=>$now,
    'final_choices'=>$_finalfive,
    'final_twelve'=>$_finaltwelve,
    'personalities'=>$_result_personalities
  );
  $db->insert('bg_results',$data);


  //email data
  $_email = array();

  $tagData = array(
    'name'=>$user['first'],
    'personality'=>$personality
  );

  if(in_array(13,$personalityID)){
    $_email['confused'] = true;
    $_email['before'] = brandp_replace_tags(get_option('message_before_fail'),$tagData);
  } else {
    $_email['confused'] = false;
    $_email['before'] = brandp_replace_tags(get_option('message_before'),$tagData);
    $_email['personality_desc'] = '';
    foreach($personality as $p){
      $page = get_page_by_title($p['value']);
      $link = get_permalink($page->ID);
      $_email['personality_desc'] .= '<p><strong>'.$p['value'].':</strong><br>';
      $_email['personality_desc'] .= stripslashes($p['description']).'</p>';
    }
  }
  $_email['after'] = brandp_replace_tags(get_option('message_after'),$tagData);

  ob_start();
  global $siteurl;
  if(count($personality)==1){
    $page = get_page_by_title($personality[0]['value']);
    $link = get_permalink($page->ID);
    if($personality[0]['ID'] == 13){
      $icon = $siteurl.'/img/unclear-icon.png';
    }else{
      $icon = 'http://whiteriverdesign.com/files/brand-personalities/result_icons/'.$personality[0]['ID'].'.png';
    }
    ?>
    <a target="_blank" href="<?php echo $link; ?>" style="color: #3498db;text-decoration: underline;font-size: 16px !important;">
      <img alt="<?php echo $personality[0]['value']; ?>" src="<?php echo $icon ?>" width="50%" style="margin: 0 auto;display: block;border: none;-ms-interpolation-mode: bicubic;max-width: 100%;">
    </a>
  <?php } else {
    foreach($personality as $p){
      $page = get_page_by_title($p['value']);
      $link = get_permalink($page->ID);
      $icon = 'http://whiteriverdesign.com/files/brand-personalities/result_icons/'.$p['ID'].'.png';
    ?>
      <a target="_blank" href="<?php echo $link; ?>" style="color: #3498db;text-decoration: underline;font-size: 16px !important;">
        <img alt="<?php echo $p['value']; ?>" src="<?php echo $icon ?>" width="50%" style="float: left;margin: 0 auto;display: block;border: none;-ms-interpolation-mode: bicubic;max-width: 100%;">
      </a>
    <?php } ?>
  <?php }
  $_email['personality_content'] = ob_get_clean();

  //send e-mail
  ob_start();
  include 'email-template-result.php';
  $message = ob_get_clean();
  $to = $user['email'];
  $subject = ($_email['confused']) ? 'Your Brand Personality Result hit a snag ' . $user['first'] . '. See inside for info.' : 'Your Brand Personality Results are in ' . $user['first'];
//   $headers = array('Content-Type: text/html; charset=UTF-8');
  $headers = array('Content-Type: text/html; charset=UTF-8', 'From: Brand Personalities <info@brandpersonalities.com.au>');
  wp_mail( $to, $subject, $message, $headers );
  //----------------------------------------

  //send email notification to admin
  $formName = 'New Brand Personality Result';
  $values = array();
  $personality_list = array();
  foreach($personality as $p){
    $personality_list[] = $p['value'];
  }
  $values['Name'] = $user['first'].' '.$user['last'];
  $values['E-Mail'] = $user['email'];
  if(!empty($user['business'])) $values['Business'] = $user['business'];
  $values['Personality'] = implode(", ", $personality_list);
  $values['Mode'] = $user['mode'];

  $_price = floatval($user['payment_amount']) / 100;
  $price = number_format((float)$_price, 2, '.', '');
  $price_dsp = '$'.$price;
  $values['Payment Amount'] = $price_dsp;
  if(!empty($user['payment_coupon'])) $values['Payment Coupon'] = $user['payment_coupon'];

  $to = get_option('wrd-notification-email');
//   $headers = array('Content-Type: text/html; charset=UTF-8');
  $headers = array('Content-Type: text/html; charset=UTF-8', 'From: Brand Personalities <info@brandpersonalities.com.au>');
  include 'email-template-notification.php';
  //----------------------------------------

  $p_string = array();
  $i = 0;
  foreach($personality as $p){
    if($p['ID']==13){
      $p['description'] = brandp_replace_tags(get_option('message_fail'),$tagData);
    } else {
      $p['description'] = brandp_replace_tags(get_option('message_success'),$tagData);
    }

    $x = str_replace('The ','',$p['value']);
    $x = strtolower($x);
    $p_string[] = $x;

    $personality[$i] = $p;
    $i++;
  }

  $p_string = implode('-',$p_string);

  //return data
  echo json_encode(array('result'=>$result,'count'=>$count,'personalityID'=>$personalityID,'personality'=>$personality,'max'=>$max,'personality_string'=>$p_string, 'result_personalities'=>$result_personalities, '_result_personalities'=>$_result_personalities, 'play_result'=>$play_result));
  die();
}

function get_selections(){
  global $db;

  $type = $_POST['mode'];
  if($type == 'personal'){
    $q_table = 'bg_questions';
    $a_table = 'bg_answers';
  }
  if($type == 'business'){
    $q_table = 'bg_questions_business';
    $a_table = 'bg_answers_business';
  }

  $ids = $_POST['ids'];
  $ids = array_filter($ids);
  $_ids = implode(",",$ids);

  $sql = "SELECT ID as id, value as word
  FROM $a_table
  WHERE ID IN($_ids)";
  // $sql.=" ORDER BY RAND()";

  $result = $db->select($sql,array());
  $i=0;

  $words = array();
  foreach($result as $r){
    if(!array_key_exists($r['word'],$words)){
      $words[$r['word']] = array();
    }
    $words[$r['word']][] = $r['id'];
    $i++;
  }
  $word_arr = array();
  foreach($words as $k=>$v){
    $word_arr[] = array('word'=>$k,'ids'=>$v);
  }

  echo json_encode($word_arr);
  // echo json_encode($result);
  die();
}

function get_questions(){
  global $db;

  /*$sql = "SELECT GROUP_CONCAT(p.per_name SEPARATOR '\n') as word, p.per_id as id, qp.question_id as qid, q.question
  FROM question_personalities_personal qp
  LEFT JOIN personalities_personal p ON qp.personality_id = p.per_id
  LEFT JOIN questions_personal q ON q.question_id = qp.question_id
  GROUP BY qid
  ORDER BY qid";*/

  $type = $_POST['mode'];
  if($type == 'personal'){
    $q_table = 'bg_questions';
    $a_table = 'bg_answers';
  }
  if($type == 'business'){
    $q_table = 'bg_questions_business';
    $a_table = 'bg_answers_business';
  }

  // $question = $_POST['question'];
  $sql = "SELECT a.value as word, a.ID as id, a.qid
  FROM $a_table a
  ORDER BY RAND()";
  $_words = $db->select($sql,array());

  $words = array();
  foreach($_words as $w){
    $words[$w['qid']][] = $w;
  }

  $sql = "SELECT ID, value as question
  FROM $q_table";
  $questions = $db->select($sql,array());

  echo json_encode(array('words'=>$words,'questions'=>$questions));
  die();
}


function brandp_replace_tags($string,$data){
  $string = str_replace("{name}", $data['name'], $string);

  $_p = array();
  foreach($data['personality'] as $p){
    $_p[] = $p['value'];
  }

  $data['personality'] = implode(' and ', $_p);
  $string = str_replace("{personality}", $data['personality'], $string);

  return stripslashes($string);
}

function brandp_post_to_mailchimp($info, $list){
  global $wpdb;

  $key = 'c0984c48dbfa5bcb80a9a3a37d20ef98-us16';

  $first = $info['first'];
  $last = $info['last'];
  $data = [
    'email'     => $info['email'],
    'status'    => 'subscribed',
    'firstname' => $first,
    'lastname'  => $last
  ];

  $apiKey = $key;
  $listId = $list;

  $memberId = md5(strtolower($data['email']));
  $dataCenter = substr($apiKey,strpos($apiKey,'-')+1);
  $url = 'https://' . $dataCenter . '.api.mailchimp.com/3.0/lists/' . $listId . '/members/' . $memberId;

  // Submit all data apart from the birthday field as it is possible it has invalid data. And
  // if it does then nothing will be stored in Mailchimp. So we'll store everything else and then
  // try to add the birthday later. If the birthday update fails then at least we have most of the
  // data entered into Mailchimp.
  $json = json_encode([
      'email_address'  => $data['email'],
      'status'         => $data['status'], // "subscribed","unsubscribed","cleaned","pending"
      'merge_fields'   => [
          'FNAME'      => $data['firstname'],
          'LNAME'      => $data['lastname'],
          'BUSNAME'    => $info['business'],
          'HEARABOUT'  => $info['hear_about'],
          'QUIZPURPOS' => $info['interested']
      ]
  ]);
//  error_log('Mailchimp api content = ' . $json);
  
  $ch = curl_init($url);

  curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $apiKey);
  curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_TIMEOUT, 10);
  curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
  curl_setopt($ch, CURLOPT_POSTFIELDS, $json);

  $result = curl_exec($ch);
  $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

  // Stored or updated most of the contact details in Mailchimp
  
  if ($httpCode != 200) {
    // The initial PUT has failed for some reason. There is no point continuing
    curl_close($ch);
    return;
  }
  
  // Do we have a valid birthday?
  $matches = array();
  if (!preg_match('/^([0-9]+)\/([0-9]+)$/', $info['birthday'], $matches)) {
    // We don't have a valid birthday. Nothing more we can do
    curl_close($ch);
    return;    
  }
  
  $day = $matches[1];
  $month = $matches[2];
  $birthday = date('n/j', strtotime("2019-$month-$day"));
  
  $json = json_encode([
      'email_address'  => $data['email'],
      'merge_fields'   => [
          'BIRTHDAY'   => $birthday
      ]
  ]);
//  error_log('Mailchimp api birthday content = ' . $json);
  
  curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $apiKey);
  curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_TIMEOUT, 10);
  curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PATCH');
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
  curl_setopt($ch, CURLOPT_POSTFIELDS, $json);

  $result = curl_exec($ch);
  $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
  
  curl_close($ch);
}













?>
