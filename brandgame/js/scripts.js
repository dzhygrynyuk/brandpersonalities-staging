var mobile;

$(function(){

  //---Reload Stylesheets [DEV]
    function reloadStylesheets() {
      var queryString = '?reload=' + new Date().getTime();
      $('link[rel="stylesheet"]').each(function () {
          this.href = this.href.replace(/\?.*|$/, queryString);
      });
    }
    // $(document).bind('keydown', 'ctrl+a', reloadStylesheets);
    //
    // $(document).bind('keydown', 'ctrl+r', restart_game);
    //
    // $(document).bind('keydown', 'ctrl+d', show_dialogue);
  //---

  $('body').on('click','.restart-game',function(){
    restart_game();
  });

  function restart_game() {
    init_game();
  }
  function show_dialogue() {
    $('.dialogue, #blackout').fadeIn(250);
  }
  $('body').on('click','.help-icon',function(){
    show_dialogue();
  });

  function is_mobile(){
    if (window.matchMedia("(max-width: 1280px)").matches) {
      mobile = true;
    } else {
      mobile = false;
    }
  }
  is_mobile();

  var resizeTimer;
  $(window).on('resize',function(){
    clearTimeout(resizeTimer);
    resizeTimer = setTimeout(function() {
      is_mobile();
    }, 250);
  });

  var token = $('#__token').text();

  var card, stripe;
  var $g = $global;
  $g.pulse = true;
  $g.clickable = false;
  $g.mode = $('#__mode').text();
  $g.returning = false;
  $g.paymentNeeded = true;
  $g.action_disabled = false;
  $g.colors = [
    {b:'#f48021',t:'#ffffff'},
    {b:'#510856',t:'#ffffff'},
    {b:'#00b5ef',t:'#ffffff'},
    {b:'#f6a0b3',t:'#000000'},
    {b:'#38635a',t:'#ffffff'},
    {b:'#b3a9d3',t:'#ffffff'},
    {b:'#a3dad6',t:'#000000'},
    {b:'#ed1944',t:'#ffffff'},
    {b:'#7dcef1',t:'#000000'},
    {b:'#b5d99a',t:'#000000'},
    {b:'#f9a456',t:'#000000'},
    {b:'#6a1240',t:'#ffffff'},
    {b:'#f26c52',t:'#ffffff'},
  ]

  function toTitleCase(str) {
    return str.replace(
      /\w\S*/g,
      function(txt) {
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
      }
    );
  }

  function load_template(x,data){
    var html = ejs.render($g['templates'][x],data);
    return html;
  }

  function load_question(){
    if($g.question > 12) {
      return;
    }

    var timeout = 0;
    if($g.pulse){
      window.setTimeout(function(){
        $('.words').addClass('pulse');
        $('.question').addClass('pulse');
      // },0);
      },350);
      timeout = 0;
      timeout = 650;
    }

    console.log('load_question = $g', $g);

    window.setTimeout(function(){
      $('.question ._question').html($g.questions[$g.question-1].question);
      $('.question ._num').html($g.question);
      $('.question').css('background-color',$g.colors[$g.question-1].b);
      $('.question').css('color',$g.colors[$g.question-1].t);

      window.scrollTo(0,0);

      $('.words').html('');
      $.each($g.words[$g.question],function(i,v){
        var html = load_template('item',v);
        $('.words').append(html);
      });

      $('.words li .inner').css('border-color',$g.questions[$g.question-1].question_color);
      $('.words').removeClass('pulse');
      $('.question').removeClass('pulse');

      if($g.selection[$g.question]) {
        var id = $g.selection[$g.question];
        $('.words li[data-id="'+id+'"] .inner').addClass('active');
      }
      $g.clickable = true;

    },timeout);
  }

  function question_pagi(){
    console.log('question_pagi');
    if($g.selected){
      $g.qmax ++;
      $g.selected = false;
    }

    $('.arrow').removeClass('disabled');

    if($g.question == $g.qmax) {
      $('.arrow.right').addClass('disabled');
    }
    if($g.question < $g.qmax){
      $('.arrow.right').removeClass('progress');
    }
    if($g.question == 1) {
      $('.arrow.left').addClass('disabled');
    }
    if($g.question == 12) {
      $g.pulse = false;
      $('.arrow.right').addClass('disabled');
    }

  }

  function initStage(){
    $('.arrow').addClass('disabled');
    // $('.words').html('');
    // $('.question div').html('');

    if($g.stage==1){

      var data = {
        action:'getPaymentData',
        mode:$g.mode
      };

      return $.post(siteurl+'/core.php',data,function(r){
        r = JSON.parse(r);
        console.log(r);
        $g.price = r.price;

        var html = load_template('payment_stage',{price:r.price_dsp,homeurl:homeurl});
        $('#stage').html(html);
        $("#datepicker").datepicker({
          dateFormat: 'dd/mm',
          changeMonth: true,
          changeYear: false
        });

        init_stripe();
        $('body').addClass('payment_stage');
        var top = ['Australia','United States','United Kingdom'];
        // $('#countries').prepend($('<option>', {
        //     text: 'Select a country',
        //     value: ''
        // }));
        $('#countries').append($('<option>', {
            text: 'Select a country',
            value: '',
        }));
        //top.reverse();
        $.each(top,function(i,v){
          $('#countries').append($('<option>', {
              value: v,
              text: v
          }));
        });
        $('#countries').append($('<option>', {
            text: '-------',
            disabled:true
        }));
        $.each(countries.countries,function(i,v){
          v.country.country_name = toTitleCase(v.country.country_name);
          if($.inArray(v.country.country_name,top) == -1){
            $('#countries').append($('<option>', {
                value: v.country.country_name,
                text: v.country.country_name
            }));
          }
        });

      });
    }

    if($g.stage==2){
      var html = load_template('main_stage',{});
      $('#stage').html(html);

      if($g.returning){
        var msg = ($g.plays_avail > 1) ? 'You have '+$g.plays_avail+' plays left' : 'This is your last play.';
        var html = load_template('dialogue_success_return',{plays:msg,mode:$g.mode});
        $('.dialogue').html(html);
        $('.dialogue, #blackout').fadeIn(250);
      } else {
        var html = load_template('dialogue_payment_success',{mode:$g.mode});
        $('.dialogue').html(html);
        $('.dialogue, #blackout').fadeIn(250);
      }

      var data = {
        action:'getQuestions',
        mode:$g.mode
      };

      return $.post(siteurl+'/core.php',data,function(r){
        r = JSON.parse(r);
        console.log(r);
        $g.questions = r.questions;
        $g.words = r.words;
        app();


        //tmp dev
        // $g.stage = 2;
        // $g.clickable = true; //IMPORTANT
        // $g.selection = ["7", "15", "27", "39", "67","1","2","3","4","5","6","8"];
        // $g.final_selection = ["7", "15", "27", "39", "67"];
        // $g.final_selection = [];
        // $g.question = 12;
        // $g.qmax = 12;
        // app('next');
      });

    }

    if($g.stage==3){

      var html = load_template('dialogue_stage2',{mode:$g.mode});
      $('.dialogue').html(html);
      $('.dialogue, #blackout').fadeIn(250);

      $('.words').addClass('pulse');
      $('.question').addClass('pulse');
      $g.final_selection = [];
      $('.question ._question').html('Pick your top 5');
      $('.question ._num').html($g.final_selection.length+' / '+5);
      $('.question').css('background-color',$g.colors[13-1].b);
      $('.question').css('color',$g.colors[13-1].t);
      var data = {
        action:'getSelections',
        ids:$g.selection,
        mode:$g.mode
      };
      console.log(data);
      return $.post(siteurl+'/core.php',data,function(r){
        r = JSON.parse(r);
        console.log(r);

        window.setTimeout(function(){
          $('.words').html('');
          $.each(r,function(i,v){
            v.id = v.ids.join();
            var html = load_template('item',v);
            $('.words').append(html);
          });
          $('.words').removeClass('pulse');
          $('.question').removeClass('pulse');
        },500);

      });
    }

    if($g.stage == 4){

      var html = load_template('loader',{text:'Calculating your Brand Personality'});
      $('.dialogue').html(html);
      bpAnimReset();
      $('.dialogue, #blackout').fadeIn(250);

      console.log($g.final_selection);

      $('.question ._question').html('Your Brand Personality');
      var data = {
        action:'getPersonality',
        ids:$g.final_selection,
        finaltwelve:$g.selection,
        mode:$g.mode,
        token:token
      };
      console.log(data);
      return $.post(siteurl+'/core.php',data,function(r){
        window.setTimeout(function(){
          r = JSON.parse(r);
          console.log(r);
          // $('body').addClass('result_stage');
          // var html = load_template('result',{personalities:r.personality});
          // $('.wrap').html(html);

          if(r.personality_string == 'confused'){
            window.location.replace(homeurl+'/brand-result/'+r.personality_string+'/?token='+token+'&result='+r.play_result);
          }else{
            window.location.replace(homeurl+'/brand-result/'+r.personality_string+'/?token='+token);
          }
          $('.dialogue, #blackout').fadeOut(250);
        },2000);
      });
    }
  }

  function app(action){

    if($g.action_disabled) return;

    if(action=='next' || action=='init'){
      $('body').removeClass();
      $('.arrow.right').removeClass('progress');
      if(action=='next') {
        $g.stage++;
        $g.action_disabled = true;
        console.log($g.action_disabled);
      }
      var $ajax = initStage();
      $ajax.done(function() {
        $g.action_disabled = false;
        console.log($g.action_disabled);
      });
      if(action=='init') return;
    }

    if($g.stage == 2){
      $g.clickable = false;

      load_question();
      question_pagi();

      if($g.question == $g.max && !isEmpty($g.selection[$g.max])){

        var html = load_template('dialogue_stage1',{});
        $('.dialogue').html(html);
        $('.dialogue, #blackout').fadeIn(250);

        $('.arrow.right').addClass('progress');
      }
    }

    if($g.stage == 3){
      $('.question ._num').html($g.final_selection.length+' / '+5);
      if($g.final_selection.length == 5){

        var html = load_template('dialogue_stage2.1',{});
        $('.dialogue').html(html);
        $('.dialogue, #blackout').fadeIn(250);

        $('.arrow.right').addClass('progress');
      } else {
        $('.arrow.right').removeClass('progress');
      }
    }
  }

  $('body').on('click','.arrow',function(e){
    if($(this).hasClass('progress')) {
      app('next');
      return;
    }

    if($(this).hasClass('disabled')) return;

    if($g.stage == 2){

      if($(this).hasClass('left')){
        $g.pulse = false;
        $g.question --;
      }
      if($(this).hasClass('right')){
        $g.question ++;
        if($g.qmax < $g.question) $g.qmax = $g.question;
      }

      if($g.question < $g.min) $g.question = $g.min;
      if($g.question > $g.max) $g.question = $g.max;

    }

    app();
  });

  $('body').on('click','.words li .inner',function(e){
    if(!$g.clickable) return;
    var par = $(this).closest('li');
    var id = par.attr('data-id');

    if($g.stage==2){
      $('.words li .inner').removeClass('active');
      $(this).addClass('active');
      if(!$g.selection[$g.question]) $g.selected = true;
      $g.selection[$g.question] = id;

      window.setTimeout(function(){
        if($g.question < $g.max){
          //$g.question++;
          $g.pulse = false;
          app();
        } else if($g.question == $g.max){
          app();
        }
      },0);
    }

    if($g.stage == 3){
      if($(this).hasClass('active')){
        var rid = par.attr('data-id');
        console.log(rid);
        var index = $g.final_selection.indexOf(rid);
        $g.final_selection.splice(index, 1);
        $(this).removeClass('active');
      } else {
        if($g.final_selection.length > 4){
          var rid = $g.final_selection[0];
          console.log(rid);
          $('.words li[data-id="'+rid+'"] .inner').removeClass('active');
          $g.final_selection.shift(); //remove first item
        }
        $(this).addClass('active');
        $g.final_selection.push(id);
      }
      console.log($g.final_selection);
      app();
    }
  });

  $('body').on('templates_ready', function() {
    console.log($g.templates);

    init_game();
    // app('next'); //remove this!

  });

  function init_game(){
    $g.selected = false;
    $g.selection = [];
    $g.question = 1;
    $g.qmax = 1;
    $g.min = 1;
    $g.max = 12;
    $g.stage = 0;

    var data = {
      siteurl:siteurl,
      homeurl:homeurl
    }
    var html = load_template('intro_stage',data);
    $('#stage').html(html);
    $('body').on('click','#play-business',function(){
      $g.mode = 'business';
      app('next');
    });
    $('body').on('click','#play-personal',function(){
      $g.mode = 'personal';
      app('next');
    });

    check_valid('redirect');
  }

  function check_valid(action){
    var data = {
      action:'checkValid',
      token:token
    };

    $.post(siteurl+'/core.php',data,function(r){
      r = JSON.parse(r);
      console.log(r);
      if(action=='redirect'){
        if(r.valid === true){
          $g.mode = r.data.mode;
          $g.stage = 2;
          $g.returning = true;
          $g.plays_avail = r.data.plays_avail;
          app('init');
        } else {
          if(!isEmpty(token)){
            var html = load_template('dialogue_token_expired',{});
            $('.dialogue').html(html);
            $('.dialogue, #blackout').fadeIn(250);
          }
        }
      }

    });
  }

  $('body').on('click','.__winclose',function(){
    $('.dialogue, #blackout').fadeOut(250);
  });

  $('body').on("mouseenter", ".box-section", function() {
    $('.box-section').addClass('smaller');
    $(this).addClass('hovered').removeClass('smaller');
  });

  $('body').on("mouseleave", ".box-section", function() {
    $('.box-section').removeClass('smaller hovered');
  });

  $('body').on('click','form.payment .form-submit',function(e){
    if($(this).hasClass('disabled')) return;

    $('.form-submit').addClass('disabled');

    var form = $(this).closest('form');
    try_submit(form);
  });

  function try_submit(form){
    form.validate({
      ignore:'',
      errorPlacement: function(error,element) {
        return true;
      },
      highlight: function(element, errorClass) {
          var $element = $(element);
          $element.parent().addClass(errorClass);
      },
      unhighlight: function(element, errorClass) {
          var $element = $(element);
          $element.parent().removeClass(errorClass);
      }
    });
    form.submit();
    $('.form-submit').removeClass('disabled');
  }

  $(document).on('submit','form.payment',function(e){
    e.preventDefault();
    if(!$(this).valid()) {
      $('.form-submit').removeClass('disabled');
      return;
    }

    var form = $(this);

    $('.form-loader').addClass('active');
    $('.form-loader').removeClass('error');

    if($g.paymentNeeded){

      stripe.createToken(card).then(function(result) {
        if (result.error) {
          // Inform the user if there was an error.
          var errorElement = document.getElementById('card-errors');
          errorElement.textContent = result.error.message;

          window.setTimeout(function(){
            $('.form-loader').removeClass('active');
            $('.form-submit').removeClass('disabled');
          },500);
        } else {
          $('form.payment').slideToggle();
          // Send the token to your server.
          // stripeTokenHandler(result.token);

          stripe_token = result.token;

          var formData = form.serializeArray();

          var data = {
            action : 'submitPayment',
            data : formData,
            card : stripe_token,
            mode : $g.mode
          };

          submit_data_final(data);

        }
      });

    } else {
      var formData = form.serializeArray();

      var data = {
        action : 'submitPayment',
        data : formData,
        mode : $g.mode
      };

      submit_data_final(data);

    }

  });

  function submit_data_final(data){
    $('.status-message').html('').hide();

    jQuery.post(ajaxurl, data, function(r) {
      r = JSON.parse(r);
      console.log(r);
      if(r.success === true){
        var url = wp_siteurl+"?token="+r.token;
        token = r.token;
        history.pushState(null, null, url);
        app('next');

        fbq('track', 'Purcahse', {
          content_name: 'Brand Personality Game ['+$g.mode+']',
          content_type: 'product'
        });

      } else {
        $('.form-loader').addClass('error');
        $('.form-submit').removeClass('disabled');
        $('form.payment').show();
        $('.status-message').html(r.error.jsonBody.error.message).show();
      }
    });
  }

  var wto;
  $('body').on('keydown','#coupon_code',function() {
    var $this = $(this);
    clearTimeout(wto);
    wto = setTimeout(function() {
      $('.input-loader').removeClass('success error');
      $('.input-loader').addClass('active');
      // $('.input-loader').addClass('success');

      var data = {
        action:'checkCoupon',
        coupon:$this.val(),
        mode:$g.mode
      };

      $.post(siteurl+'/core.php',data,function(r){
        r = JSON.parse(r);
        r.price = JSON.parse(r.price);
        console.log(r);
        if(r.status=='success'){
          $('.input-loader').addClass('success');
          if(r.price == 0){
            $g.paymentNeeded = false;
            $('.card-field').hide();
          }
        } else {
          $('.input-loader').addClass('error');
          $g.paymentNeeded = true;
          $('.card-field').show();
        }
        $('.price-amount').html(r.price_dsp);
      });

    }, 500);
  });

  var length, bpAnimTimer;
  function fix_range(x){
    if(x > (length-1)) return x - length;
    else return x;
  }
  var i = 0;
  function bpAnimate(){
    var par = $('.bp-anim-contain');
    $('.icon',par).removeClass('noanimate');

    var active_index = $('.active',par).index();

    length = $('.icon',par).length;

    $('.icon',par).removeClass('active active-right active-left active-right-old');
    var new_active_index = active_index + 1;
    $('.icon',par).eq(fix_range(new_active_index)).addClass('active');
    $('.icon',par).eq(fix_range(new_active_index+1)).addClass('active-left');
    $('.icon',par).eq(fix_range(new_active_index-1)).addClass('active-right');

    $('.icon',par).eq(fix_range(new_active_index-2)).addClass('active-right-old');
  }
  function bpAnimReset(){
    clearInterval(bpAnimTimer);
    var par = $('.bp-anim-contain');
    $('.icon',par).addClass('noanimate');
    $('.icon',par).removeClass('active active-right active-left active-right-old');
    $('.icon',par).eq(0).addClass('active');
    $('.icon',par).eq(1).addClass('active-left');
    $('.icon',par).eq(10).addClass('active-right-old');
    $('.icon',par).eq(11).addClass('active-right');
    bpAnimTimer = window.setInterval(bpAnimate,600);
  }

  //------ STRIPE

  function init_stripe(){
    // Create a Stripe client.
    stripe = Stripe('pk_live_paBUM2VNEoYEmwRKLj9RbGKQ'); /* publishable key here */

    // Create an instance of Elements.
    var elements = stripe.elements();

    // Custom styling can be passed to options when creating an Element.
    // (Note that this demo uses a wider set of styles than the guide below.)
    var style = {
      base: {
        color: '#777777',
        lineHeight: '18px',
        fontSmoothing: 'antialiased',
        fontSize: '16px',
        '::placeholder': {
          color: '#777777'
        }
      },
      invalid: {
        color: '#fa755a',
        iconColor: '#fa755a'
      }
    };

    // Create an instance of the card Element.
    card = elements.create('card', {style: style});

    // Add an instance of the card Element into the `card-element` <div>.
    card.mount('#card-element');

    // Handle real-time validation errors from the card Element.
    card.addEventListener('change', function(event) {
      var displayError = document.getElementById('card-errors');
      if (event.error) {
        displayError.textContent = event.error.message;
      } else {
        displayError.textContent = '';
      }
    });
  }

});



function isEmpty(string){
  if(!string) return true;
  return /^\s+$/.test(string);
}

//---Allows moving of array items (from,to)
Array.prototype.move = function (old_index, new_index) {
    if (new_index >= this.length) {
        var k = new_index - this.length;
        while ((k--) + 1) {
            this.push(undefined);
        }
    }
    this.splice(new_index, 0, this.splice(old_index, 1)[0]);
    return this; // for testing purposes
};
