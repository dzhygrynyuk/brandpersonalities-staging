<?php
  include_once 'config.php';
  include_once 'db.php';
  $mode = !empty($_GET['mode']) ? $_GET['mode'] : 'personal';
?>

<!DOCTYPE html>

<head>
  <title><?php echo get_the_title(); ?></title>

  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

  <!-- <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet"> -->
  <link rel="stylesheet" href="<?php echo get_bloginfo("url") ?>/brandgame/fonts/fa/css/font-awesome.min.css">
  <link rel="stylesheet" href="<?php echo get_bloginfo("url") ?>/brandgame/style.css?v=1.04">
  <link rel="stylesheet" href="<?php echo get_bloginfo("url") ?>/brandgame/mq.css?v=1.03">
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

  <script src="<?php echo get_bloginfo("url") ?>/brandgame/js/countries.js"></script>
  <script src="<?php echo get_bloginfo("url") ?>/brandgame/js/jquery.js"></script>
  <script src="<?php echo get_bloginfo("url") ?>/brandgame/js/jquery.validate.min.js"></script>
  <script src="<?php echo get_bloginfo("url") ?>/brandgame/js/jquery.hotkeys.js"></script>
  <script src="https://js.stripe.com/v3/"></script>
  <script src="<?php echo get_bloginfo("url") ?>/brandgame/js/ejs.min.js"></script>
  <script src="<?php echo get_bloginfo("url") ?>/brandgame/templates/_main.js?v=1.02"></script>
  <script src="<?php echo get_bloginfo("url") ?>/brandgame/js/matchmedia.js"></script>
  <script src="<?php echo get_bloginfo("url") ?>/brandgame/js/scripts.js?v=<?php echo time();?>"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

  <link rel="apple-touch-icon" sizes="180x180" href="<?php echo $siteurl; ?>/img/favicons/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="<?php echo $siteurl; ?>/img/favicons/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="<?php echo $siteurl; ?>/img/favicons/favicon-16x16.png">
  <link rel="manifest" href="<?php echo $siteurl; ?>/img/favicons/site.webmanifest">
  <link rel="mask-icon" href="<?php echo $siteurl; ?>/img/favicons/safari-pinned-tab.svg" color="#3d3d3d">
  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="theme-color" content="#ffffff">

</head>

<body>
  <div class="__hidden" id="__mode"><?php echo $mode; ?></div>
  <div class="__hidden" id="__token"><?php echo $_GET['token']; ?></div>

  <div id="blackout"></div>

  <div id="stage"></div>


  <!-- Facebook Pixel Code -->
  <script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window,document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');

    fbq('init', '322428671450889');
    fbq('track', 'PageView');
  </script>
  <noscript>
  <img height="1" width="1"
  src="https://www.facebook.com/tr?id=322428671450889&ev=PageView
  &noscript=1"/>
  </noscript>
  <!-- End Facebook Pixel Code -->

</body>
