<?php 
include_once 'config.php'; 
include_once 'db.php';

global $homeurl;
global $db;

$sql_user = "SELECT * FROM bg_users WHERE token = ?";
$user = $db->select_row($sql_user,array($data['token']));

if($data['discount_status'] == 'success'){
  if($data['discount_type']=='percent'){
    $_discount = $data['discount'] / 100;
    $discount = number_format(($data['price_original'] * $_discount), 2);
  } else if($data['discount_type']=='dollars') {
    $discount = number_format($data['discount'], 2);
  }
}
?>
<!doctype html>
<html>
  <head>
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;700&display=swap" rel="stylesheet">
  </head>
  <style>
    .pricing-table{border-spacing: 0px; border-collapse: collapse; float: right; margin-top: -10px;}
    .pricing-table td.line{height: 0; background-color: #000;}
    .pricing-table .td-first{width: 90px;}
    .pricing-table .td-second{width: 100px;}
    .dark-item{
      display: block;
      font-size: 13px; 
      line-height: 12px;
      color: #fff; 
      background-color: #000; 
      padding: 0px 0px 2px 5px;
    }
  </style>
  <body class="" style="font-family: 'Open Sans', sans-serif; padding-right: 20px;">
    <div style="display: inline-block; width: 100%; padding-top: 10px;">
      <img width="400" src="<?php echo $homeurl; ?>/wp-content/themes/brandp/img/pdf_new_logo3x.png" style="float: right;">
    </div>
    <br><br><br>
    <div style="width: 100%; font-size: 19px; display: block;">
      <h2 style="font-size: 32px; font-weight: 400; margin-bottom: 0px;">INVOICE</h2>
      <div style="margin-top: -8px;"><b style="display: inline-block; width: 170px;">Invoice Number</b><span style="display: inline-block;"><?php echo date('ymd').$user['ID'];?></span></div>
      <div style="margin-top: -10px;"><b style="display: inline-block; width: 170px;">Date</b><span style="display: inline-block;"><?php echo date('j F Y');?></span></div>
      <div style="margin-top: 10px;"><b style="display: inline-block; width: 170px;">Bill To</b><span style="display: inline-block;"><?php echo $data['first']." ".$data['last'];?></span></div>
      <div style="margin-top: -10px;"><b style="display: inline-block; width: 170px;"></b><span style="display: inline-block;"><?php echo $data['business'];?></span></div>
    </div>
    <div style="width: 100%; height: 2px; background-color: #000; margin-top: 15px;"></div>

    <div style="display: block; width: 100%; margin-top: 30px;">
      <div style="float: left;">
        <div>DESCRIPTION</div>
        <div style="display: block; letter-spacing: 1px; margin-top: 5px;">&nbsp;&nbsp;• Brand Personalities Quiz via www.brandpersonalities.com.au</div>
        <?php if($data['discount_status'] === 'success'): ?>
          <div style="display: block; letter-spacing: 1px; margin-top: 5px;">&nbsp;&nbsp;• Coupon applied</div>
        <?php endif; ?>
      </div>
      <table style="float: right;">
        <tr>
          <td align="right">AMOUNT</td>
        </tr>
        <tr>
          <td align="right">US$<?php echo $data['price_original'];?></td>
        </tr>
        <?php if($data['discount_status'] === 'success'): ?>
          <tr>
            <td align="right">-US$<?php echo $discount;?></td>
          </tr>
        <?php endif; ?>  
      </table>  
    </div> 

    <br><br><br><br><br><br><br><br><br>

    <table class="pricing-table">
      <tbody>
        <tr>
          <td class="td-first" align="right">SUB TOTAL</td>
          <td class="td-second" align="right">US$<?php echo $data['price'];?></td>
        </tr>
        <tr style="margin-top: -10px;">
          <td class="td-first" align="right">GST</td>
          <td class="td-second" align="right">0</td>
        </tr>
        <tr>
          <td class="line"></td>
          <td class="line"></td>
        </tr>    
        <tr>
          <td class="td-first" align="right"><b>TOTAL</b></td>
          <td class="td-second" align="right"><b>US$<?php echo $data['price'];?></b></td>
        </tr>
        <tr>
          <td class="td-first" align="right">AMOUNT PAID</td>
          <td class="td-second" align="right">US$<?php echo $data['price'];?></td>
        </tr>
        <tr>
          <td class="line"></td>
          <td class="line"></td>
        </tr>
        <tr>
          <td class="td-first" align="right"><b>BALANCE DUE</b></td>
          <td class="td-second" align="right"><b>0</b></td>
        </tr>
      </tbody>
    </table>

    <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
    <p>*GST has not been charged</p>
    <br><br>

    <div class="dark-item" style="width: 125px; margin: 15px 0 2px 0;">t • +61 2 4732 4766</div>
    <div class="dark-item" style="width: 225px; margin-bottom: 2px;">e • info@brandpersonalities.com.au</div>
    <div class="dark-item" style="width: 213px; margin-bottom: 5px;">www.<b>brandpersonalities</b>.com.au</div>
    <div class="dark-item" style="width: 127px; margin-bottom: 3px;">The Creative Fringle</div>
    <div class="dark-item" style="width: 265px; margin-bottom: 2px;">a • Unit 6, 51 York Road, Penrith NSW 2750</div>
    <div style="font-size: 9px; margin-top: -5px;">Brand Magic Pty Ltd • abn 24632257982</div>

  </body>
</html>