$global = {};

$(function(){

  $global.templates = {};

  var dir = siteurl+'/templates/';
  var files = [
    'payment_stage',
    'intro_stage',
    'main_stage',
    'item',
    'result',
    'loader',
    'dialogue_stage1',
    'dialogue_stage2',
    'dialogue_stage2.1',
    'dialogue_payment_success',
    'dialogue_success_return',
    'dialogue_token_expired'
  ];

  var len = files.length - 1;
  $.each(files,function(i,v){
    var f = dir+v+'.ejs?v='+Date.now();
    $.get(f, function (file) {
      $global['templates'][v] = file;
      if(i==len) {
        $('body').trigger('templates_ready');
      }
    });
  });

});
