<?php 
include_once 'config.php'; 

global $homeurl;
?>
<!doctype html>
<html>
  <head>
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Brand Personalities</title>
  </head>
  <body class="" style="background-color: #f6f6f6;font-family: sans-serif;-webkit-font-smoothing: antialiased;font-size: 16px;line-height: 1.4;margin: 0;padding: 0;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
    <table border="0" cellpadding="0" cellspacing="0" class="body" style="border-collapse: separate;mso-table-lspace: 0pt;mso-table-rspace: 0pt;width: 100%;background-color: #f6f6f6;">
      <tr>
        <td style="position: relative;font-family: sans-serif;font-size: 16px !important;vertical-align: top;">&nbsp;</td>
        <td class="container" style="position: relative;font-family: sans-serif;font-size: 16px !important;vertical-align: top;display: block;max-width: 580px;padding: 0 !important;width: 100% !important;margin: 0 auto !important;">
          <div class="content" style="box-sizing: border-box;display: block;margin: 0 auto;max-width: 580px;padding: 0 !important;">

            <!-- START CENTERED WHITE CONTAINER -->
            <table class="main" style="border-collapse: separate;mso-table-lspace: 0pt;mso-table-rspace: 0pt;width: 100%;background: #ffffff;border-radius: 0 !important;border-left-width: 0 !important;border-right-width: 0 !important;">

              <!-- START MAIN CONTENT AREA -->
              <tr>
                <td class="logo" style="background-color: #000000;padding: 20px;box-sizing: border-box;position: relative;font-family: sans-serif;font-size: 16px !important;vertical-align: top;">
                  <img width="360" src="<?php echo $homeurl; ?>/wp-content/themes/brandp/img/logo-new-white.png" style="display: block;border: none;-ms-interpolation-mode: bicubic;max-width: 100%;position: relative;margin: 0 auto;">
                </td>
              </tr>
              <tr>
                <td class="wrapper" style="position: relative;font-family: sans-serif;font-size: 16px !important;vertical-align: top;box-sizing: border-box;padding: 10px !important;">
                  <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate;mso-table-lspace: 0pt;mso-table-rspace: 0pt;width: 100%;">
                    <tr>
                      <td style="position: relative;font-family: sans-serif;font-size: 16px !important;vertical-align: top;">

                        <?php echo $_email['content']; ?>

                      </td>
                    </tr>
                    <tr>
                      <td class="btn" style="position: relative;font-family: sans-serif;font-size: 16px !important;vertical-align: top;box-sizing: border-box;width: 100%;"><a href="<?php echo $_email['link']; ?>" target="_blank" style="color: #000000;text-decoration: none;background-color: #ffffff;border: solid 1px #000000;border-radius: 5px;box-sizing: border-box;cursor: pointer;display: block;font-size: 16px !important;font-weight: bold;margin: 0;padding: 12px 25px;text-transform: capitalize;text-align: center;width: 100% !important;"><?php echo $_email['button_text']; ?></a></td>
                    </tr>
                  </table>
                </td>
              </tr>

            <!-- END MAIN CONTENT AREA -->
            </table>

          <!-- END CENTERED WHITE CONTAINER -->
          </div>
        </td>
        <td style="position: relative;font-family: sans-serif;font-size: 16px !important;vertical-align: top;">&nbsp;</td>
      </tr>
    </table>
  </body>
</html>
