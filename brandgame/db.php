<?php
class db extends _db {
  var $id;

  function insert($table, $data){
    $sql = "INSERT INTO $table (";

    foreach($data as $key => $val){$sql.=$key.',';}
    $sql = rtrim($sql,',');
    $sql.=") VALUES (";
    foreach($data as $key => $val){$sql.=':'.$key.',';}
    $sql = rtrim($sql,',');
    $sql.=")";

		$query = $this->con->prepare($sql);
    $status = $query->execute($data);
    if($status){
      $this->id = $this->con->lastInsertId();
    }
    return $status;
  }

  function update($table, $data, $where) {
    $sql = "UPDATE $table SET ";

    foreach($data as $key => $val){$sql.=$key.' = :'.$key.',';}
    $sql = rtrim($sql,',');
    $sql.=" WHERE ";
    foreach($where as $key => $val){
      $sql.=$key.' = :w'.$key.' AND ';
      $where['w'.$key]=$val;
    	unset($where[$key]);
    }
    $sql = substr($sql, 0, -4);

    $data = array_merge($data,$where);
    $query = $this->con->prepare($sql);
    $status = $query->execute($data);
    return $status;
  }

  function replace($table, $data){
    $sql = "REPLACE INTO $table (";

    foreach($data as $key => $val){$sql.=$key.',';}
    $sql = rtrim($sql,',');
    $sql.=") VALUES (";
    foreach($data as $key => $val){$sql.=':'.$key.',';}
    $sql = rtrim($sql,',');
    $sql.=")";

		$query = $this->con->prepare($sql);
    $status = $query->execute($data);
    if($status){
      $this->id = $this->con->lastInsertId();
    }
    return $status;
  }

  function delete($table,$where){
    $sql = "DELETE FROM $table WHERE ";

    foreach($where as $key => $val){$sql.=$key.' = :'.$key.' AND ';}
    $sql = substr($sql, 0, -4);

    $query = $this->con->prepare($sql);
    $query->execute($where);
    return 'success';
  }

  function select($sql,$data){
    $query = $this->con->prepare($sql);
    $query->execute($data);
    return $query->fetchAll(PDO::FETCH_ASSOC);
  }

  function select_row($sql,$data){
    $query = $this->con->prepare($sql.' LIMIT 1');
    $query->execute($data);
    return $query->fetch(PDO::FETCH_ASSOC);
  }

  function select_var($sql,$data){
    $query = $this->con->prepare($sql);
    $query->execute($data);
    return $query->fetchColumn();
  }

  function query($sql,$data){
    $query = $this->con->prepare($sql);
    $query->execute($data);
    return 'success';
  }

}

$db = new db();

//INSERT
/*$array = array(
  'name' => 'strawberry',
  'number' => 53
);
echo $db->insert('gallery',$array);
echo '<br>'.$db->id;*/

//UPDATE
/*$data = array(
  'name' => 'apple',
  'number' => 24
);
$where = array(
  'number' => 15
);
echo $db->update('gallery',$data,$where);*/

//DELETE
/*$where = array(
  'name' => 'orange',
  'number' => 52
);
echo $db->delete('gallery',$where);*/

//SELECT
/*$sql = "SELECT * FROM gallery WHERE name LIKE ? AND number > ?";
$data = array('%orang%', 50);
$result = $db->select($sql,$data);
//---
foreach($result as $r){
  foreach($r as $key => $val){
    echo '['.$key.'] => '.$val.'<br>';
  }
  echo '<br>';
}*/

//SELECT VAR
/*$sql = "SELECT name FROM gallery WHERE ID = ?";
$data = array(4);
echo $db->select_var($sql,$data);*/

//QUERY
/*$sql = "DELETE FROM gallery WHERE number > ?";
$data = array(1);
echo $db->query($sql,$data);*/
?>
